#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    BDAQ53 plotting class for creating combined multi-chip module plots from existing scan results.

    Standalone usage:
        At the bottom of the script, enter
        - the type of the module
        - the run_name (filename w/o extension) of the interpreted scan results
        - the path to the module's output directory
        - the module's chips' serial numbers
        and run the script.
'''

import os
import numpy as np
import tables as tb
import matplotlib.pyplot as plt

from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

from bdaq53.system import logger
from bdaq53.chips import rd53a
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis.plotting import Plotting

OVERTEXT_COLOR = '#07529a'


class ModulePlotting(Plotting):

    def __init__(self, analyzed_data_file_0, analyzed_data_file_1, analyzed_data_file_2=None, analyzed_data_file_3=None, pdf_file=None, mask_noisy_pixels=False, noisy_pixel_threshold=1e3, level='preliminary', qualitative=False, internal=False, save_single_pdf=False, save_png=False):
        self.log = logger.setup_derived_logger('Plotting')
        self.plot_cnt = 0
        self.save_single_pdf = save_single_pdf
        self.save_png = save_png
        self.level = level
        self.qualitative = qualitative
        self.internal = internal
        self.clustered = False
        self.mask_noisy_pixels = mask_noisy_pixels
        self.noisy_pixel_threshold = noisy_pixel_threshold

        if analyzed_data_file_0 is not None and analyzed_data_file_1 is not None and analyzed_data_file_2 is None and analyzed_data_file_3 is None:  # Dual
            self._module_type = 'dual'
        elif analyzed_data_file_0 is not None and analyzed_data_file_1 is not None and analyzed_data_file_2 is not None and analyzed_data_file_3 is not None:  # Quad
            self._module_type = 'quad'
        else:
            raise ValueError('Can only plot for dual and quad modules')

        if pdf_file is None:
            if self._module_type == 'dual':
                path = os.path.commonpath([analyzed_data_file_0, analyzed_data_file_1])
            else:
                path = os.path.commonpath([analyzed_data_file_0, analyzed_data_file_1, analyzed_data_file_2, analyzed_data_file_3])
            name = os.path.basename(analyzed_data_file_0)
            self.filename = os.path.join(path, '.'.join(name.split('.')[:-1]) + '.pdf')
        else:
            self.filename = pdf_file
        self.out_file = PdfPages(self.filename)

        try:
            in_file_0 = tb.open_file(analyzed_data_file_0, 'r')
            root_0 = in_file_0.root
            in_file_1 = tb.open_file(analyzed_data_file_1, 'r')
            root_1 = in_file_1.root
            if self._module_type == 'quad':
                in_file_2 = tb.open_file(analyzed_data_file_2, 'r')
                root_2 = in_file_2.root
                in_file_3 = tb.open_file(analyzed_data_file_3, 'r')
                root_3 = in_file_3.root
        except IOError as e:
            self.log.error(e)
            raise RuntimeError('Interpreted data file does not exist!')

        self.run_config_0 = au.ConfigDict(root_0.configuration_in.scan.run_config[:])
        self.run_config_1 = au.ConfigDict(root_1.configuration_in.scan.run_config[:])
        self.scan_config_0 = au.ConfigDict(root_0.configuration_in.scan.scan_config[:])
        self.scan_config_1 = au.ConfigDict(root_1.configuration_in.scan.scan_config[:])
        if self._module_type == 'quad':
            self.run_config_2 = au.ConfigDict(root_2.configuration_in.scan.run_config[:])
            self.run_config_3 = au.ConfigDict(root_3.configuration_in.scan.run_config[:])
            self.scan_config_2 = au.ConfigDict(root_2.configuration_in.scan.scan_config[:])
            self.scan_config_3 = au.ConfigDict(root_3.configuration_in.scan.scan_config[:])

        try:
            self.scan_params_0 = root_0.configuration_in.scan.scan_params[:]
            self.scan_params_1 = root_1.configuration_in.scan.scan_params[:]
            if self._module_type == 'quad':
                self.scan_params_2 = root_2.configuration_in.scan.scan_params[:]
                self.scan_params_3 = root_3.configuration_in.scan.scan_params[:]
        except tb.NoSuchNodeError:
            self.scan_params_0 = None
            self.scan_params_1 = None
            if self._module_type == 'quad':
                self.scan_params_2 = None
                self.scan_params_3 = None

        try:
            self.registers_0 = au.ConfigDict(root_0.configuration_in.chip.registers[:])
            self.registers_1 = au.ConfigDict(root_1.configuration_in.chip.registers[:])
            if self._module_type == 'quad':
                self.registers_2 = au.ConfigDict(root_2.configuration_in.chip.registers[:])
                self.registers_3 = au.ConfigDict(root_3.configuration_in.chip.registers[:])
        except tb.NoSuchNodeError:
            self.registers_0 = {}
            self.registers_1 = {}
            if self._module_type == 'quad':
                self.registers_2 = {}
                self.registers_3 = {}

        self.chip_type = self.run_config_0['chip_type']
        if self._module_type == 'dual':
            if self.chip_type.lower() == 'rd53a':
                self.rows = 192
            if self.chip_type.lower() == 'itkpixv1':
                self.rows = 384
            self.cols = 800
            self.num_pix = self.rows * self.cols
            self.plot_box_bounds = [0.5, self.cols + 0.5, self.rows + 0.5, 0.5]
        else:
            if self.chip_type.lower() == 'rd53a':
                self.rows = 384
            if self.chip_type.lower() == 'itkpixv1':
                self.rows = 768
            self.cols = 800
            self.num_pix = self.rows * self.cols
            self.plot_box_bounds = [0.5, self.rows + 0.5, self.cols + 0.5, 0.5]

        if self.run_config_0['scan_id'] not in ['dac_linearity_scan', 'sensor_iv_scan', 'seu_test']:
            try:
                self.enable_mask_0 = self._mask_disabled_pixels(root_0.configuration_in.chip.use_pixel[:], self.scan_config_0)
                self.n_enabled_pixels_0 = len(self.enable_mask_0[~self.enable_mask_0])
                self.enable_mask_1 = self._mask_disabled_pixels(root_1.configuration_in.chip.use_pixel[:], self.scan_config_1)
                self.n_enabled_pixels_1 = len(self.enable_mask_1[~self.enable_mask_1])
                if self._module_type == 'quad':
                    self.enable_mask_2 = self._mask_disabled_pixels(root_2.configuration_in.chip.use_pixel[:], self.scan_config_2)
                    self.n_enabled_pixels_2 = len(self.enable_mask_2[~self.enable_mask_2])
                    self.enable_mask_3 = self._mask_disabled_pixels(root_3.configuration_in.chip.use_pixel[:], self.scan_config_3)
                    self.n_enabled_pixels_3 = len(self.enable_mask_3[~self.enable_mask_3])
            except tb.NoSuchNodeError:
                self.enable_mask_0 = None
                self.enable_mask_1 = None
                self.n_enabled_pixels_0 = (self.scan_config_0['stop_column'] - self.scan_config_0['start_column']) * (self.scan_config_0['stop_row'] - self.scan_config_0['start_row'])
                self.n_enabled_pixels_1 = (self.scan_config_1['stop_column'] - self.scan_config_1['start_column']) * (self.scan_config_1['stop_row'] - self.scan_config_1['start_row'])
                if self._module_type == 'quad':
                    self.enable_mask_2 = None
                    self.enable_mask_3 = None
                    self.n_enabled_pixels_2 = (self.scan_config_2['stop_column'] - self.scan_config_2['start_column']) * (self.scan_config_2['stop_row'] - self.scan_config_2['start_row'])
                    self.n_enabled_pixels_3 = (self.scan_config_3['stop_column'] - self.scan_config_3['start_column']) * (self.scan_config_3['stop_row'] - self.scan_config_3['start_row'])

            try:
                self.tdac_mask_0 = root_0.configuration_in.chip.masks.tdac[:]
                self.tdac_mask_1 = root_1.configuration_in.chip.masks.tdac[:]
                if self._module_type == 'quad':
                    self.tdac_mask_2 = root_2.configuration_in.chip.masks.tdac[:]
                    self.tdac_mask_3 = root_3.configuration_in.chip.masks.tdac[:]
            except tb.NoSuchNodeError:
                self.tdac_mask_0 = None
                self.tdac_mask_1 = None
                if self._module_type == 'quad':
                    self.tdac_mask_2 = None
                    self.tdac_mask_3 = None

        if self.run_config_0['scan_id'] not in ['sensor_iv_scan']:
            self.calibration_0 = {e[0].decode('utf-8'): float(e[1].decode('utf-8')) for e in root_0.configuration_in.chip.calibration[:]}
            self.calibration_1 = {e[0].decode('utf-8'): float(e[1].decode('utf-8')) for e in root_1.configuration_in.chip.calibration[:]}
            if self._module_type == 'quad':
                self.calibration_2 = {e[0].decode('utf-8'): float(e[1].decode('utf-8')) for e in root_2.configuration_in.chip.calibration[:]}
                self.calibration_3 = {e[0].decode('utf-8'): float(e[1].decode('utf-8')) for e in root_3.configuration_in.chip.calibration[:]}

        if self.run_config_0['scan_id'] not in ['dac_linearity_scan', 'sensor_iv_scan', 'seu_test']:
            try:
                self.HistTdcStatus_0 = root_0.HistTdcStatus[:]
                self.HistTdcStatus_1 = root_1.HistTdcStatus[:]
                if self._module_type == 'quad':
                    self.HistTdcStatus_2 = root_2.HistTdcStatus[:]
                    self.HistTdcStatus_3 = root_3.HistTdcStatus[:]
            except tb.NoSuchNodeError:
                self.HistTdcStatus_0 = None
                self.HistTdcStatus_1 = None
                if self._module_type == 'quad':
                    self.HistTdcStatus_2 = None
                    self.HistTdcStatus_3 = None

            self.HistEventStatus_0 = root_0.HistEventStatus[:]
            self.HistOcc_0 = root_0.HistOcc[:]
            self.HistTot_0 = root_0.HistTot[:]
            self.HistRelBCID_0 = root_0.HistRelBCID[:]
            self.HistBCIDError_0 = root_0.HistBCIDError[:]
            self.HistTrigID_0 = root_0.HistTrigID[:]
            self.HistEventStatus_1 = root_1.HistEventStatus[:]
            self.HistOcc_1 = root_1.HistOcc[:]
            self.HistTot_1 = root_1.HistTot[:]
            self.HistRelBCID_1 = root_1.HistRelBCID[:]
            self.HistBCIDError_1 = root_1.HistBCIDError[:]
            self.HistTrigID_1 = root_1.HistTrigID[:]

            if self._module_type == 'quad':
                self.HistEventStatus_2 = root_2.HistEventStatus[:]
                self.HistOcc_2 = root_2.HistOcc[:]
                self.HistTot_2 = root_2.HistTot[:]
                self.HistRelBCID_2 = root_2.HistRelBCID[:]
                self.HistBCIDError_2 = root_2.HistBCIDError[:]
                self.HistTrigID_2 = root_2.HistTrigID[:]
                self.HistEventStatus_3 = root_3.HistEventStatus[:]
                self.HistOcc_3 = root_3.HistOcc[:]
                self.HistTot_3 = root_3.HistTot[:]
                self.HistRelBCID_3 = root_3.HistRelBCID[:]
                self.HistBCIDError_3 = root_3.HistBCIDError[:]
                self.HistTrigID_3 = root_3.HistTrigID[:]

            if self.mask_noisy_pixels:
                n_noisy = 0
                for col in range(400):
                    for row in range(192):
                        median_0 = max(1., np.median(self.HistOcc_0))
                        median_1 = max(1., np.median(self.HistOcc_1))

                        if self.HistOcc_0[col, row] > median_0 * self.noisy_pixel_threshold:
                            self.enable_mask_0[col, row] = True
                            n_noisy += 1
                            self.log.warning('Disabling noisy pixel on FE0: {0}:{1}'.format(col, row))
                        if self.HistOcc_1[col, row] > median_1 * self.noisy_pixel_threshold:
                            self.enable_mask_1[col, row] = True
                            n_noisy += 1
                            self.log.warning('Disabling noisy pixel on FE1: {0}:{1}'.format(col, row))

                        if self._module_type == 'quad':
                            median_2 = max(1., np.median(self.HistOcc_2))
                            median_3 = max(1., np.median(self.HistOcc_3))
                            if self.HistOcc_2[col, row] > median_2 * self.noisy_pixel_threshold:
                                self.enable_mask_2[col, row] = True
                                n_noisy += 1
                                self.log.warning('Disabling noisy pixel on FE2: {0}:{1}'.format(col, row))
                            if self.HistOcc_3[col, row] > median_3 * self.noisy_pixel_threshold:
                                self.enable_mask_3[col, row] = True
                                n_noisy += 1
                                self.log.warning('Disabling noisy pixel on FE3: {0}:{1}'.format(col, row))

                if n_noisy > 0:
                    self.log.warning('Disabled {} noisy pixels in total.'.format(n_noisy))

            if self.run_config_0['scan_id'] in ['threshold_scan', 'global_threshold_tuning', 'in_time_threshold_scan', 'fast_threshold_scan', 'crosstalk_scan']:
                self.ThresholdMap_0 = root_0.ThresholdMap[:, :]
                self.Chi2Map_0 = root_0.Chi2Map[:, :]
                self.NoiseMap_0 = root_0.NoiseMap[:]
                self.n_failed_scurves_0 = self.n_enabled_pixels_0 - len(self.ThresholdMap_0[self.ThresholdMap_0 != 0])
                self.ThresholdMap_1 = root_1.ThresholdMap[:, :]
                self.Chi2Map_1 = root_1.Chi2Map[:, :]
                self.NoiseMap_1 = root_1.NoiseMap[:]
                self.n_failed_scurves_1 = self.n_enabled_pixels_1 - len(self.ThresholdMap_1[self.ThresholdMap_1 != 0])
                if self._module_type == 'quad':
                    self.ThresholdMap_2 = root_2.ThresholdMap[:, :]
                    self.Chi2Map_2 = root_2.Chi2Map[:, :]
                    self.NoiseMap_2 = root_2.NoiseMap[:]
                    self.n_failed_scurves_2 = self.n_enabled_pixels_2 - len(self.ThresholdMap_2[self.ThresholdMap_2 != 0])
                    self.ThresholdMap_3 = root_3.ThresholdMap[:, :]
                    self.Chi2Map_3 = root_3.Chi2Map[:, :]
                    self.NoiseMap_3 = root_3.NoiseMap[:]
                    self.n_failed_scurves_3 = self.n_enabled_pixels_3 - len(self.ThresholdMap_3[self.ThresholdMap_3 != 0])

        if self.run_config_0['scan_id'] in ['tot_calibration']:
            self.HistTotCal_0 = root_0.HistTotCal[:]
            self.HistTotCalMean_0 = root_0.HistTotCalMean[:]
            self.HistTotCalStd_0 = root_0.HistTotCalStd[:]
            self.TOThist_0 = root_0.TOThist[:]
            self.HistTotCal_1 = root_1.HistTotCal[:]
            self.HistTotCalMean_1 = root_1.HistTotCalMean[:]
            self.HistTotCalStd_1 = root_1.HistTotCalStd[:]
            self.TOThist_1 = root_1.TOThist[:]
            if self._module_type == 'quad':
                self.HistTotCal_2 = root_2.HistTotCal[:]
                self.HistTotCalMean_2 = root_2.HistTotCalMean[:]
                self.HistTotCalStd_2 = root_2.HistTotCalStd[:]
                self.TOThist_2 = root_2.TOThist[:]
                self.HistTotCal_3 = root_3.HistTotCal[:]
                self.HistTotCalMean_3 = root_3.HistTotCalMean[:]
                self.HistTotCalStd_3 = root_3.HistTotCalStd[:]
                self.TOThist_3 = root_3.TOThist[:]

        if self.run_config_0['scan_id'] in ['hitor_calibration']:
            self.HistTdc_0 = root_0.hist_2d_tdc_vcal[:]
            self.HistTotCalMean_0 = root_0.hist_tot_mean[:]
            self.HistTotCalStd_0 = root_0.hist_tot_std[:]
            self.HistTdcCalMean_0 = root_0.hist_tdc_mean[:]
            self.HistTdcCalStd_0 = root_0.hist_tdc_std[:]
            self.lookup_table_0 = root_0.lookup_table[:]
            self.HistTdc_1 = root_1.hist_2d_tdc_vcal[:]
            self.HistTotCalMean_1 = root_1.hist_tot_mean[:]
            self.HistTotCalStd_1 = root_1.hist_tot_std[:]
            self.HistTdcCalMean_1 = root_1.hist_tdc_mean[:]
            self.HistTdcCalStd_1 = root_1.hist_tdc_std[:]
            self.lookup_table_1 = root_1.lookup_table[:]
            if self._module_type == 'quad':
                self.HistTdc_2 = root_2.hist_2d_tdc_vcal[:]
                self.HistTotCalMean_2 = root_2.hist_tot_mean[:]
                self.HistTotCalStd_2 = root_2.hist_tot_std[:]
                self.HistTdcCalMean_2 = root_2.hist_tdc_mean[:]
                self.HistTdcCalStd_2 = root_2.hist_tdc_std[:]
                self.lookup_table_2 = root_2.lookup_table[:]
                self.HistTdc_3 = root_3.hist_2d_tdc_vcal[:]
                self.HistTotCalMean_3 = root_3.hist_tot_mean[:]
                self.HistTotCalStd_3 = root_3.hist_tot_std[:]
                self.HistTdcCalMean_3 = root_3.hist_tdc_mean[:]
                self.HistTdcCalStd_3 = root_3.hist_tdc_std[:]
                self.lookup_table_3 = root_3.lookup_table[:]

        if self.run_config_0['scan_id'] == 'dac_linearity_scan':
            self.DAC_data_0 = root_0.dac_data[:]
            self.DAC_data_1 = root_1.dac_data[:]
            if self._module_type == 'quad':
                self.DAC_data_2 = root_2.dac_data[:]
                self.DAC_data_3 = root_3.dac_data[:]

        if self.run_config_0['scan_id'] in ['bump_connection_source_scan', 'bump_connection_crosstalk_scan', 'bump_connection_bias_thr_shift_scan']:
            self.bump_bonds_connection_map_0 = root_0.BumpConnectivityMap[:]
            self.bump_bonds_connection_map_1 = root_1.BumpConnectivityMap[:]
            if self._module_type == 'quad':
                self.bump_bonds_connection_map_2 = root_2.BumpConnectivityMap[:]
                self.bump_bonds_connection_map_3 = root_3.BumpConnectivityMap[:]

        try:
            self.Hits_0 = root_0.Hits[:]
            self.Hits_1 = root_1.Hits[:]
            if self._module_type == 'quad':
                self.Hits_2 = root_2.Hits[:]
                self.Hits_3 = root_3.Hits[:]
        except tb.NoSuchNodeError:
            self.Hits_0 = None
            self.Hits_1 = None
            if self._module_type == 'quad':
                self.Hits_2 = None
                self.Hits_3 = None

        try:
            self.Cluster_0 = root_0.Cluster[:]
            self.HistClusterSize_0 = root_0.HistClusterSize[:]
            self.HistClusterShape_0 = root_0.HistClusterShape[:]
            self.HistClusterTot_0 = root_0.HistClusterTot[:]
            self.clustered_0 = True
            self.Cluster_1 = root_1.Cluster[:]
            self.HistClusterSize_1 = root_1.HistClusterSize[:]
            self.HistClusterShape_1 = root_1.HistClusterShape[:]
            self.HistClusterTot_1 = root_1.HistClusterTot[:]
            if self._module_type == 'quad':
                self.Cluster_2 = root_2.Cluster[:]
                self.HistClusterSize_2 = root_2.HistClusterSize[:]
                self.HistClusterShape_2 = root_2.HistClusterShape[:]
                self.HistClusterTot_2 = root_2.HistClusterTot[:]
                self.Cluster_3 = root_3.Cluster[:]
                self.HistClusterSize_3 = root_3.HistClusterSize[:]
                self.HistClusterShape_3 = root_3.HistClusterShape[:]
                self.HistClusterTot_3 = root_3.HistClusterTot[:]
        except tb.NoSuchNodeError:
            self.Cluster_0 = None

        try:
            in_file_0.close()
            in_file_1.close()
            in_file_2.close()
            in_file_3.close()
        except Exception:
            pass

        self.run_config = self.run_config_0
        self.scan_config = self.scan_config_0
        if 'trigger_pattern' not in self.scan_config.keys():
            self.scan_config['trigger_pattern'] = '0b11111111111111111111111111111111'

        if self._module_type == 'dual':

            self.scan_params = self.scan_params_0
            if np.any(self.scan_params_0 != self.scan_params_1):
                self.log.warning('Scan parameters differ between chips!')

            if self.run_config_0['scan_id'] not in ['dac_linearity_scan', 'sensor_iv_scan', 'seu_test']:
                if self.enable_mask_0 is not None:
                    self.enable_mask = self._concatenate_maps(self.enable_mask_0, self.enable_mask_1)
                else:
                    self.enable_mask = None
                self.n_enabled_pixels = self.n_enabled_pixels_0 + self.n_enabled_pixels_1

                if self.tdac_mask_0 is not None:
                    self.tdac_mask = self._concatenate_maps(self.tdac_mask_0, self.tdac_mask_1)
                else:
                    self.tdac_mask = None

            if self.run_config_0['scan_id'] not in ['sensor_iv_scan']:
                self.calibration = {}
                for key in self.calibration_0.keys():
                    # Calculate "average calibration"
                    self.calibration[key] = (self.calibration_0[key] + self.calibration_1[key]) / 2.

            if self.run_config_0['scan_id'] not in ['dac_linearity_scan', 'sensor_iv_scan', 'seu_test']:
                if self.HistTdcStatus_0 is not None:
                    self.HistTdcStatus = self.HistTdcStatus_0 + self.HistTdcStatus_1
                else:
                    self.HistTdcStatus = None
                self.HistEventStatus = self.HistEventStatus_0 + self.HistEventStatus_1
                self.HistOcc = self._concatenate_maps(self.HistOcc_0, self.HistOcc_1)
                self.HistTot = self._concatenate_maps(self.HistTot_0, self.HistTot_1)
                self.HistRelBCID = self._concatenate_maps(self.HistRelBCID_0, self.HistRelBCID_1)
                self.HistBCIDError = self.HistBCIDError_0 + self.HistBCIDError_1
                self.HistTrigID = self._concatenate_maps(self.HistTrigID_0, self.HistTrigID_1)
                if self.run_config_0['scan_id'] in ['threshold_scan', 'global_threshold_tuning', 'in_time_threshold_scan', 'fast_threshold_scan', 'crosstalk_scan']:
                    self.ThresholdMap = self._concatenate_maps(self.ThresholdMap_0, self.ThresholdMap_1)
                    self.Chi2Map = self._concatenate_maps(self.Chi2Map_0, self.Chi2Map_1)
                    self.NoiseMap = self._concatenate_maps(self.NoiseMap_0, self.NoiseMap_1)
                    self.n_failed_scurves = self.n_failed_scurves_0 + self.n_failed_scurves_1

            if self.run_config_0['scan_id'] in ['tot_calibration']:
                self.HistTotCal = self._concatenate_maps(self.HistTotCal_0, self.HistTotCal_1)
                self.HistTotCalMean = self._concatenate_maps(self.HistTotCalMean_0, self.HistTotCalMean_1)
                self.HistTotCalStd = self._concatenate_maps(self.HistTotCalStd_0, self.HistTotCalStd_1)
                self.TOThist = self._concatenate_maps(self.TOThist_0, self.TOThist_1)

            if self.run_config_0['scan_id'] in ['hitor_calibration']:
                self.HistTdc = self.HistTdc_0 + self.HistTdc_1
                self.HistTotCalMean = self._concatenate_maps(self.HistTotCalMean_0, self.HistTotCalMean_1)
                self.HistTotCalStd = self._concatenate_maps(self.HistTotCalStd_0, self.HistTotCalStd_1)
                self.HistTdcCalMean = self._concatenate_maps(self.HistTdcCalMean_0, self.HistTdcCalMean_1)
                self.HistTdcCalStd = self._concatenate_maps(self.HistTdcCalStd_0, self.HistTdcCalStd_1)
                self.lookup_table = self._concatenate_maps(self.lookup_table_0, self.lookup_table_1)

            if self.run_config_0['scan_id'] in ['bump_connection_source_scan', 'bump_connection_crosstalk_scan', 'bump_connection_bias_thr_shift_scan']:
                self.bump_bonds_connection_map = self._concatenate_maps(self.bump_bonds_connection_map_0, self.bump_bonds_connection_map_1)

            if self.Cluster_0 is not None:
                self.HistClusterSize = self.HistClusterSize_0 + self.HistClusterSize_1
                self.HistClusterShape = self.HistClusterShape_0 + self.HistClusterShape_1
                self.HistClusterTot = self.HistClusterTot_0 + self.HistClusterTot_1
            else:
                self.HistClusterSize = None
                self.HistClusterShape = None
                self.HistClusterTot = None
            self.Cluster = None

        elif self._module_type == 'quad':

            self.scan_params = self.scan_params_0
            if np.any(np.logical_or(np.logical_or(self.scan_params_0 != self.scan_params_1, self.scan_params_0 != self.scan_params_2), self.scan_params_0 != self.scan_params_3)):
                self.log.warning('Scan parameters differ between chips!')

            if self.run_config_0['scan_id'] not in ['dac_linearity_scan', 'sensor_iv_scan', 'seu_test']:
                if self.enable_mask_0 is not None:
                    self.enable_mask = self._concatenate_maps(self.enable_mask_0, self.enable_mask_1, self.enable_mask_2, self.enable_mask_3)
                else:
                    self.enable_mask = None
                self.n_enabled_pixels = self.n_enabled_pixels_0 + self.n_enabled_pixels_1 + self.n_enabled_pixels_2 + self.n_enabled_pixels_3

                if self.tdac_mask_0 is not None:
                    self.tdac_mask = self._concatenate_maps(self.tdac_mask_0, self.tdac_mask_1, self.tdac_mask_2, self.tdac_mask_3)
                else:
                    self.tdac_mask = None

            if self.run_config_0['scan_id'] not in ['sensor_iv_scan']:
                self.calibration = {}
                for key in self.calibration_0.keys():
                    # Calculate "average calibration"
                    self.calibration[key] = (self.calibration_0[key] + self.calibration_1[key] + self.calibration_2[key] + self.calibration_3[key]) / 4.

            if self.run_config_0['scan_id'] not in ['dac_linearity_scan', 'sensor_iv_scan', 'seu_test']:
                if self.HistTdcStatus_0 is not None:
                    self.HistTdcStatus = self.HistTdcStatus_0 + self.HistTdcStatus_1 + self.HistTdcStatus_2 + self.HistTdcStatus_3
                else:
                    self.HistTdcStatus = None
                self.HistEventStatus = self.HistEventStatus_0 + self.HistEventStatus_1 + self.HistEventStatus_2 + self.HistEventStatus_3
                self.HistOcc = self._concatenate_maps(self.HistOcc_0, self.HistOcc_1, self.HistOcc_2, self.HistOcc_3)
                self.HistTot = self._concatenate_maps(self.HistTot_0, self.HistTot_1, self.HistTot_2, self.HistTot_3)
                self.HistRelBCID = self._concatenate_maps(self.HistRelBCID_0, self.HistRelBCID_1, self.HistRelBCID_2, self.HistRelBCID_3)
                self.HistBCIDError = self.HistBCIDError_0 + self.HistBCIDError_1 + self.HistBCIDError_2 + self.HistBCIDError_3
                self.HistTrigID = self._concatenate_maps(self.HistTrigID_0, self.HistTrigID_1, self.HistTrigID_2, self.HistTrigID_3)
                if self.run_config_0['scan_id'] in ['threshold_scan', 'global_threshold_tuning', 'in_time_threshold_scan', 'fast_threshold_scan', 'crosstalk_scan']:
                    self.ThresholdMap = self._concatenate_maps(self.ThresholdMap_0, self.ThresholdMap_1, self.ThresholdMap_2, self.ThresholdMap_3)
                    self.Chi2Map = self._concatenate_maps(self.Chi2Map_0, self.Chi2Map_1, self.Chi2Map_2, self.Chi2Map_3)
                    self.NoiseMap = self._concatenate_maps(self.NoiseMap_0, self.NoiseMap_1, self.NoiseMap_2, self.NoiseMap_3)
                    self.n_failed_scurves = self.n_failed_scurves_0 + self.n_failed_scurves_1 + self.n_failed_scurves_2 + self.n_failed_scurves_3

            if self.run_config_0['scan_id'] in ['tot_calibration']:
                self.HistTotCal = self._concatenate_maps(self.HistTotCal_0, self.HistTotCal_1, self.HistTotCal_2, self.HistTotCal_3)
                self.HistTotCalMean = self._concatenate_maps(self.HistTotCalMean_0, self.HistTotCalMean_1, self.HistTotCalMean_2, self.HistTotCalMean_3)
                self.HistTotCalStd = self._concatenate_maps(self.HistTotCalStd_0, self.HistTotCalStd_1, self.HistTotCalStd_2, self.HistTotCalStd_3)
                self.TOThist = self._concatenate_maps(self.TOThist_0, self.TOThist_1, self.TOThist_2, self.TOThist_3)

            if self.run_config_0['scan_id'] in ['hitor_calibration']:
                self.HistTdc = self.HistTdc_0 + self.HistTdc_1 + self.HistTdc_2 + self.HistTdc_3
                self.HistTotCalMean = self._concatenate_maps(self.HistTotCalMean_0, self.HistTotCalMean_1, self.HistTotCalMean_2, self.HistTotCalMean_3)
                self.HistTotCalStd = self._concatenate_maps(self.HistTotCalStd_0, self.HistTotCalStd_1, self.HistTotCalStd_2, self.HistTotCalStd_3)
                self.HistTdcCalMean = self._concatenate_maps(self.HistTdcCalMean_0, self.HistTdcCalMean_1, self.HistTdcCalMean_2, self.HistTdcCalMean_3)
                self.HistTdcCalStd = self._concatenate_maps(self.HistTdcCalStd_0, self.HistTdcCalStd_1, self.HistTdcCalStd_2, self.HistTdcCalStd_3)
                self.lookup_table = self._concatenate_maps(self.lookup_table_0, self.lookup_table_1, self.lookup_table_2, self.lookup_table_3)

            if self.run_config_0['scan_id'] in ['bump_connection_source_scan', 'bump_connection_crosstalk_scan', 'bump_connection_bias_thr_shift_scan']:
                self.bump_bonds_connection_map = self._concatenate_maps(self.bump_bonds_connection_map_0, self.bump_bonds_connection_map_1, self.bump_bonds_connection_map_2, self.bump_bonds_connection_map_3)

            if self.Cluster_0 is not None:
                self.HistClusterSize = self.HistClusterSize_0 + self.HistClusterSize_1 + self.HistClusterSize_2 + self.HistClusterSize_3
                self.HistClusterShape = self.HistClusterShape_0 + self.HistClusterShape_1 + self.HistClusterShape_2 + self.HistClusterShape_3
                self.HistClusterTot = self.HistClusterTot_0 + self.HistClusterTot_1 + self.HistClusterTot_2 + self.HistClusterTot_3
            else:
                self.HistClusterSize = None
                self.HistClusterShape = None
                self.HistClusterTot = None
            self.Cluster = None

    ''' User callable plotting functions '''

    def create_standard_plots(self):
        self.log.info('Creating selected plots...')
        self.create_module_parameter_page()
        if self.run_config['scan_id'] == 'dac_linearity_scan':
            self.create_dac_linearity_plot()
        else:
            self.create_event_status_plot()
            self.create_occupancy_map()
            if self.run_config['scan_id'] in ['source_scan', 'ext_trigger_scan']:
                self.create_fancy_occupancy()
            self.create_tot_plot()
            if self.run_config['scan_id'] in ['threshold_scan', 'in_time_threshold_scan', 'hitor_calibration', 'tot_calibration', 'fast_threshold_scan']:
                self.create_tot_hist()
                self.create_rel_bcid_hist()
            self.create_tot_map()
            self.create_rel_bcid_plot()
            self.create_rel_bcid_map()
            self.create_bcid_error_plot()
            self.create_trigger_id_map()
            if self.run_config['scan_id'] in ['analog_scan', 'threshold_scan', 'fast_threshold_scan', 'in_time_threshold_scan', 'noise_occupancy_scan', 'global_threshold_tuning', 'ext_trigger_scan', 'source_scan', 'crosstalk_scan']:
                self.create_tdac_plot()
                self.create_tdac_map()
                self.create_hit_pix_plot()
            if self.run_config['scan_id'] in ['threshold_scan', 'in_time_threshold_scan', 'fast_threshold_scan']:
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_stacked_threshold_plot()
                self.create_threshold_map()
                self.create_noise_plot()
                self.create_noise_map()
            if self.run_config['scan_id'] == 'global_threshold_tuning':
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_threshold_map()
                self.create_noise_plot()
                self.create_noise_map()
            if self.run_config['scan_id'] == 'tot_calibration':
                self.create_tot_perpixel_plot()
            if self.run_config['scan_id'] == 'hitor_calibration':
                self.create_tdc_hist()
                self.create_tdc_tot_perpixel_plot()
            if self.run_config['scan_id'] == 'crosstalk_scan':
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_stacked_threshold_plot()
                self.create_threshold_map()
            if self.run_config_0['scan_id'] in ['bump_connection_source_scan', 'bump_connection_crosstalk_scan', 'bump_connection_bias_thr_shift_scan']:
                self._plot_bump_connectivity_map(self.bump_bonds_connection_map)
                conn_state_count = dict(zip(*np.unique(self.bump_bonds_connection_map, return_counts=True)))
                num_connected_bumps = conn_state_count.get(1.0, 0)
                num_poorly_connected_bumps = conn_state_count.get(0.5, 0)
                num_disconnected_bumps = conn_state_count.get(0.0, 0)
                num_unknown_state_bumps = conn_state_count.get(-12.0, 0)
                self._plot_bump_connectivity_hist(connected_bumps=num_connected_bumps, num_poorly_connected_bumps=num_poorly_connected_bumps, disconnected_bumps=num_disconnected_bumps, unknown_state_bumps=num_unknown_state_bumps)

            if self.clustered:
                self.create_cluster_tot_plot()
                self.create_cluster_shape_plot()
                self.create_cluster_size_plot()
            if self.HistTdcStatus is not None:  # Check if TDC analysis is activated.
                self.create_tdc_status_plot()

    def create_tdac_map(self):
        try:
            if self._module_type == 'dual':
                flavors = [rd53a.get_flavor(self.scan_config_0['stop_column'] - 1),
                           rd53a.get_flavor(self.scan_config_1['stop_column'] - 1)]
            else:
                flavors = [rd53a.get_flavor(self.scan_config_0['stop_column'] - 1),
                           rd53a.get_flavor(self.scan_config_1['stop_column'] - 1),
                           rd53a.get_flavor(self.scan_config_2['stop_column'] - 1),
                           rd53a.get_flavor(self.scan_config_3['stop_column'] - 1)]
            if 'SYNC' in flavors:
                return

            min_tdac = +16
            max_tdac = -16
            for flavor in flavors:
                t_min_tdac, t_max_tdac, _, _ = rd53a.get_tdac_range(flavor)
                if t_max_tdac > max_tdac:
                    max_tdac = t_max_tdac
                if t_min_tdac < min_tdac:
                    min_tdac = t_min_tdac
            mask = self.enable_mask.copy()
            self._plot_fancy_occupancy(hist=np.ma.masked_array(self.tdac_mask, mask).T,
                                       title='TDAC map',
                                       z_label='TDAC',
                                       z_min=min(min_tdac, max_tdac),
                                       z_max=max(min_tdac, max_tdac),
                                       log_z=False,
                                       norm_projection=True)
        except Exception:
            self.log.error('Could not create TDAC map!')

    def create_module_parameter_page(self):
        try:
            self._plot_module_parameter_page()
        except Exception:
            self.log.error('Could not create parameter page!')

    def _plot_module_parameter_page(self):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.axis('off')
        if self.chip_type.lower() == 'rd53a':
            logo = plt.imread(os.path.join(os.path.dirname(__file__), '..', 'chips', 'RD53A_logo.bmp'))
            logo = logo[::4, ::4]
        if self.chip_type.lower() in ['itkpixv1', 'crocv1']:
            logo = plt.imread(os.path.join(os.path.dirname(__file__), '..', 'chips', 'RD53B_logo.bmp'))
            logo = logo[::8, ::8]
        fig.figimage(logo, fig.bbox.xmax - fig.get_figwidth() - 130, fig.bbox.ymax - fig.get_figheight() - 105)

        scan_id = self.run_config['scan_id']
        run_name = self.run_config['run_name']
        module = self.run_config['module']
        if module.startswith('module_'):    # Nice formatting
            module = module.split('module_')[1]

        sw_ver = self.run_config['software_version']
        timestamp = self.run_config['run_name'][0:15]

        text = 'This is a bdaq53 {0} for module {1}.\nRun {2} was started {3}.'.format(scan_id, module, run_name, timestamp)

        ax.text(0.01, 0.75, text, fontsize=10)
        ax.text(-0.1, -0.11, 'Software version: {0}'.format(sw_ver), fontsize=3)

        if scan_id in ['threshold_scan', 'fast_threshold_scan', 'autorange_threshold_scan', 'crosstalk_scan']:
            txt = 'Charge calibration: $y \\; [e^-] = x \\; [\\Delta VCAL] \\cdot ({0:1.2f} \\pm {1:1.2f}) \\; [\\frac{{e^-}}{{\\Delta VCAL}}] + ({2:1.0f} \\pm {3:1.2f}) \\; [e^-]$'.format(self.calibration['e_conversion_slope'],
                                                                                                                                                                                             self.calibration['e_conversion_slope_error'],
                                                                                                                                                                                             self.calibration['e_conversion_offset'],
                                                                                                                                                                                             self.calibration['e_conversion_offset_error'])
            ax.text(0.01, 0.02, txt, fontsize=6)

        if scan_id not in ['sensor_iv_scan']:
            if 'maskfile' in self.scan_config.keys() and self.scan_config['maskfile'] is not None and not self.scan_config['maskfile'] == 'None':
                ax.text(0.01, -0.05, 'Maskfile:\n{0}'.format(self.scan_config['maskfile']), fontsize=6)

        chip_sn_dict = {}

        chip_sn_dict['FE0'] = self.run_config_0['chip_sn']
        chip_sn_dict['FE1'] = self.run_config_1['chip_sn']
        if self._module_type == 'quad':
            chip_sn_dict['FE2'] = self.run_config_2['chip_sn']
            chip_sn_dict['FE3'] = self.run_config_3['chip_sn']

        tb_list = []
        for i in range(len(self.scan_config)):
            try:
                key1 = list(self.scan_config.keys())[i]
                value1 = self.scan_config[key1]
                if key1 == 'trigger_pattern':  # Nice formatting
                    value1 = hex(value1)
            except IndexError:
                key1 = ''
                value1 = ''
            try:
                key2 = list(chip_sn_dict.keys())[i]
                value2 = chip_sn_dict[key2]
            except IndexError:
                key2 = ''
                value2 = ''

            tb_list.append([key1, value1, '', key2, value2])

        widths = [0.18, 0.10, 0.03, 0.10, 0.10]
        labels = ['Scan config', 'Value', '', '', 'chip_sn']
        table = ax.table(cellText=tb_list, colWidths=widths, colLabels=labels, cellLoc='left', loc='center')
        table.scale(0.8, 0.8)
        table.auto_set_font_size(False)

        for key, cell in table.get_celld().items():
            cell.set_fontsize(3.5)
            row, col = key
            if row == 0:
                cell.set_color('#ffb300')
                cell.set_fontsize(5)
            if col in [2, 5, 8, 11]:
                cell.set_color('white')
            if col in [1, 4, 7, 10, 13]:
                cell._loc = 'center'

        self._save_plots(fig, suffix='parameter_page')

    ''' Internal functions '''

    def _concatenate_maps(self, map_0, map_1, map_2=None, map_3=None):
        '''
            Combine maps of multiple chips on a module in order to make a single 2D plot for the whole module
            The resulting plot orientation for the dual modules is:
             __________________________________________
             |...................||...................|
             |......Chip 1.......||......Chip 1.......|
             |...................||...................|
             |///////////////////||///////////////////|
             |_|_|_|_|_|_|_|_|_|_||_|_|_|_|_|_|_|_|_|_|

            The resulting plot orientation for the quad modules (with chip numbering like on slide number
            seven in this presentation: https://indico.cern.ch/event/814971/contributions/3504832/) is:

             |–––––––––––––––––||–––––––––––––––––|
             |–|/|. . . . . . .||. . . . . . .|/|–|
             |–|/|. . . C . . .||. . . C . . .|/|–|
             |–|/|. . . h . . .||. . . h . . .|/|–|
             |–|/|. . . i . . .||. . . i . . .|/|–|
             |–|/|. . . p . . .||. . . p . . .|/|–|
             |–|/|. . . . . . .||. . . . . . .|/|–|
             |–|/|. . . 1 . . .||. . . 4 . . .|/|–|
             |–|/|. . . . . . .||. . . . . . .|/|–|
             |–––––––––––––––––||–––––––––––––––––|
             |–––––––––––––––––||–––––––––––––––––|
             |–|/|. . . . . . .||. . . . . . .|/|–|
             |–|/|. . . C . . .||. . . C . . .|/|–|
             |–|/|. . . h . . .||. . . h . . .|/|–|
             |–|/|. . . i . . .||. . . i . . .|/|–|
             |–|/|. . . p . . .||. . . p . . .|/|–|
             |–|/|. . . . . . .||. . . . . . .|/|–|
             |–|/|. . . 2 . . .||. . . 3 . . .|/|–|
             |–|/|. . . . . . .||. . . . . . .|/|–|
             |–––––––––––––––––||–––––––––––––––––|
         '''
        if self._module_type == 'dual':
            return np.concatenate((map_0, map_1), axis=0)
        else:
            return np.concatenate((np.flip(np.swapaxes(np.concatenate((map_0, map_1), axis=0), 0, 1), 0),
                                   np.flip(np.swapaxes(np.concatenate((map_3, map_2), axis=0), 0, 1), 1)), axis=0)

    def _save_plots(self, fig, suffix=None, tight=True):
        increase_count = False
        bbox_inches = 'tight' if tight else ''
        if suffix is None:
            suffix = str(self.plot_cnt)
        self.out_file.savefig(fig, bbox_inches=bbox_inches)
        if self.save_png:
            fig.savefig(self.filename[:-4] + '_' +
                        suffix + '.png', bbox_inches=bbox_inches, dpi=200)
            increase_count = True
        if self.save_single_pdf:
            fig.savefig(self.filename[:-4] + '_' +
                        suffix + '.pdf', bbox_inches=bbox_inches)
            increase_count = True
        if increase_count:
            self.plot_cnt += 1

    def _add_text(self, fig):
        fig.subplots_adjust(top=0.85)
        y_coord = 0.92
        if self.qualitative:
            fig.text(0.1, y_coord, 'RD53A qualitative', fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
            fig.text(0.7, y_coord, 'Module: 0x0000', fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
        else:
            fig.text(0.1, y_coord, 'RD53A {0}'.format(self.level), fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
            try:
                module = self.run_config['module']
                if module.startswith('module_'):  # Nice formatting
                    module = module.split('module_')[1]
                fig.text(0.7, y_coord, 'Module: {0}'.format(module), fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
            except KeyError:  # Old file where module name was not defined (before 2020)
                self.log.warning('Module name not defined in the interpreted data file.')
                fig.text(0.7, y_coord, 'Module: -', fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
        if self.internal:
            fig.text(0.1, 1, 'RD53 Internal', fontsize=16, color='r', rotation=45,
                     bbox=dict(boxstyle='round', facecolor='white', edgecolor='red', alpha=0.7), transform=fig.transFigure)


if __name__ == "__main__":

    # Choose 'dual' or 'quad'
    module_type = 'dual'

    # Filename of interpreted data files of same module without path and extension (ScanBase.run_name)
    interpreted_data_filename = '20200101_120000_digital_scan_interpreted'

    # Common module directory with output of all chips in subdirectories
    module_path = '...output_data/module_0'

    if module_type == 'dual':

        chip_0_rel_path = '0x0001'  # Relative path to chip number 0 (chip SN)
        chip_1_rel_path = '0x0002'  # Relative path to chip number 1 (chip SN)

        filename_0 = os.path.join(module_path, chip_0_rel_path, interpreted_data_filename + '.h5')
        filename_1 = os.path.join(module_path, chip_1_rel_path, interpreted_data_filename + '.h5')
        filename_2 = None
        filename_3 = None

    elif module_type == 'quad':

        chip_0_rel_path = '0x0001'  # Relative path to chip number 0 (chip SN)
        chip_1_rel_path = '0x0002'  # Relative path to chip number 1 (chip SN)
        chip_2_rel_path = '0x0003'  # Relative path to chip number 2 (chip SN)
        chip_3_rel_path = '0x0004'  # Relative path to chip number 3 (chip SN)

        filename_0 = os.path.join(module_path, chip_0_rel_path, interpreted_data_filename + '.h5')
        filename_1 = os.path.join(module_path, chip_1_rel_path, interpreted_data_filename + '.h5')
        filename_2 = os.path.join(module_path, chip_2_rel_path, interpreted_data_filename + '.h5')
        filename_3 = os.path.join(module_path, chip_3_rel_path, interpreted_data_filename + '.h5')

    with ModulePlotting(analyzed_data_file_0=filename_0, analyzed_data_file_1=filename_1, analyzed_data_file_2=filename_2, analyzed_data_file_3=filename_3,
                        mask_noisy_pixels=False,
                        noisy_pixel_threshold=1e3,
                        pdf_file=None,  # Path to output pdf file. If None, the name of the analyzed data file is used
                        level='',  # Level of the results. For example 'preliminary'
                        qualitative=False,  # Create qualitative plots without numbers on axes
                        internal=False,  # Write 'RD53 internal' on every plot
                        save_single_pdf=False,  # Save every plot as a single pdf file in addition to the one output pdf file
                        save_png=True) as p:  # Save every plot as a single png file in addition to the one output pdf file

        p.create_standard_plots()
