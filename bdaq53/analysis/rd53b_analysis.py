#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Collection of functions specific to the RD53B analysis
'''
import numpy as np
import numba
from collections import OrderedDict
import bdaq53.analysis.analysis_utils as au

# Word defines
USERK_FRAME_ID = 0x01000000
HEADER_ID = 0x00010000
NEW_STREAM_ID = 0x00008000  # Detect new stream
TRIGGER_ID = 0x80000000
TDC_ID_0 = 0x10000000
TDC_ID_1 = 0x20000000
TDC_ID_2 = 0x30000000
TDC_ID_3 = 0x40000000

# Data Masks
TRG_MASK = 0x7FFFFFFF  # Trigger data (number and/or time stamp)
TDC_HEADER_MASK = 0xF0000000  # TDC data header (decodes to which region the TDC word belongs to)
TDC_TRIG_DIST_MASK = 0x0FF00000  # TDC trigger distance
TDC_TIMESTAMP_MASK = 0x0FFFF000  # TDC timestamp
TDC_VALUE_MASK = 0x00000FFF  # TDC value
TAG_MASK = 0xFF  # RD53B tag (8 bit), location depends on data
CCOL_MASK = 0x3F  # RD53B core column (6 bit), location depends on data
ISLAST_MASK = 0x1  # RD53B islast bit, location depends on data
ISNEIGHBOUR_MASK = 0x1  # RD53B isneighbour bit, location depends on data
QROW_MASK = 0xFF  # RD53B qrow (8 bit), location depends on data
TOT_MASK = 0xF  # RD53B (regular, no precision tot) tot (4 bit), location depends on data


@numba.njit
def is_new_event(n_event_header, n_trigger, start_tag, tag, prev_tag, last_tag, trg_header,
                 event_status, is_tag_offset, method):
    ''' Detect new event by different methods.

        Note:
        -----
        Checks for new events are rather complex to handle all possible
        cases. They are tested with high coverage in unit tests, do not
        make quick chances without checking that event building still works!

        Methods to do event alignment, create new event if:
        0: - Using tags: Not yet implemented
        1: - Previous data word is TLU trigger word. This is expected to be at the beginning of an event.
             OR
              - Number of event header (n_event_header) exceeds the number of sub-triggers used during data taking (n_trigger)
                This is a fallback to event header alignment and needed for rare cases where more BCIDs are readout.
        2: - Previous data word is TLU trigger word. This is expected to be at the beginning of an event. No error
             checks on FE data is applied (like number of event header). Needed for event reconstruction from TB data with sync flavor.
             This flavor has a lot of bugs in the digital part.
    '''

    if method == 1:
        if not trg_header:
            # Fallback to event header alginment. Needed for missing or not recognized
            # trigger words.
            if n_event_header >= n_trigger:
                return True
            else:
                return False
        else:
            return True
    elif method == 2:
        # Every trigger creates a new event, independent how FE data looks like
        if not trg_header:
            return False
        else:
            return True
    else:
        raise


@numba.njit()
def interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_trigger_id, hist_event_status,
                   hist_tdc_status, hist_tdc_value, hist_bcid_error,
                   hist_ptot, hist_ptoa,
                   scan_param_id, event_number=0,
                   trig_pattern=0b11111111111111111111111111111111,
                   align_method=0,
                   prev_tag=-1, prev_trg_number=-1, analyze_tdc=False,
                   use_tdc_trigger_dist=False, last_chunk=False, rx_id=0, trigger_table_stretched=None, ptot_table_stretched=np.array([-1])):

    is_tag_offset = False
    trg_header = False
    data_out_i = 0

    # Per call variables
    n_trigger = au.number_of_set_bits(trig_pattern)
    # Calculate effective not byte aligned pattern (= ommit leading zeros)
    eff_trig_patt = trig_pattern
    for _ in range(32):
        if eff_trig_patt & 0x80000000:
            break
        eff_trig_patt = eff_trig_patt << 1

    # Hit buffer to store actual event hits, needed to set parameters
    # calculated at the end of an event and not available before
    hit_buffer_i = 0  # Index of entry of hit buffer
    hit_buffer = np.zeros_like(hits)
    last_word_index = 0  # Last raw data index of full event

    # Per stream variables
    next_word_is_tag = False
    next_word_is_ccol = False
    next_word_is_last_neighbor = False
    next_word_is_qrow = False
    next_word_is_hmap = False
    next_word_is_tot = False
    found_start_of_stream = False
    old_word = 0  # Store left-over raw data word. Needed since RD53B data words have variable length, thus FPGA-splitting of raw data words (16 bit?) can lead to incomplete raw data words
    ccol = 0  # core column
    is_last = 0
    is_neighbor = 0
    q_row = 0  # quarter core
    q_row_array = np.full(54, 0)
    hmap_word = 0
    hits_hmap = 0
    tot = np.full(16, -1)

    # Per event variables
    tag = -1
    start_tag = -1  # First tag of new event
    prev_tag = -1  # Tag before the expected tag
    last_tag = -1
    n_tags = 0  # Number of tags in event
    event_status = 0
    trg_number = 0
    tdc_word_count = 0  # TDC word counter per event
    tdc_status = np.full(shape=(4,), fill_value=0, dtype=np.uint8)  # Status for each TDC line
    tdc_status_buffer = np.full(shape=(4,), fill_value=0, dtype=np.uint8)  # Array in order to buffer TDC status
    tdc_value = np.full(shape=(4,), fill_value=2 ** 16 - 1, dtype=np.uint16)  # TDC value for each Hitor line
    tdc_value_buffer = np.full(shape=(4,), fill_value=2 ** 16 - 1,
                               dtype=np.uint16)  # Array in order to buffer TDC values
    tdc_timestamp = np.zeros(shape=(4,),
                             dtype=np.uint16)  # TDC timestamp for each Hitor line. If use_trigger_dist is True, this will be trigger distance
    tdc_timestamp_buffer = np.zeros(shape=(4,), dtype=np.uint16)  # Array in order to buffer TDC timestamps
    tdc_word = np.full(shape=(4,), fill_value=False, dtype=np.bool_)  # Indicator for TDC word for each Hitor line
    event_status_tdc = 0  # Separate variable for TDC event status in order to buffer the event status of TDC (TDC comes before actual event if no external trigger word is provided)

    # Loop over raw data words
    for i in range(len(rawdata)):
        shift = 16

        if rawdata[i] & au.TRIGGER_HEADER:  # TLU word
            trg_word_id = i
            trg_number = rawdata[i] & au.TRG_MASK
            event_status |= au.E_EXT_TRG
            trg_header = True
            # Special case for first trigger word
            if (align_method == 1 or align_method == 2):
                if i == 0:
                    prev_trg_number = trg_number
                    trg_header = False
            continue

        if au.is_tdc_word(rawdata[i]) and analyze_tdc:  # Select first TDC word (no matter if it comes from TDC1 - 4)
            tdc_word_count += 1  # Increase TDC word counter (per event).
            tdc_index = (rawdata[i] & 0x70000000) >> 28  # Get index of TDC module (Hitor line).
            if tdc_word[tdc_index - 1]:  # If event has already TDC word from the actual tdc index, set TDC ambiguous status.
                if not use_tdc_trigger_dist:
                    # Set TDC status per TDC line.
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_AMBIGUOUS
                elif (au.TDC_TRIG_DIST_MASK & rawdata[i]) >> 20 < 254:  # In case of trigger distance measurement, a valid trigger distance (smaller than 254) defines the valid TDC word
                    if tdc_timestamp_buffer[tdc_index - 1] < 254:
                        # Set TDC ambiguous if have already a valid TDC word (trigger distance < 254)
                        tdc_status_buffer[tdc_index - 1] |= au.H_TDC_AMBIGUOUS
                    else:
                        # If still had no valid, update TDC word with the first valid one
                        tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TRIG_DIST_MASK & rawdata[i]) >> 20
                        tdc_value_buffer[tdc_index - 1] = rawdata[i] & au.TDC_VALUE_MASK
                        if tdc_value_buffer[tdc_index - 1] != 0xFFF and not (tdc_timestamp_buffer[tdc_index - 1] == 254 and use_tdc_trigger_dist):
                            # Un-set TDC overflow in case found valid TDC word
                            tdc_status_buffer[tdc_index - 1] = tdc_status_buffer[tdc_index - 1] & ~au.H_TDC_OVF
                        if tdc_value_buffer[tdc_index - 1] != 0 and not (tdc_timestamp_buffer[tdc_index - 1] == 255 and use_tdc_trigger_dist):
                            # Un-set TDC error in case found valid TDC word
                            tdc_status_buffer[tdc_index - 1] = tdc_status_buffer[tdc_index - 1] & ~au.H_TDC_ERR
            else:  # Set for the first TDC word the corresponding event status
                # Do not use event status directly, in order to buffer TDC event status, since it comes before
                # the actual event (before EH0).
                event_status_tdc = au.E_TDC
                tdc_word[tdc_index - 1] = True  # Set TDC indicator for actual TDC index to True
                tdc_value_buffer[tdc_index - 1] = rawdata[i] & au.TDC_VALUE_MASK  # Extract the TDC value from TDC word
                if use_tdc_trigger_dist:
                    # If use_tdc_trigger_dist is True, extract trigger distance (delay between Hitor and Trigger) from TDC word
                    tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TRIG_DIST_MASK & rawdata[i]) >> 20
                else:
                    # If use_tdc_trigger_dist is False, extract TDC timestamp from TDC word
                    tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TIMESTAMP_MASK & rawdata[i]) >> 12

                # Set error/overflow
                if tdc_value_buffer[tdc_index - 1] == 0xFFF or (
                        tdc_timestamp_buffer[tdc_index - 1] == 254 and use_tdc_trigger_dist):
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_OVF
                if tdc_value_buffer[tdc_index - 1] == 0 or (
                        tdc_timestamp_buffer[tdc_index - 1] == 255 and use_tdc_trigger_dist):
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_ERR

            if i == 0 and align_method == 0:  # Special case if first word is TDC word. Only possible if no external trigger word is provided.
                event_status |= event_status_tdc
                for index in range(4):
                    tdc_value[index] = tdc_value_buffer[index]
                    tdc_timestamp[index] = tdc_timestamp_buffer[index]
                    tdc_status[index] = tdc_status_buffer[index]
                tdc_word[:] = False
                event_status_tdc = 0

            continue

        # Aurora receiver ID encoded in header must match rx_id of currently analyzed chip
        if ((rawdata[i] >> 20) & 0xf) != rx_id:
            event_status |= au.E_INV_RX_ID
            continue
        else:
            word = rawdata[i] & 0x1ffff

        if (word & au.AURORA_HEADER) == au.USERK_FRAME_ID:  # skip USER_K frame
            event_status |= au.E_USER_K
            continue

        if rawdata[i] & HEADER_ID:
            word = word & 0xffff
            if rawdata[i] & NEW_STREAM_ID:  # Beginning of new stream
                # Set per stream variables
                found_start_of_stream = True  # Found start of stream
                next_word_is_tag = True  # Next word is tag. TODO: Need to check for R/O configuration. Depending on that, next word is tag (default) OR chip id and then tag.
                next_word_is_ccol = False
                next_word_is_last_neighbor = False
                next_word_is_qrow = False
                next_word_is_hmap = False
                next_word_is_tot = False
                old_word = 0
            else:  # Stream is still continuing
                word = word & 0x7fff
                if old_word == 0:
                    word += 2 ** 15
                shift = 15

        if au.get_length(old_word) < 47 and found_start_of_stream:
            word = (old_word << shift) + word  # Reassemble the complete raw data word.
            if au.get_length(word) < 16 and not rawdata[i] & HEADER_ID:
                word += 2 ** 16
        elif not found_start_of_stream:
            continue

        if next_word_is_tag:  # Extract tag
            if au.get_length(word) < 8:  # Check for complete 8 bit tag.
                old_word = word  # Store left over data word
                continue

            tag = word >> (au.get_length(word) - 8) & TAG_MASK
            word = au.rm_msb(word, 8)

            # Special cases for first event/tag
            if start_tag == -1:
                start_tag = tag
                if align_method == 0:
                    prev_trg_number = trg_number
                event_status |= event_status_tdc
                for index in range(4):
                    tdc_value[index] = tdc_value_buffer[index]
                    tdc_timestamp[index] = tdc_timestamp_buffer[index]
                    tdc_status[index] = tdc_status_buffer[index]
                tdc_word[:] = False
                event_status_tdc = 0
                tdc_value_buffer[:] = 2 ** 16 - 1
                tdc_timestamp_buffer[:] = 0
                tdc_status_buffer[:] = 0

            if is_new_event(n_tags, n_trigger, start_tag, tag, prev_tag, last_tag, trg_header, event_status, is_tag_offset, align_method):
                # Set event errors of old event
                if n_tags != n_trigger:
                    event_status |= au.E_STRUCT_WRONG
                if tag < 0:
                    event_status |= au.E_STRUCT_WRONG
                if trg_header and not au.check_difference(prev_trg_number, trg_number, 31):
                    event_status |= au.E_EXT_TRG_ERR
                # Handle case of too many event header than expected by setting
                # flag to check BCIDs in next event more carfully (allow to exceed n_triggers event header)
                if au.check_difference(last_tag, tag, 5):
                    is_tag_offset = True
                else:
                    is_tag_offset = False

                if len(trigger_table_stretched) > 1:
                    scan_param_id = trigger_table_stretched[trg_number]
                elif len(ptot_table_stretched) > 1:
                    scan_param_id = ptot_table_stretched[trg_number]['scan_param_id']

                data_out_i = au.build_event(hits, data_out_i,
                                            hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                            hist_event_status, hist_tdc_status, hist_tdc_value,
                                            hist_ptot, hist_ptoa,
                                            hit_buffer, hit_buffer_i, 0,  # start BCID is 0
                                            scan_param_id, event_status, tdc_status)
                event_number += 1

                # Calculate last_word_index in order to split properly the chunks
                if align_method == 2:
                    last_word_index = trg_word_id  # in case of force trg numer calculate offset always to last trigger word
                else:
                    # TODO: last word index should always be beginning of stream?
                    if not trg_header:
                        if analyze_tdc:  # Since TDC words come before the actual event (before EH 0) need to set last_word_index to first TDC word
                            last_word_index = i - tdc_word_count - 1  # to get first TDC word
                        else:
                            last_word_index = i - 1  # to get high word
                    else:
                        if analyze_tdc:
                            # FIXME: i - tdc_word_count - 2 should also work.
                            # But 1 of 1000000 it can happen that trigger word comes in between TDC words.
                            last_word_index = trg_word_id  # to get trigger word
                        else:
                            last_word_index = i - 2  # to get trigger word

                # Reset per event variables
                hit_buffer_i = 0
                start_tag = tag
                prev_trg_number = trg_number
                trg_header = False
                n_tags = 0

                # If old event struture is wrong this event is likely wrong too
                # Assume trigger ID error if no external trigger is available
                if (event_status & au.E_STRUCT_WRONG and align_method == 0):
                    event_status = au.E_BCID_INC_ERROR
                else:
                    event_status = 0

                # Set after reset of event variables the TDC event status and write tdc value and timestamp.
                # Needed since TDC words come before actual event.
                event_status |= event_status_tdc
                for index in range(4):
                    tdc_value[index] = tdc_value_buffer[index]
                    tdc_timestamp[index] = tdc_timestamp_buffer[index]
                    tdc_status[index] = tdc_status_buffer[index]
                # Reset TDC variables
                event_status_tdc = 0
                tdc_value_buffer[:] = 2 ** 16 - 1
                tdc_timestamp_buffer[:] = 0
                tdc_status_buffer[:] = 0
                tdc_word[:] = False
                tdc_word_count = 0

            next_word_is_tag = False
            next_word_is_ccol = True  # Next word is ccol
            next_word_is_last_neighbor = False
            next_word_is_qrow = False
            next_word_is_hmap = False
            next_word_is_tot = False

            # Set tag counter variables
            n_tags += 1
            prev_tag = tag
            last_tag = tag

        if next_word_is_ccol and not next_word_is_tot:  # Extract core column
            if au.get_length(word) < 6:  # Check for complete 6 bit core column.
                old_word = word  # Store left over data word
                continue
            ccol_buf = word >> (au.get_length(word) - 6) & CCOL_MASK
            if ccol_buf >= 54:  # TODO: why not >= 56. According to manual valid ccol values are < 56, >=56 is new tag.
                # No valid ccol, no hit. Continue with next raw data word.
                word = au.rm_msb(word, 3)
                old_word = word
                next_word_is_tag = True  # Next word is tag
                next_word_is_ccol = False
                next_word_is_last_neighbor = False
                next_word_is_qrow = False
                next_word_is_hmap = False
                next_word_is_tot = False
                continue
            elif ccol_buf == 0:  # End of stream
                # Reset per stream variables. Continue with next raw data word
                found_start_of_stream = False
                old_word = 0
                next_word_is_tag = False
                next_word_is_ccol = False
                next_word_is_last_neighbor = False
                next_word_is_qrow = False
                next_word_is_hmap = False
                next_word_is_tot = False
                continue
            else:
                # Valid hit, store core column value.
                word = au.rm_msb(word, 6)
                ccol = ccol_buf
                old_word = word
                next_word_is_tag = False
                next_word_is_ccol = False
                next_word_is_last_neighbor = True  # Next word is islast and isneighbor
                next_word_is_qrow = False
                next_word_is_hmap = False
                next_word_is_tot = False

        if next_word_is_last_neighbor and not next_word_is_tot:  # Extract islast and isneighbor
            if au.get_length(word) < 3:  # Check for complete 2 bit islast and isneihbor
                old_word = word  # Store left over data word
                continue
            is_last = word >> (au.get_length(word) - 1) & ISLAST_MASK
            word = au.rm_msb(word, 1)
            is_neighbor = word >> (au.get_length(word) - 1) & ISNEIGHBOUR_MASK
            word = au.rm_msb(word, 1)
            if is_neighbor:
                next_word_is_hmap = True  # Next word is hmap
                next_word_is_last_neighbor = False
                old_word = word  # Store left over data word
            else:
                next_word_is_qrow = True  # Next word is qrow
                next_word_is_last_neighbor = False
                old_word = word  # Store left over data word

        if next_word_is_qrow and not next_word_is_tot:  # Extract quarter row
            if au.get_length(word) < 8:  # Check for complete 8 bit quarter row
                old_word = word  # Store left over data word
                continue
            q_row = word >> (au.get_length(word) - 8) & QROW_MASK
            word = au.rm_msb(word, 8)
            if q_row > 200:
                next_word_is_qrow = False
                found_start_of_stream = False
                continue
            q_row_array[ccol] = q_row
            next_word_is_qrow = False
            next_word_is_hmap = True  # Next word is hmap

        if next_word_is_hmap and not next_word_is_tot:  # Extract hmap, decodes a quarter core (= 8 columns x 2 rows).
            tot[:] = -1
            tot_cnt = 0
            if au.get_length(word) < 30:  # Check for complete 30 bit hmap
                old_word = word  # Store left over data word
                continue
            word, hmap_word, hits_hmap = _hmap_raw(word)
            next_word_is_hmap = False
            next_word_is_tot = True  # Next word is tot

        if next_word_is_tot:  # Extract tot
            for _ in range(hits_hmap):
                if au.get_length(word) < 4:  # Check for complete 4 bit tot
                    continue
                tot_val = word >> (au.get_length(word) - 4) & TOT_MASK
                word = au.rm_msb(word, 4)
                hits_hmap -= 1
                tot[tot_cnt] = tot_val  # Store tot value for each hit
                tot_cnt += 1
            if hits_hmap == 0:
                next_word_is_tot = False
                old_word = word
                if len(ptot_table_stretched) > 1:
                    ptot_table_piece = ptot_table_stretched[trg_number]
                else:
                    ptot_table_piece = ptot_table_stretched[0]
                hit_buffer, q_row, hit_buffer_i = add_hits(hit_buffer, tag, ccol, is_neighbor, q_row_array[ccol],
                                                           hmap_word, tot, event_number, trg_number, hit_buffer_i, ptot_table_piece, tdc_value, tdc_timestamp)

        if is_last:
            next_word_is_ccol = True  # Next word is ccol
            is_last = 0
            old_word = word
            continue
        else:
            next_word_is_last_neighbor = True  # Next word is lastneighbor flag
            old_word = word
            continue

    if last_chunk:
        if len(trigger_table_stretched) > 1:
            scan_param_id = trigger_table_stretched[trg_number]
        data_out_i = au.build_event(hits, data_out_i,
                                    hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                    hist_event_status, hist_tdc_status, hist_tdc_value,
                                    hist_ptot, hist_ptoa,
                                    hit_buffer, hit_buffer_i, 0,
                                    scan_param_id, event_status, tdc_status)
        event_number += 1
        last_word_index = i + 1

    return data_out_i, event_number, last_word_index - rawdata.shape[0], start_tag - 1, prev_trg_number - 1


@numba.njit()
def _hmap_raw(word):
    hits_hmap = 0
    vertical_indicator = word >> (au.get_length(word) - 2) & 0x3
    if vertical_indicator in [0, 1]:
        vertical_indicator_start = 1
        vertical_indicator_stop = 2
        word = au.rm_msb(word, 1)
    elif vertical_indicator in [2]:
        vertical_indicator_start = 0
        vertical_indicator_stop = 1
        word = au.rm_msb(word, 2)
    elif vertical_indicator in [3]:
        vertical_indicator_start = 0
        vertical_indicator_stop = 2
        word = au.rm_msb(word, 2)
    store_array = [1, 1, 1, 1, 1, 1]
    for vertical_hits in range(vertical_indicator_start, vertical_indicator_stop):
        start_val = word >> (au.get_length(word) - 2) & 0x3
        if start_val in [0, 1]:
            start_val = 1
            word = au.rm_msb(word, 1)
        else:
            word = au.rm_msb(word, 2)
        start_val += 4
        store_array[vertical_hits * 3] = start_val
        for i in range(1, 4):
            buf = store_array[vertical_hits * 3 + i - 1]
            for _ in range(au.get_length(store_array[vertical_hits * 3 + i - 1])):
                j = buf >> (au.get_length(buf) - 1) & 0x1
                buf = au.rm_msb(buf, 1)
                if i < 3:
                    if j == 0:
                        store_array[vertical_hits * 3 + i] = store_array[vertical_hits * 3 + i] << 2
                    if j == 1:
                        word_val = word >> (au.get_length(word) - 2) & 0x3
                        if word_val in [0, 1]:
                            store_array[vertical_hits * 3 + i] = (store_array[vertical_hits * 3 + i] << 2) + 1
                            word = au.rm_msb(word, 1)
                        else:
                            store_array[vertical_hits * 3 + i] = (store_array[vertical_hits * 3 + i] << 2) + (word >> (au.get_length(word) - 2) & 0x3)
                            word = au.rm_msb(word, 2)
                else:
                    if j == 1:
                        hits_hmap += 1
    if vertical_indicator_start == 1:
        hword = ((store_array[2]) << 16) + (store_array[5] & 0xff)
    elif vertical_indicator_stop == 1:
        hword = ((store_array[2]) << 8)
    else:
        hword = ((store_array[2]) << 8) + (store_array[5] & 0xff)
    return word, hword, hits_hmap


def _open_h5_file(fname):
    import tables as tb
    with tb.open_file(fname) as in_file:
        words = np.array(in_file.root.raw_data[:], dtype=np.int64)
    return words


def _dump_print(content):
    full_str = ''
    cnt = 1
    cnt2 = 0
    for i in content:
        word = bin(i)[2:].zfill(16)
        if len(bin(i)[2:]) == 17:
            word = word[2:].zfill(16)
        full_str += word
        if cnt == 4:
            full_str += ' ' + str(len(full_str)) + ' '
            cnt = 0
        cnt += 1
        cnt2 += 1
    return full_str


@numba.njit()
def add_hits(hits, tag, ccol, is_neighbor, q_row, hmap_word, tot, event_number, trg_number, hit_buffer_i, ptot_meta_data, tdc_value, tdc_timestamp):
    hit_cnt = 0
    col = 0
    row = 0
    if is_neighbor == 1:
        q_row += 1
    if q_row == 196:  # Precision TOT word
        tot_val4 = 0  # No regular 4 bit ToT in precision ToT mode
        tot_cnt = 0
        for hitor_id in range(4):  # Loop over data words from all hitors
            tot_val16 = 0
            for _ in range(4):  # Loop over four the 4-bit fragments and re-construct 16 bit ptot word from the 4-bit fragments.
                hit = hmap_word >> (au.get_length(hmap_word) - 1) & 0x1
                hmap_word = au.rm_msb(hmap_word, 1)
                if hit:  # Hit
                    tot_val16 += tot[tot_cnt]
                    tot_cnt += 1
                else:
                    tot_val16 += 15  # No hit
                tot_val16 = tot_val16 << 4

            # Calculate PTOT and PTOA. TODO: Needs to be confirmed
            ptoa = (tot_val16 >> 4) & 0x001f
            tot_val16 = tot_val16 >> 8
            tot_val16 = ((tot_val16 & 0xf00) >> 8) + (tot_val16 & 0x0f0) + ((tot_val16 & 0x00f) << 8)
            tot_val16 = tot_val16 & 0x7ff

            if hitor_id == 0:
                col = ptot_meta_data['hit_or_1_col']
                row = ptot_meta_data['hit_or_1_row']
                if ptot_meta_data['hit_or_1_col'] == 65535 and ptot_meta_data['hit_or_2_col'] == 65535 and ptot_meta_data['hit_or_3_col'] == 65535 and ptot_meta_data['hit_or_4_col'] == 65535:
                    col = 0
                    row = 0
            if hitor_id == 1:
                col = ptot_meta_data['hit_or_2_col']
                row = ptot_meta_data['hit_or_2_row']
                if ptot_meta_data['hit_or_1_col'] == 65535 and ptot_meta_data['hit_or_2_col'] == 65535 and ptot_meta_data['hit_or_3_col'] == 65535 and ptot_meta_data['hit_or_4_col'] == 65535:
                    col = 1
                    row = 0
            if hitor_id == 2:
                col = ptot_meta_data['hit_or_3_col']
                row = ptot_meta_data['hit_or_3_row']
                if ptot_meta_data['hit_or_1_col'] == 65535 and ptot_meta_data['hit_or_2_col'] == 65535 and ptot_meta_data['hit_or_3_col'] == 65535 and ptot_meta_data['hit_or_4_col'] == 65535:
                    col = 2
                    row = 0
            if hitor_id == 3:
                col = ptot_meta_data['hit_or_4_col']
                row = ptot_meta_data['hit_or_4_row']
                if ptot_meta_data['hit_or_1_col'] == 65535 and ptot_meta_data['hit_or_2_col'] == 65535 and ptot_meta_data['hit_or_3_col'] == 65535 and ptot_meta_data['hit_or_4_col'] == 65535:
                    col = 3
                    row = 0
            if col < 65534 & tot_val16 < 2000:
                au.fill_buffer(hits, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, tag, tag, tag, (ccol - 1) * 8 + col, row, tot_val4, tot_val16)
                hit_buffer_i += 1
    else:  # Normal TOT word
        ptot = 0  # No PToT available in case of normal TOT mode
        ptoa = 0  # No PtoA available in case of normal TOT mode
        for cnt in range(au.get_length(hmap_word)):  # Loop over all hits
            hit = hmap_word >> (au.get_length(hmap_word) - 1) & 0x1
            hmap_word = au.rm_msb(hmap_word, 1)
            # Calculate column and row using core column and position inside quarter core. Hmap words decodes hits in quater core (= 8 columns x 2 rows).
            col = (ccol - 1) * 8 + cnt
            row = q_row * 2
            if cnt > 7:
                col -= 8
                row += 1
            if row > 383:
                break
            if hit == 1:  # High bit means hit
                au.fill_buffer(hits, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, tag, tag, tag, col, row, tot[hit_cnt], ptot)
                hit_buffer_i += 1
                hit_cnt += 1
    return hits, q_row, hit_buffer_i


def analyze_chunk(rawdata, return_hists=('HistOcc',), return_hits=False,
                  scan_param_id=0,
                  trig_pattern=0b11111111111111111111111111111111,
                  align_method=0,
                  analyze_tdc=False,
                  use_tdc_trigger_dist=False,
                  analyze_ptot=False,
                  rx_id=0,
                  ext_trig_id_map=np.array([-1]),
                  ptot_table_stretched=np.array([-1])):
    ''' Helper function to quickly analyze a data chunk.

        Warning
        -------
            If the rawdata contains incomplete event data only data that do
            not need event building are correct (occupancy + tot histograms)

        Parameters
        ----------
        rawdata : np.array, 32-bit dtype
            The raw data containing FE, trigger and TDC words
        return_hists : iterable of strings
            Names to select the histograms to return. Is not case sensitive
            and string must contain only e.g.: occ, tot, bcid, event
        return_hits : boolean
            Return the hit array
        scan_param_id : integer
            Set scan par id in hit info table
        trig_pattern : integer (32-bits)
            Indicate the position of the sub-triggers. Needed to check
            the BCIDs.
        align_method : integer
            Methods to do event alignment
            0: New event when number if event headers exceeds number of
               sub-triggers. Many fallbacks for corrupt data implemented.
            1: New event when data word is TLU trigger word, with error checks
            2: Force new event always at TLU trigger word, no error checks
        analyze_tdc : boolean
            If analyze_tdc is True, interpret and analyze also TDC words. Default is False,
            meaning that TDC analysis is skipped. This is useful for scans which do no
            require an TDC word interpretation (e.g. threshold scan) in order to save time.
        analyze_ptot : boolean
            If analyze_ptot is True, RD53B ToT words will be interpreted as PTOT words (contain precision ToT and ToA).
            Only possible for RD53B.
        use_tdc_trigger_dist : boolean
            If True use trigger distance (delay between Hitor and Trigger) in TDC word
            interpretation. If False use instead TDC timestamp from TDC word. Default
            is False.

        Usefull for tuning. Analysis per scan parameter not possible.
        Chunks should not be too large.

        Returns
        -------
            ordered dict: With key = return_hists string, value = data
            and first entry are hits when selected
    '''

    n_hits = rawdata.shape[0] * 4
    (hits, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id, hist_event_status, hist_tdc_status, hist_tdc_value,
     hist_bcid_error, hist_ptot, hist_ptoa) = au.init_outs(n_hits, n_scan_params=1, rows=384, analyze_ptot=analyze_ptot)

    interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_trigger_id, hist_event_status,
                   hist_tdc_status, hist_tdc_value, hist_bcid_error,
                   hist_ptot, hist_ptoa,
                   scan_param_id=scan_param_id, event_number=0,
                   trig_pattern=trig_pattern,
                   align_method=align_method,
                   prev_tag=-1,
                   analyze_tdc=analyze_tdc,
                   use_tdc_trigger_dist=use_tdc_trigger_dist,
                   last_chunk=True,
                   rx_id=rx_id,
                   trigger_table_stretched=np.array([-1]),
                   ptot_table_stretched=ptot_table_stretched)

    hists = {'occ': hist_occ,
             'tot': hist_tot,
             'rel': hist_rel_bcid,
             'event': hist_event_status,
             'error': hist_bcid_error
             }

    ret = OrderedDict()

    if return_hits:
        ret['hits'] = hits

    for key, value in hists.items():
        for word in return_hists:
            if key in word.lower():
                ret[word] = value
                break

    return ret


def interpret_userk_data(rawdata):
    userk_data = np.zeros(shape=rawdata.shape[0],
                          dtype={
                              'names': ['ChipID', 'AuroraKWord', 'Status', 'Data1', 'Data1_AddrFlag', 'Data1_Addr',
                                        'Data1_Data',
                                        'Data0', 'Data0_AddrFlag', 'Data0_Addr', 'Data0_Data'],
                              'formats': ['uint8', 'uint8', 'uint8', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16',
                                          'uint16',
                                          'uint16', 'uint16']})
    userk_data_i = _jit_userk_data(rawdata, userk_data)
    return userk_data[:userk_data_i]


@numba.njit(cache=True)
def _jit_userk_data(rawdata, userk_data):
    userk_word_cnt = 0
    userk_data_i = 0

    for word in rawdata:
        if (word & au.USERK_FRAME_ID):
            if userk_word_cnt == 0:
                userk_word = word & 0x0fff
                userk_data[userk_data_i]['Status'] = (word >> 12) & 0x3
                userk_data[userk_data_i]['ChipID'] = (word >> 14) & 0x3
            else:
                userk_word = userk_word << 16 | word & 0xffff
            userk_word_cnt += 1
            if userk_word_cnt == 4:
                userk_data[userk_data_i]['AuroraKWord'] = userk_word & 0xff
                Data1 = (userk_word >> 34) & 0x7ffffff
                Data0 = (userk_word >> 8) & 0x7ffffff
                userk_data[userk_data_i]['Data1'] = Data1
                userk_data[userk_data_i]['Data1_AddrFlag'] = (Data1 >> 25) & 0x1
                userk_data[userk_data_i]['Data1_Addr'] = (Data1 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data1_Data'] = (Data1 >> 0) & 0xffff
                userk_data[userk_data_i]['Data0'] = Data0
                userk_data[userk_data_i]['Data0_AddrFlag'] = (Data0 >> 25) & 0x1
                userk_data[userk_data_i]['Data0_Addr'] = (Data0 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data0_Data'] = (Data0 >> 0) & 0xffff
                userk_data_i += 1
                userk_word_cnt = 0
    return userk_data_i


def print_raw_data(raw_data):
    ''' Print raw data with interpretation for debugging '''
    for i, word in enumerate(raw_data):
        if word & au.TRIGGER_HEADER:
            print('TRG {:<26} {:032b}'.format(*[word & au.TRG_MASK, word]))
            continue

        if word & au.TDC_HEADER == au.TDC_ID_0:
            print('TDC VAL0 {:<26} TDC TS0 {:<26} TDC DIST0 {:<26} {:032b}'.format(*[word & au.TDC_VALUE_MASK, (au.TDC_TIMESTAMP_MASK & word) >> 12, (au.TDC_TRIG_DIST_MASK & word) >> 20, word]))
            continue
        if word & au.TDC_HEADER == au.TDC_ID_1:
            print('TDC VAL1 {:<26} TDC TS1 {:<26} TDC DIST0 {:<26} {:032b}'.format(*[word & au.TDC_VALUE_MASK, (au.TDC_TIMESTAMP_MASK & word) >> 12, (au.TDC_TRIG_DIST_MASK & word) >> 20, word]))
            continue
        if word & au.TDC_HEADER == au.TDC_ID_2:
            print('TDC VAL2 {:<26} TDC TS2 {:<26} TDC DIST0 {:<26} {:032b}'.format(*[word & au.TDC_VALUE_MASK, (au.TDC_TIMESTAMP_MASK & word) >> 12, (au.TDC_TRIG_DIST_MASK & word) >> 20, word]))
            continue
        if word & au.TDC_HEADER == au.TDC_ID_3:
            print('TDC VAL3 {:<26} TDC TS3 {:<26} TDC DIST0 {:<26} {:032b}'.format(*[word & au.TDC_VALUE_MASK, (au.TDC_TIMESTAMP_MASK & word) >> 12, (au.TDC_TRIG_DIST_MASK & word) >> 20, word]))
            continue

        if word & au.USERK_FRAME_ID:
            print('U-K {:032b}'.format(*[word]) + '\t{:08x}'.format(*[word]))
            continue

if __name__ == '__main__':
    words = _open_h5_file("Test h5 file")
    _dump_print(words[:32])
    analyze_chunk(words, rx_id=3)
