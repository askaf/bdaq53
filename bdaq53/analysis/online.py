#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Online data analysis functions
'''

import ctypes
import logging
import multiprocessing
import time
import queue
from functools import reduce

import numpy as np
import numba

from bdaq53.analysis import analysis_utils as au

logger = logging.getLogger('OnlineAnalysis')

numba_logger = logging.getLogger('numba')
numba_logger.setLevel(logging.WARNING)


@numba.njit(cache=True, fastmath=True)
def histogram(raw_data, occ_hist, data_word, is_fe_high_word, is_data_header):
    ''' Raw data to 2D occupancy histogram '''

    for word in raw_data:
        if word & au.TRIGGER_HEADER:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_0:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_1:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_2:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_3:
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.USERK_FRAME_ID:     # skip USER_K frame
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.HEADER_ID:          # data header
            is_data_header = 1
            is_fe_high_word = 1
        # Can only interpret FE words if high word is found, high word can
        # only be found in event header since event hit records do not
        # have a header
        elif is_fe_high_word < 0:
            continue
        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if is_fe_high_word == 1:
            data_word = word & 0xffff
            is_fe_high_word = 0  # Next is low word
            continue  # Low word still missing
        elif is_fe_high_word == 0:
            data_word = data_word << 16 | word & 0xffff
            is_fe_high_word = 1  # Next is high word

        if is_data_header == 1:
            # After data header follows header less hit data
            is_data_header = 0
        else:  # data word is hit data
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff
            for i in range(4):
                col, row = au.translate_mapping(multicol, region, i)
                tot = (data_word >> i * 4) & 0xf

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        occ_hist[col, row] += 1
                        # print np.count_nonzero(occ_hist)
                        # print occ_hist.sum()
                else:  # unknown word
                    continue

    return data_word, is_fe_high_word, is_data_header


@numba.njit
def histogram_tot(raw_data, hist_tot, data_word, is_fe_high_word, is_data_header):
    ''' Raw data to TOT histogram for each pixel'''
    for word in raw_data:
        if word & au.TRIGGER_HEADER:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_0:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_1:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_2:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_3:
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.USERK_FRAME_ID:     # skip USER_K frame
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.HEADER_ID:          # data header
            is_data_header = 1
            is_fe_high_word = 1
        # Can only interpret FE words if high word is found, high word can
        # only be found in event header since event hit records do not
        # have a header
        elif is_fe_high_word < 0:
            continue
        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if is_fe_high_word == 1:
            data_word = word & 0xffff
            is_fe_high_word = 0  # Next is low word
            continue  # Low word still missing
        elif is_fe_high_word == 0:
            data_word = data_word << 16 | word & 0xffff
            is_fe_high_word = 1  # Next is high word

        if is_data_header == 1:
            # After data header follows header less hit data
            is_data_header = 0
        else:  # data word is hit data
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff
            for i in range(4):
                col, row = au.translate_mapping(multicol, region, i)
                tot = (data_word >> i * 4) & 0xf

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        hist_tot[col, row, tot] += 1
                        # print np.count_nonzero(occ_hist)
                        # print occ_hist.sum()
                else:  # unknown word
                    continue

    return data_word, is_fe_high_word, is_data_header


class OnlineHistogrammingBase():
    ''' Base class to do online analysis with raw data from chip.

        The output data is a histogram of a given shape.
    '''
    _queue_timeout = 0.01  # max blocking time to delete object [s]

    def __init__(self, shape):
        self._raw_data_queue = multiprocessing.Queue()
        self.stop = multiprocessing.Event()
        self.lock = multiprocessing.Lock()
        self.last_add = None  # time of last add to queue
        self.shape = shape
        self.analysis_function_kwargs = {}

    def init(self):
        # Create shared memory 32 bit unsigned int numpy array
        n_values = reduce(lambda x, y: x * y, self.shape)
        shared_array_base = multiprocessing.Array(ctypes.c_uint, n_values)
        shared_array = np.ctypeslib.as_array(shared_array_base.get_obj())
        self.hist = shared_array.reshape(*self.shape)
        self.idle_worker = multiprocessing.Event()
        self.p = multiprocessing.Process(target=self.worker,
                                         args=(self._raw_data_queue, shared_array_base,
                                               self.lock, self.stop, self.idle_worker))
        self.p.start()
        logger.info('Starting process %d', self.p.pid)

    def analysis_function(self, raw_data, hist, *args):
        raise NotImplementedError("You have to implement the analysis_funtion")

    def add(self, raw_data):
        ''' Add raw data to be histogrammed '''
        self.last_add = time.time()  # time of last add to queue
        self.idle_worker.clear()  # after addding data worker cannot be idle
        self._raw_data_queue.put(raw_data)

    def _reset_hist(self):
        with self.lock:
            self.hist = self.hist.reshape(-1)
            for i in range(self.hist.shape[0]):
                self.hist[i] = 0
            self.hist = self.hist.reshape(self.shape)

    def reset(self, wait=True, timeout=0.5):
        ''' Reset histogram '''
        if not wait:
            if not self._raw_data_queue.empty() or not self.idle_worker.is_set():
                logger.warning('Resetting histogram while filling data')
        else:
            if not self.idle_worker.wait(timeout):
                logger.warning('Resetting histogram while filling data')
        self._reset_hist()

    def get(self, wait=True, timeout=None, reset=True):
        ''' Get the result histogram '''
        if not wait:
            if not self._raw_data_queue.empty() or not self.idle_worker.is_set():
                logger.warning('Getting histogram while analyzing data')
        else:
            if not self.idle_worker.wait(timeout):
                logger.warning('Getting histogram while analyzing data. Consider increasing the timeout.')

        if reset:
            hist = self.hist.copy()
            # No overwrite with a new zero array due to shared memory
            self._reset_hist()
            return hist
        else:
            return self.hist

    def worker(self, raw_data_queue, shared_array_base, lock, stop, idle):
        ''' Histogramming in seperate process '''
        hist = np.ctypeslib.as_array(shared_array_base.get_obj()).reshape(self.shape)
        while not stop.is_set():
            try:
                raw_data = raw_data_queue.get(timeout=self._queue_timeout)
                idle.clear()
                with lock:
                    return_values = self.analysis_function(raw_data, hist, **self.analysis_function_kwargs)
                    self.analysis_function_kwargs.update(zip(self.analysis_function_kwargs, return_values))
            except queue.Empty:
                idle.set()
                continue
            except KeyboardInterrupt:   # Need to catch KeyboardInterrupt from main process
                stop.set()
        idle.set()

    def close(self):
        ''' Close process and wait till done. Likely needed to give access to pytable file handle.'''
        logger.info('Stopping process %d', self.p.pid)
        self._raw_data_queue.close()
        self._raw_data_queue.join_thread()  # Needed otherwise IOError: [Errno 232] The pipe is being closed
        self.stop.set()
        self.p.join()

    def __del__(self):
        if self.p.is_alive():
            logger.warning('Process still running. Was close() called?')
            self.close()


class OccupancyHistogramming(OnlineHistogrammingBase):
    ''' Fast histogramming of raw data to a 2D hit histogramm

        No event building.
    '''

    def __init__(self, chip_type='rd53a'):
        if 'rd53a' in chip_type.lower():
            super().__init__(shape=(400, 192))
            self.analysis_function_kwargs = {'is_fe_high_word': -1, 'is_data_header': 0, 'data_word': 0}

            def analysis_function(self, raw_data, hist, is_fe_high_word, is_data_header, data_word):
                return histogram(raw_data, hist, is_fe_high_word, is_data_header, data_word)
            setattr(OccupancyHistogramming, 'analysis_function', analysis_function)
        else:
            raise NotImplementedError('Chip type %s not supported!' % chip_type)
        self.init()


class TotHistogramming(OnlineHistogrammingBase):
    ''' Fast histogramming of raw data to a TOT histogramm for each pixel

        No event building.
    '''
    _queue_timeout = 0.01  # max blocking time to delete object [s]

    def __init__(self, chip_type='rd53a'):
        if 'rd53a' in chip_type.lower():
            super().__init__(shape=(400, 192, 16))
            self.analysis_function_kwargs = {'is_fe_high_word': -1, 'is_data_header': 0, 'data_word': 0}

            def analysis_function(self, raw_data, hist, is_fe_high_word, is_data_header, data_word):
                return histogram_tot(raw_data, hist, is_fe_high_word, is_data_header, data_word)
            setattr(TotHistogramming, 'analysis_function', analysis_function)
        else:
            raise NotImplementedError('Chip type %s not supported!' % chip_type)
        self.init()


if __name__ == "__main__":
    pass
