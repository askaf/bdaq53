#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    BDAQ53 plotting class

    This class can be used standalone to reproduce plots from interpreted data files generated with BDAQ53.
    To install prerequisutes in an (Ana- or Mini-) Conda environment, run
        conda install --yes numpy scipy pytables matplotlib tqdm blosc

    Standalone usage:
        At the bottom of the script,
        - enter the path to the interpreted data file,
        - select the appropriate flags,
        - select which plots to generate
        and run the script.
'''

import copy
import math
import shutil
import os
import random
import datetime

import iminuit
import tables as tb
import matplotlib
import numpy as np
import matplotlib.pyplot as plt

from collections import OrderedDict
from scipy.optimize import curve_fit
from scipy.stats import norm
from scipy import interpolate
from matplotlib.figure import Figure
from matplotlib.artist import setp
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import colors, cm
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec

from bdaq53.system import logger
from bdaq53.chips import rd53a
import bdaq53.analysis.analysis_utils as au

TITLE_COLOR = '#07529a'
OVERTEXT_COLOR = '#07529a'

SCURVE_CHI2_UPPER_LIMIT = 50

DACS = {'SYNC': ['IBIASP1_SYNC',
                 'IBIASP2_SYNC',
                 'IBIAS_SF_SYNC',
                 'IBIAS_KRUM_SYNC',
                 'IBIAS_DISC_SYNC',
                 'ICTRL_SYNCT_SYNC',
                 'VBL_SYNC',
                 'VTH_SYNC',
                 'VREF_KRUM_SYNC'],
        'LIN': ['PA_IN_BIAS_LIN',
                'FC_BIAS_LIN',
                'KRUM_CURR_LIN',
                'LDAC_LIN',
                'COMP_LIN',
                'REF_KRUM_LIN',
                'Vthreshold_LIN'],
        'DIFF': ['PRMP_DIFF',
                 'FOL_DIFF',
                 'PRECOMP_DIFF',
                 'COMP_DIFF',
                 'VFF_DIFF',
                 'VTH1_DIFF',
                 'VTH2_DIFF',
                 'LCC_DIFF',
                 'CONF_FE_DIFF'],
        'ITKPIXV1': ['DAC_PREAMP_L_DIFF',
                     'DAC_PREAMP_R_DIFF',
                     'DAC_PREAMP_TL_DIFF',
                     'DAC_PREAMP_TR_DIFF',
                     'DAC_PREAMP_T_DIFF',
                     'DAC_PREAMP_M_DIFF',
                     'DAC_PRECOMP_DIFF',
                     'DAC_COMP_DIFF',
                     'DAC_VFF_DIFF',
                     'DAC_TH1_L_DIFF',
                     'DAC_TH1_R_DIFF',
                     'DAC_TH1_M_DIFF',
                     'DAC_TH2_DIFF',
                     'DAC_LCC_DIFF',
                     'LEACKAGE_FEEDBACK']
        }


class Plotting(object):
    def __init__(self, analyzed_data_file, pdf_file=None, level='preliminary', qualitative=False, internal=False, save_single_pdf=False, save_png=False):

        self.log = logger.setup_derived_logger('Plotting')

        self.plot_cnt = 0
        self.save_single_pdf = save_single_pdf
        self.save_png = save_png
        self.level = level
        self.qualitative = qualitative
        self.internal = internal
        self.clustered = False
        self.skip_plotting = False

        if pdf_file is None:
            self.filename = '.'.join(
                analyzed_data_file.split('.')[:-1]) + '.pdf'
        else:
            self.filename = pdf_file
        self.out_file = PdfPages(self.filename)

        try:
            if isinstance(analyzed_data_file, str):
                in_file = tb.open_file(analyzed_data_file, 'r')
                root = in_file.root
            else:
                root = analyzed_data_file
        except IOError:
            self.log.warning('Interpreted data file does not exist!')
            self.skip_plotting = True
            return

        self.scan_config = au.ConfigDict(root.configuration_in.scan.scan_config[:])
        self.run_config = au.ConfigDict(root.configuration_in.scan.run_config[:])
        self.chip_settings = au.ConfigDict(root.configuration_in.chip.settings[:])
        self.cols = 400
        self.chip_type = 'RD53A'
        try:
            if self.run_config['chip_type'].lower() in ['itkpixv1']:
                self.rows = 384
                self.chip_type = 'ITkPixV1'
            elif self.run_config['chip_type'].lower() in ['crocv1']:
                self.rows = 336
                self.cols = 432
                self.chip_type = 'CROCV1'
            else:
                self.rows = 192
        except KeyError:
            self.log.warning('Specified chip type is not supported!')
            self.rows = 192
        self.num_pix = self.rows * self.cols
        self.plot_box_bounds = [0.5, self.cols + 0.5, self.rows + 0.5, 0.5]
        try:
            self.scan_params = root.configuration_in.scan.scan_params[:]
        except tb.NoSuchNodeError:
            self.scan_params = None

        self.registers = au.ConfigDict(root.configuration_in.chip.registers[:])
        self.use_ptot = self.chip_settings.get('use_ptot', False)

        if self.run_config['scan_id'] not in ['adc_tuning', 'dac_linearity_scan', 'seu_test', 'pixel_register_scan']:
            self.enable_mask = self._mask_disabled_pixels(root.configuration_in.chip.use_pixel[:], self.scan_config)
            self.n_enabled_pixels = len(self.enable_mask[~self.enable_mask])
            self.tdac_mask = root.configuration_in.chip.masks.tdac[:]

        self.calibration = {e[0].decode('utf-8'): float(e[1].decode('utf-8')) for e in root.configuration_in.chip.calibration[:]}

        if self.run_config['scan_id'] not in ['dac_linearity_scan', 'seu_test', 'adc_tuning', 'pixel_register_scan', 'noise_occupancy_scan_advanced', 'bump_connection_threshold_shift_scan', 'bump_connection_noise_shift_bias_scan', 'bump_connection_noise_shift_ir_scan']:
            self.HistEventStatus = root.HistEventStatus[:]
            try:
                self.HistTdcStatus = root.HistTdcStatus[:]
            except tb.NoSuchNodeError:
                self.HistTdcStatus = None
            self.HistOcc = root.HistOcc[:]
            self.HistTot = root.HistTot[:]
            self.HistRelBCID = root.HistRelBCID[:]
            self.HistBCIDError = root.HistBCIDError[:]
            self.HistTrigID = root.HistTrigID[:]
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'global_threshold_tuning', 'in_time_threshold_scan', 'autorange_threshold_scan', 'crosstalk_scan']:
                self.ThresholdMap = root.ThresholdMap[:, :]
                self.Chi2Map = root.Chi2Map[:, :]
                self.Chi2Sel = (self.Chi2Map > 0) & (self.Chi2Map < SCURVE_CHI2_UPPER_LIMIT) & (~self.enable_mask)
                self.n_failed_scurves = self.n_enabled_pixels - len(self.Chi2Map[self.Chi2Sel])
                self.NoiseMap = root.NoiseMap[:]
            elif self.run_config['scan_id'] == 'bump_connection_bias_thr_shift_scan':
                thresholdMap1 = root.ThresholdMapReverse[:, :]
                thresholdMap2 = root.ThresholdMapForward[:, :]
                self.n_failed_scurves_1 = self.n_enabled_pixels - len(thresholdMap1[thresholdMap1 != 0])
                self.n_failed_scurves_2 = self.n_enabled_pixels - len(thresholdMap2[thresholdMap2 != 0])

        if self.run_config['scan_id'] in ['noise_occupancy_scan_advanced']:
            self.HistOcc = root.HistOcc[:]

        if self.run_config['scan_id'] in ['tot_calibration']:
            self.HistTotCal = root.HistTotCal[:]
            self.HistTotCalMean = root.HistTotCalMean[:]
            self.HistTotCalStd = root.HistTotCalStd[:]
            self.TOThist = root.TOThist[:]

        if self.run_config['scan_id'] in ['hitor_calibration', 'hitor_calibration_ptot']:
            self.HistTdc = root.hist_2d_tdc_vcal[:]
            self.HistTotCalMean = root.hist_tot_mean[:]
            self.HistTotCalStd = root.hist_tot_std[:]
            self.HistTdcCalMean = root.hist_tdc_mean[:]
            self.HistTdcCalStd = root.hist_tdc_std[:]
            self.lookup_table = root.lookup_table[:]

        if self.run_config['scan_id'] in ['dac_linearity_scan', 'adc_tuning']:
            self.DAC_data = root.dac_data[:]

        if 'ptot' in self.run_config['scan_id']:
            self.HistPToT = root.HistPToT[:]
            self.HistPToA = root.HistPToA[:]

        try:
            self.Cluster = root.Cluster[:]  # FIXME: This line of code does not take chunking into account
            self.HistClusterSize = root.HistClusterSize[:]
            self.HistClusterShape = root.HistClusterShape[:]
            self.HistClusterTot = root.HistClusterTot[:]
            self.clustered = True
        except tb.NoSuchNodeError:
            pass

        if 'trigger_pattern' not in self.scan_config.keys():
            self.scan_config['trigger_pattern'] = '0b11111111111111111111111111111111'

        try:
            in_file.close()
        except Exception:
            pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.out_file is not None and isinstance(self.out_file, PdfPages):
            self.log.info('Closing output PDF file: {0}'.format(self.out_file._file.fh.name))
            self.out_file.close()
            shutil.copyfile(self.filename, os.path.join(os.path.split(self.filename)[0], 'last_scan.pdf'))

    ''' User callable plotting functions '''

    def create_standard_plots(self):
        if self.skip_plotting:
            return
        self.log.info('Creating selected plots...')
        if self.run_config['scan_id'] in ['dac_linearity_scan', 'adc_tuning']:
            self.create_parameter_page()
            self.create_dac_linearity_plot()
        else:
            self.create_parameter_page()
            self.create_event_status_plot()
            self.create_occupancy_map()
            if self.run_config['scan_id'] in ['source_scan', 'ext_trigger_scan', 'source_scan_random_trigger', 'source_scan_injection']:
                self.create_fancy_occupancy()
            if 'ptot' in self.run_config['scan_id']:
                self.create_ptot_plot()
            else:
                self.create_tot_plot()
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'in_time_threshold_scan', 'hitor_calibration', 'hitor_calibration_ptot', 'tot_calibration', 'autorange_threshold_scan']:
                if 'ptot' in self.run_config['scan_id']:
                    self.create_ptot_hist()
                else:
                    self.create_tot_hist()
                self.create_rel_bcid_hist()
            if 'ptot' in self.run_config['scan_id']:
                self.create_ptot_map()
            else:
                self.create_tot_map()
            self.create_rel_bcid_plot()
            self.create_rel_bcid_map()
            self.create_bcid_error_plot()
            self.create_trigger_id_map()
            if 'ptot' in self.run_config['scan_id']:
                self.create_ptoa_map()
            if self.run_config['scan_id'] in ['analog_scan', 'threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'autorange_threshold_scan', 'in_time_threshold_scan', 'noise_occupancy_scan', 'global_threshold_tuning', 'ext_trigger_scan', 'source_scan', 'source_scan_random_trigger', 'source_scan_injection', 'crosstalk_scan', 'bump_connection_bias_thr_shift_scan']:
                self.create_tdac_plot()
                self.create_tdac_map()
                self.create_hit_pix_plot()
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'in_time_threshold_scan', 'autorange_threshold_scan']:
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_stacked_threshold_plot()
                self.create_threshold_map()
                self.create_noise_plot()
                self.create_noise_map()
            if self.run_config['scan_id'] == 'global_threshold_tuning':
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_threshold_map()
                self.create_noise_plot()
                self.create_noise_map()
            if self.run_config['scan_id'] == 'tot_calibration':
                self.create_tot_perpixel_plot()
            if self.run_config['scan_id'] in ['hitor_calibration', 'hitor_calibration_ptot']:
                self.create_tdc_hist()
                self.create_tdc_tot_perpixel_plot()
            if self.run_config['scan_id'] in ['crosstalk_scan']:
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_stacked_threshold_plot()
                self.create_threshold_map()

            if self.clustered:
                self.create_cluster_tot_plot()
                self.create_cluster_shape_plot()
                self.create_cluster_size_plot()
            if self.HistTdcStatus is not None:  # Check if TDC analysis is activated.
                self.create_tdc_status_plot()

    def create_parameter_page(self):
        try:
            self._plot_parameter_page()
        except Exception:
            self.log.error('Could not create parameter page!')

    def create_event_status_plot(self):
        try:
            self._plot_event_status(hist=self.HistEventStatus.T)
        except Exception:
            self.log.error('Could not create event status plot!')

    def create_tdc_status_plot(self):
        try:
            self._plot_tdc_status(hist=self.HistTdcStatus)
        except Exception:
            self.log.error('Could not create tdc status plot!')

    def create_bcid_error_plot(self):
        try:
            self._plot_bcid_error(hist=self.HistBCIDError.T)
        except Exception:
            self.log.error('Could not create BCID error plot!')

    def create_occupancy_map(self):
        try:
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'autorange_threshold_scan', 'global_threshold_tuning', 'injection_delay_scan', 'in_time_threshold_scan', 'injection_delay_scan', 'crosstalk_scan']:
                title = 'Integrated occupancy'
                z_max = 'maximum'
            else:
                title = 'Occupancy'
                z_max = None

            self._plot_occupancy(hist=np.ma.masked_array(self.HistOcc[:].sum(axis=2), self.enable_mask).T, z_max=z_max, suffix='occupancy', title=title)
        except Exception:
            self.log.error('Could not create occupancy map!')

    def create_fancy_occupancy(self):
        try:
            self._plot_fancy_occupancy(hist=np.ma.masked_array(self.HistOcc[:].sum(axis=2), self.enable_mask).T)
        except Exception:
            self.log.error('Could not create fancy occupancy plot!')

    def create_three_way(self):
        try:
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'autorange_threshold_scan', 'global_threshold_tuning', 'in_time_threshold_scan', 'crosstalk_scan']:
                title = 'Integrated occupancy'
            else:
                title = 'Occupancy'

            self._plot_three_way(hist=np.ma.masked_array(self.HistOcc[:].sum(axis=2), self.enable_mask).T, title=title, x_axis_title=title)
        except Exception:
            self.log.error('Could not create three way plot!')

    def create_tot_plot(self):
        ''' Create 1D tot plot '''
        try:
            self._plot_tot(hist=self.HistTot.sum(axis=(0, 1, 2)).T, plot_range=range(0, self.HistTot.shape[3]))
        except Exception:
            self.log.error('Could not create tot plot!')

    def create_ptot_plot(self):
        ''' Create 1D tot plot '''
        try:
            self._plot_ptot(hist=self.HistPToT.sum(axis=(0, 1, 2)).T, plot_range=range(0, self.HistPToT.shape[3]))
        except Exception:
            self.log.error('Could not create ptot plot!')

    def create_tot_map(self):
        try:
            self._plot_occupancy(hist=np.ma.masked_array(au.get_mean_from_histogram(self.HistTot.sum(axis=(2)), np.arange(self.HistTot.shape[3]), axis=2), self.enable_mask).T, z_label='ToT code', suffix='tot_map', z_min=0, z_max=self.HistTot.shape[3] - 2, show_sum=False, title='Mean ToT')
        except Exception:
            self.log.error('Could not create tot map!')

    def create_ptot_map(self):
        try:
            zdim = np.where(self.HistPToT > 0)[3]
            offset = np.min(zdim)
            _, counts = np.unique(zdim, return_counts=True)
            all_hits = np.sum(counts)
            zdim = np.where(counts > (all_hits * 0.005))[0] + offset
            self._plot_occupancy(hist=np.ma.masked_array(au.get_mean_from_histogram(self.HistPToT.sum(axis=(2)), np.arange(self.HistPToT.shape[3]), axis=2), self.enable_mask).T, suffix='tot_map', z_min=zdim[0], z_max=zdim[-1], show_sum=False, title='Mean PToT')
        except Exception:
            self.log.error('Could not create ptot map!')

    def create_ptoa_map(self):
        try:
            zdim = np.where(self.HistPToA > 0)[3]
            _, counts = np.unique(zdim, return_counts=True)
            all_hits = np.sum(counts)
            zdim = np.where(counts > (all_hits * 0.005))[0]
            self._plot_occupancy(hist=np.ma.masked_array(au.get_mean_from_histogram(self.HistPToA.sum(axis=(2)), np.arange(self.HistPToA.shape[3]), axis=2), self.enable_mask).T, suffix='toa_map', z_min=zdim[0], z_max=zdim[-1], show_sum=False, title='Mean Toa')
        except Exception:
            self.log.error('Could not create ptoa map!')

    def create_tot_perpixel_plot(self, n_random_pixels=3):
        try:
            if 'VCAL_HIGH_start' in self.scan_config:
                scan_parameter_range = [v - self.scan_config['VCAL_MED'] for v in
                                        range(self.scan_config['VCAL_HIGH_start'],
                                              self.scan_config['VCAL_HIGH_stop'] + 1,
                                              self.scan_config['VCAL_HIGH_step'])]
            else:  # Handle case if VCAL_HIGH_values key is given, instead of start and stop
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
            for i in range(n_random_pixels):
                # Select randomly one pixel
                row, col = random.randint(self.scan_config['start_row'], self.scan_config['stop_row'] - 1), random.randint(self.scan_config['start_column'], self.scan_config['stop_column'] - 1)
                # Reshape and column, row swap is needed since Histograms are saved in channel shape instead of column, row index.
                self._plot_tot_perpixel(self.HistTotCalMean.reshape((self.rows, self.cols, self.HistTotCalMean.shape[1]))[row, col],
                                        self.HistTotCalStd.reshape((self.rows, self.cols, self.HistTotCalStd.shape[1]))[row, col], scan_parameter_range,
                                        ylabel='ToT Code',
                                        title='Mean TOT for pixel ({0}, {1})'.format(col, row),
                                        suffix='tot_perpixel_{0}'.format(i))
        except Exception:
            self.log.error('Could not create tot plots per pixel!')

    def create_tdc_tot_perpixel_plot(self, n_random_pixels=3):
        if 'ptot' in self.run_config['scan_id']:
            use_ptot = True
        else:
            use_ptot = False
        try:
            if 'VCAL_HIGH_start' in self.scan_config:
                scan_parameter_range = [v - self.scan_config['VCAL_MED'] for v in
                                        range(self.scan_config['VCAL_HIGH_start'],
                                              self.scan_config['VCAL_HIGH_stop'] + 1,
                                              self.scan_config['VCAL_HIGH_step'])]
            else:  # Handle case if VCAL_HIGH_values key is given, instead of start and stop
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)

            for i in range(n_random_pixels):
                # Select randomly one pixel
                row, col = random.randint(self.scan_config['start_row'], self.scan_config['stop_row'] - 1), random.randint(self.scan_config['start_column'], self.scan_config['stop_column'] - 1)
                self._plot_tdc_tot_perpixel(self.HistTotCalMean[col, row],
                                            self.HistTdcCalMean[col, row],
                                            self.HistTotCalStd[col, row],
                                            self.HistTdcCalStd[col, row],
                                            self.lookup_table[col, row],
                                            scan_parameter_range,
                                            use_ptot=use_ptot,
                                            title='Mean TOT/TDC for pixel ({0}, {1})'.format(col, row),
                                            suffix='perpixel_{0}'.format(i))
        except Exception:
            self.log.error('Could not create tot/tdc plots per pixel!')

    def create_rel_bcid_plot(self):
        try:
            self._plot_relative_bcid(hist=self.HistRelBCID.sum(axis=(0, 1, 2)).T)
        except Exception:
            self.log.error('Could not create relative BCID plot!')

    def create_rel_bcid_map(self):
        try:
            self._plot_occupancy(hist=np.ma.masked_array(au.get_mean_from_histogram(self.HistRelBCID.sum(axis=(2)), np.arange(32), axis=2), self.enable_mask).T, z_label='Mean relative BCID', suffix='rel_bcid_map', z_min=0, z_max=32, show_sum=False, title='Mean relative BCID')
        except Exception:
            self.log.error('Could not create relative BCID map!')

    def create_trigger_id_map(self):
        try:
            self._plot_occupancy(hist=np.ma.masked_array(au.get_mean_from_histogram(self.HistTrigID.sum(axis=(2)), np.arange(32), axis=2), self.enable_mask).T, z_label='Mean Trigger ID', suffix='trig_id_map', z_min=0, z_max=32, show_sum=False, title='Trigger ID Histogram without Error correction')
        except Exception:
            self.log.error('Could not create trigger ID map!')

    def create_scurves_plot(self, scan_parameter_name='Scan parameter'):
        try:
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'in_time_threshold_scan', 'crosstalk_scan']:
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
                scan_parameter_range = [v - self.scan_config['VCAL_MED'] for v in
                                        range(self.scan_config['VCAL_HIGH_start'],
                                              self.scan_config['VCAL_HIGH_stop'] + 1,
                                              self.scan_config['VCAL_HIGH_step'])]
            elif self.run_config['scan_id'] in ['autorange_threshold_scan']:
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] == 'global_threshold_tuning':
                scan_parameter_name = self.scan_config['VTH_name']
                electron_axis = False
                scan_parameter_range = range(self.scan_config['VTH_start'],
                                             self.scan_config['VTH_stop'],
                                             -1 * self.scan_config['VTH_step'])
            elif self.run_config['scan_id'] == 'injection_delay_scan':
                scan_parameter_name = 'Finedelay [LSB]'
                electron_axis = False
                scan_parameter_range = range(0, 16)
            self._plot_scurves(scurves=self.HistOcc.reshape((self.rows * self.cols, -1)).T,
                               scan_parameters=scan_parameter_range,
                               electron_axis=electron_axis,
                               scan_parameter_name=scan_parameter_name)
        except Exception as e:
            self.log.error('Could not create scurve plot! ({0})'.format(e))

    def create_rel_bcid_hist(self):
        try:
            if self.run_config['scan_id'] == 'injection_delay_scan':
                scan_parameter_name = 'Finedelay [LSB]'
                electron_axis = False
                scan_parameter_range = range(0, 17)  # This numbers are bounds, thus max fine delay + 1

            elif self.run_config['scan_id'] == 'autorange_threshold_scan':
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
            else:
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
                if 'VCAL_HIGH_start' in self.scan_config:
                    scan_parameter_range = [v - self.scan_config['VCAL_MED'] for v in
                                            range(self.scan_config['VCAL_HIGH_start'],
                                                  self.scan_config['VCAL_HIGH_stop'] + 1,
                                                  self.scan_config['VCAL_HIGH_step'])]
                else:  # Handle case if VCAL_HIGH_values key is given, instead of start and stop
                    scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
            self._plot_2d_param_hist(hist=self.HistRelBCID.sum(axis=(0, 1)).T,
                                     y_max=31,
                                     scan_parameters=scan_parameter_range,
                                     electron_axis=electron_axis,
                                     scan_parameter_name=scan_parameter_name,
                                     title='RelBCID Scan Parameter Histogram',
                                     ylabel='RelBCID',
                                     suffix='bcid_param_hist')
        except Exception as e:
            self.log.error('Could not create relBCID param histogram plot! ({0})'.format(e))

    def create_tot_hist(self):
        try:
            scan_parameter_name = '$\\Delta$ VCAL'
            if self.run_config['scan_id'] == 'autorange_threshold_scan':
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
            elif 'VCAL_HIGH_start' in self.scan_config:
                scan_parameter_range = [v - self.scan_config['VCAL_MED'] for v in
                                        range(self.scan_config['VCAL_HIGH_start'],
                                              self.scan_config['VCAL_HIGH_stop'] + 1,
                                              self.scan_config['VCAL_HIGH_step'])]
            else:  # Handle case if VCAL_HIGH_values key is given, instead of start and stop
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
            self._plot_2d_param_hist(hist=self.HistTot.sum(axis=(0, 1)).T,
                                     y_max=self.HistTot.shape[3],
                                     scan_parameters=scan_parameter_range,
                                     electron_axis=True,
                                     scan_parameter_name=scan_parameter_name,
                                     title='ToT Scan Parameter Histogram',
                                     ylabel='ToT code',
                                     suffix='tot_param_hist')
        except Exception as e:
            self.log.error('Could not create tot param histogram plot! ({0})'.format(e))

    def create_ptot_hist(self):
        try:
            scan_parameter_name = '$\\Delta$ VCAL'
            if self.run_config['scan_id'] == 'autorange_threshold_scan':
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
            elif 'VCAL_HIGH_start' in self.scan_config:
                scan_parameter_range = [v - self.scan_config['VCAL_MED'] for v in
                                        range(self.scan_config['VCAL_HIGH_start'],
                                              self.scan_config['VCAL_HIGH_stop'] + 1,
                                              self.scan_config['VCAL_HIGH_step'])]
            else:  # Handle case if VCAL_HIGH_values key is given, instead of start and stop
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
            self._plot_2d_param_hist(hist=self.HistPToT.sum(axis=(0, 1)).T,
                                     y_max=self.HistPToT.shape[3],
                                     scan_parameters=scan_parameter_range,
                                     electron_axis=True,
                                     scan_parameter_name=scan_parameter_name,
                                     title='PToT Scan Parameter Histogram',
                                     ylabel='PToT code',
                                     suffix='ptot_param_hist')
        except Exception as e:
            self.log.error('Could not create ptot param histogram plot! ({0})'.format(e))

    def create_tdc_hist(self):
        try:
            scan_parameter_name = '$\\Delta$ VCAL'
            if 'VCAL_HIGH_start' in self.scan_config:
                scan_parameter_range = [v - self.scan_config['VCAL_MED'] for v in
                                        range(self.scan_config['VCAL_HIGH_start'],
                                              self.scan_config['VCAL_HIGH_stop'] + 1,
                                              self.scan_config['VCAL_HIGH_step'])]
            else:  # Handle case if VCAL_HIGH_values key is given, instead of start and stop
                scan_parameter_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
            self._plot_2d_param_hist(hist=self.HistTdc.T,
                                     y_max=self.HistTdc.shape[1],
                                     scan_parameters=scan_parameter_range,
                                     electron_axis=True,
                                     scan_parameter_name=scan_parameter_name,
                                     title='TDC Scan Parameter Histogram',
                                     ylabel='TDC code',
                                     suffix='tdc_param_hist')
        except Exception:
            self.log.error('Could not create tdc param histogram plot!')

    def create_threshold_plot(self, logscale=False, scan_parameter_name='Scan parameter'):
        try:
            title = 'Threshold distribution for enabled pixels'
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'in_time_threshold_scan', 'crosstalk_scan']:
                plot_range = [v - self.scan_config['VCAL_MED'] for v in range(self.scan_config['VCAL_HIGH_start'],
                                                                              self.scan_config['VCAL_HIGH_stop'] + 1,
                                                                              self.scan_config['VCAL_HIGH_step'])]
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] == 'autorange_threshold_scan':
                plot_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] == 'global_threshold_tuning':
                plot_range = range(self.scan_config['VTH_stop'],
                                   self.scan_config['VTH_start'],
                                   self.scan_config['VTH_step'])
                scan_parameter_name = self.scan_config['VTH_name']
                electron_axis = False
            elif self.run_config['scan_id'] == 'injection_delay_scan':
                scan_parameter_name = 'Finedelay [LSB]'
                electron_axis = False
                plot_range = range(0, 16)
                title = 'Fine delay distribution for enabled pixels'

            self._plot_distribution(self.ThresholdMap[self.Chi2Sel].T,
                                    plot_range=plot_range,
                                    electron_axis=electron_axis,
                                    x_axis_title=scan_parameter_name,
                                    title=title,
                                    log_y=logscale,
                                    print_failed_fits=True,
                                    suffix='threshold_distribution')
        except Exception as e:
            self.log.error('Could not create threshold plot! ({0})'.format(e))

    def create_stacked_threshold_plot(self, scan_parameter_name='Scan parameter'):
        try:
            flavor = rd53a.get_flavor(self.scan_config['stop_column'] - 1)
            if flavor == 'SYNC':
                return
            min_tdac, max_tdac, range_tdac, _ = rd53a.get_tdac_range(rd53a.get_flavor(self.scan_config['stop_column'] - 1))
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'in_time_threshold_scan', 'crosstalk_scan']:
                plot_range = [v - self.scan_config['VCAL_MED'] for v in range(self.scan_config['VCAL_HIGH_start'],
                                                                              self.scan_config['VCAL_HIGH_stop'] + 1,
                                                                              self.scan_config['VCAL_HIGH_step'])]
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] in ['fast_threshold_scan', 'fast_threshold_scan_ptot']:
                plot_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] == 'global_threshold_tuning':
                plot_range = list(range(self.scan_config['VTH_stop'],
                                        self.scan_config['VTH_start'],
                                        self.scan_config['VTH_step']))
                scan_parameter_name = self.scan_config['VTH_name']
                electron_axis = False
            elif self.run_config['scan_id'] == 'autorange_threshold_scan':
                plot_range = np.array(self.scan_params[:]['vcal_high'] - self.scan_params[:]['vcal_med'], dtype=np.float)
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True

            self._plot_stacked_threshold(data=self.ThresholdMap[self.Chi2Sel].T,
                                         tdac_mask=self.tdac_mask[self.Chi2Sel].T,
                                         plot_range=plot_range,
                                         electron_axis=electron_axis,
                                         x_axis_title=scan_parameter_name,
                                         title='Threshold distribution for enabled pixels',
                                         suffix='tdac_threshold_distribution',
                                         min_tdac=min(min_tdac, max_tdac),
                                         max_tdac=max(min_tdac, max_tdac),
                                         range_tdac=range_tdac)
        except Exception:
            self.log.error('Could not create stacked threshold plot!')

    def create_threshold_map(self):
        try:
            mask = self.enable_mask.copy()
            sel = self.Chi2Map[:] > 0.  # Mask not converged fits (chi2 = 0)
            mask[~sel] = True
            if self.run_config['scan_id'] == 'injection_delay_scan':
                electron_axis = False
                use_electron_offset = False
                z_label = 'Finedelay [LSB]'
                title = 'Injection Delay'
                z_min = 0
                z_max = 16
            else:
                electron_axis = True
                use_electron_offset = True
                z_label = 'Threshold'
                title = 'Threshold'
                z_min = None
                z_max = None

            self._plot_occupancy(hist=np.ma.masked_array(self.ThresholdMap, mask).T,
                                 electron_axis=electron_axis,
                                 z_label=z_label,
                                 title=title,
                                 use_electron_offset=use_electron_offset,
                                 show_sum=False,
                                 z_min=z_min,
                                 z_max=z_max,
                                 suffix='threshold_map')
        except Exception:
            self.log.error('Could not create threshold map!')

    def create_noise_plot(self, logscale=False, scan_parameter_name='Scan parameter'):
        try:
            plot_range = None
            if self.run_config['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'in_time_threshold_scan', 'autorange_threshold_scan', 'crosstalk_scan']:
                scan_parameter_name = '$\\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] == 'global_threshold_tuning':
                scan_parameter_name = self.scan_config['VTH_name']
                electron_axis = False
            elif self.run_config['scan_id'] == 'injection_delay_scan':
                electron_axis = False
                scan_parameter_name = 'Finedelay [LSB]'

            self._plot_distribution(self.NoiseMap[self.Chi2Sel].T,
                                    title='Noise distribution for enabled pixels',
                                    plot_range=plot_range,
                                    electron_axis=electron_axis,
                                    use_electron_offset=False,
                                    x_axis_title=scan_parameter_name,
                                    y_axis_title='# of hits',
                                    log_y=logscale,
                                    print_failed_fits=True,
                                    suffix='noise_distribution')
        except Exception:
            self.log.error('Could not create noise plot!')

    def create_noise_map(self):
        try:
            mask = self.enable_mask.copy()
            sel = self.Chi2Map[:] > 0.  # Mask not converged fits (chi2 = 0)
            mask[~sel] = True
            z_label = 'Noise'
            title = 'Noise'
            electron_axis = True,

            if self.run_config['scan_id'] == 'injection_delay_scan':
                z_label = 'Finedelay [LSB]'
                title = 'Injection Delay Noise'
                electron_axis = False
            self._plot_occupancy(hist=np.ma.masked_array(self.NoiseMap, mask).T,
                                 electron_axis=electron_axis,
                                 use_electron_offset=False,
                                 z_label=z_label,
                                 z_max='median',
                                 title=title,
                                 show_sum=False,
                                 suffix='noise_map')
        except Exception:
            self.log.error('Could not create noise map!')

    def create_tdac_plot(self):
        try:
            flavor = rd53a.get_flavor(self.scan_config['stop_column'] - 1)
            if flavor == 'SYNC':
                return
            min_tdac, max_tdac, _, tdac_incr = rd53a.get_tdac_range(flavor)
            mask = self.enable_mask.copy()
            mask[0:128, :] = True
            if min_tdac > max_tdac:
                plot_range = range(max_tdac, min_tdac - tdac_incr, -1 * tdac_incr)
            else:
                plot_range = range(min_tdac, max_tdac + tdac_incr, tdac_incr)
            self._plot_distribution(self.tdac_mask[~mask].T,
                                    plot_range=plot_range,
                                    title='TDAC distribution for enabled pixels',
                                    x_axis_title='TDAC',
                                    y_axis_title='# of hits',
                                    align='center',
                                    suffix='tdac_distribution')
        except Exception:
            self.log.error('Could not create TDAC plot!')

    def create_tdac_map(self):
        try:
            flavor = rd53a.get_flavor(self.scan_config['stop_column'] - 1)
            if flavor == 'SYNC':
                return
            min_tdac, max_tdac, _, tdac_incr = rd53a.get_tdac_range(flavor)
            mask = self.enable_mask.copy()
            self._plot_fancy_occupancy(hist=np.ma.masked_array(self.tdac_mask, mask).T,
                                       title='TDAC map',
                                       z_label='TDAC',
                                       z_min=min(min_tdac, max_tdac),
                                       z_max=max(min_tdac, max_tdac),
                                       log_z=False,
                                       norm_projection=True)
        except Exception:
            self.log.error('Could not create TDAC map!')

    def create_chi2_map(self):
        try:
            mask = self.enable_mask.copy()
            chi2 = self.Chi2Map[:]
            sel = chi2 > 0.  # Mask not converged fits (chi2 = 0)
            mask[~sel] = True

            self._plot_occupancy(hist=np.ma.masked_array(chi2, mask).T,
                                 z_label='Chi2/ndf.',
                                 z_max='median',
                                 title='Chi2 over ndf of S-Curve fits',
                                 show_sum=False,
                                 suffix='chi2_map')
        except Exception:
            self.log.error('Could not create chi2 map!')

    def create_dac_linearity_plot(self, adc_ref=0.8, I_ref=4, y_title=None):
        dac_data = [(dac[1], dac[2]) for dac in self.DAC_data]
        voltage_data = [(dac[1], dac[3]) for dac in self.DAC_data if dac[3] != 0.0]

        try:
            self._plot_dac_linearity(data=dac_data,
                                     title=self.scan_config.get('DAC', '') +
                                     ' Linearity internal ADC',
                                     y_axis_title='ADC [LSB]',
                                     x_axis_title=self.scan_config.get('DAC', 'External DAC/Voltage'))
        except Exception:
            self.log.error('Could not create DAC linearity plot from ADC data!')

        try:
            if self.scan_config['type'] == 'U':
                y_axis_title = 'Voltage [mV]'
            else:
                y_axis_title = 'Current [$\\mu$A]'
            if y_title:
                y_axis_title = y_title
            if any(voltage_data):
                self._plot_dac_linearity(data=voltage_data,
                                         title=self.scan_config.get('DAC', '') + ' Linearity external multimeter',
                                         y_axis_title=y_axis_title,
                                         x_axis_title=self.scan_config.get('DAC', 'External DAC') + ' [LSB]',
                                         second_axis=False,
                                         suffix='linearity_2',
                                         I_ref=I_ref)
        except Exception:
            self.log.error('Could not create DAC linearity plot from multimeter data!')

        try:
            if self.scan_config['value_step'] == 1:
                non_linearity = au.calculate_inl_dnl(dac_data)
                self._plot_dac_non_linearity(data=non_linearity,
                                             title=self.scan_config.get('DAC', '') + ' Non-Linearity',
                                             typ='INL',
                                             y_axis_title='INL [LSB]',
                                             x_axis_title=self.scan_config.get('DAC', '') + ' [LSB]',
                                             suffix='INL')

                self._plot_dac_non_linearity(data=non_linearity,
                                             title=self.scan_config.get('DAC', '') + ' Non-Linearity',
                                             typ='DNL',
                                             y_axis_title='DNL [LSB]',
                                             x_axis_title=self.scan_config.get('DAC', '') + ' [LSB]',
                                             suffix='DNL')
        except Exception:
            self.log.error('Could not create INL/DNL plots!')

    def create_cluster_size_plot(self):
        try:
            self._plot_cl_size(self.HistClusterSize)
        except Exception:
            self.log.error('Could not create cluster size plot!')

    def create_cluster_tot_plot(self):
        try:
            self._plot_cl_tot(self.HistClusterTot)
        except Exception:
            self.log.error('Could not create cluster TOT plot!')

    def create_cluster_shape_plot(self):
        try:
            self._plot_cl_shape(self.HistClusterShape)
        except Exception:
            self.log.error('Could not create cluster shape plot!')

    def create_hit_pix_plot(self):
        try:
            occ_1d = np.ma.masked_array(self.HistOcc[:].sum(axis=2), self.enable_mask).ravel()

            if occ_1d.sum() == 0:
                plot_range = np.arange(0, 100, 1)
            else:
                plot_range = np.arange(0, occ_1d.max() + 0.05 * occ_1d.max(), occ_1d.max() / 100)
            self._plot_distribution(data=occ_1d,
                                    plot_range=plot_range,
                                    title='Hits per Pixel',
                                    x_axis_title='# of Hits',
                                    y_axis_title='# of Pixel',
                                    log_y=True,
                                    align='center',
                                    fit_gauss=False,
                                    suffix='hit_pix')
        except Exception:
            self.log.error('Could not create hits per pixel plot!')

    def create_occupancy_core_map(self):
        try:
            self._plot_one_core_overlay(self.HistOcc,
                                        title='Core-Integrated Occupancy-Plot_DIFF',
                                        cb_label='Occupancy avg',
                                        suffix='occ_core')
        except Exception:
            self.log.error('Could not create core overlay plot!')

    def create_cluster_vs_bias_plot(self):
        try:
            data = [[], []]
            for st in self.iv_data:
                data[0].append(st[0])
                data[1].append(abs(st[1]))

            if data[0][-1] < 0:
                invert_x = True
            else:
                invert_x = False

            self._plot_2d_scatter(data=data,
                                  title='Number of clusters vs. bias voltage',
                                  x_axis_title='Bias voltage [V]',
                                  y_axis_title='# of clusters',
                                  invert_x=invert_x,
                                  suffix='cluster_vs_bias')
        except Exception:
            self.log.error('Could not create IV curve plot!')

    def create_parametric_threshold_plot(self):
        try:
            param_name = self.scan_config.get('parameter_name', 'Parameter')
            parameter, threshold = [], []
            for st in self.parametric_data:
                parameter.append(st[0])
                threshold.append(self._convert_to_e(st[1], use_offset=True)[0])

            self._plot_2d_scatter(data=[parameter, threshold],
                                  title='Mean threshold vs. {0}'.format(param_name),
                                  x_axis_title=param_name,
                                  y_axis_title='Mean Threshold [Electrons]',
                                  suffix='threshold_vs_parameter')
        except Exception:
            self.log.error('Could not create parametric threshold plot!')

    def create_parametric_noise_plot(self):
        try:
            param_name = self.scan_config.get('parameter_name', 'Parameter')
            parameter, noise = [], []
            for st in self.parametric_data:
                parameter.append(st[0])
                noise.append(self._convert_to_e(st[2], use_offset=False)[0])

            self._plot_2d_scatter(data=[parameter, noise],
                                  title='Mean noise vs. {0}'.format(param_name),
                                  x_axis_title=param_name,
                                  y_axis_title='Mean Noise [Electrons]',
                                  suffix='noise_vs_parameter')
        except Exception:
            self.log.error('Could not create parametric noise plot!')

    def create_timewalk_plots(self, hist_tdc_dist, hist_tdc_dist_single_pixel, hist_tdc_dist_corr, xedges_timewalk, yedges_timewalk, mean_single_pixel, perc_high_single_pixel,
                              perc_low_single_pixel, mean_pixel_corr, perc_high_pixel_corr, perc_low_pixel_corr, stat_2d_tdc_mean_hists, selected_pixel, delta_vcal_range, max_timewalk, max_hit_delay=25.0):
        '''
        Plot timewalk histograms.
        '''

        self.log.info('Plotting timewalk histograms...')
        try:
            for par in range(len(delta_vcal_range)):
                # Plot 2D timewalk map
                actual_2d_tdc_mean_hist = stat_2d_tdc_mean_hists[:, :, par]
                hist = np.ma.masked_where(condition=np.logical_or(actual_2d_tdc_mean_hist == 0, ~np.isfinite(actual_2d_tdc_mean_hist)),
                                          a=actual_2d_tdc_mean_hist)
                self._plot_occupancy(hist=hist.T * 1.5625,
                                     title='Mean Timewalk (in ns) @ $\\Delta$ VCAL = %i' % delta_vcal_range[par],
                                     z_label='Mean timewalk / ns',
                                     show_sum=False)

                # Plot trigger distance distribution
                tdc_range = np.arange(0, hist_tdc_dist.shape[1], 1)
                self._plot_1d_hist(hist=hist_tdc_dist[par, :],
                                   title='TDC Trigger Distance for $\\Delta$ VCAL = %i' % delta_vcal_range[par],
                                   x_axis_title='TDC Trigger Distance / LSB',
                                   y_axis_title='# of hits',
                                   color='b',
                                   plot_range=tdc_range,
                                   log_y=True)

            # Single pixel: Find in-time threshold (charge at which timewalk is max_hit_delay ns) by interpolating measured data functions
            tck = interpolate.splrep(delta_vcal_range, mean_single_pixel - max_hit_delay, s=0)
            try:
                in_time_thr_pixel = interpolate.sproot(tck, mest=delta_vcal_range[np.argmin(np.abs(mean_single_pixel - max_hit_delay))])[-1]  # in Delta VCAL
            except Exception:
                in_time_thr_pixel = None
                self.log.warning('Could not determine in-time threshold (for single pixel)!')

            # All pixels: Find in-time threshold (charge at which timewalk is max_hit_delay ns) by interpolating measured data functions
            tck = interpolate.splrep(delta_vcal_range, mean_pixel_corr - max_hit_delay, s=0)
            try:
                in_time_thr_pixel_corr = interpolate.sproot(tck, mest=delta_vcal_range[np.argmin(np.abs(mean_pixel_corr - max_hit_delay))])[-1]  # in Delta VCAL
            except Exception:
                in_time_thr_pixel_corr = None
                self.log.warning('Could not determine in-time threshold (all pixels)!')

            # Plot TDC trigger distance [ns] vs charge as 2D map (all pixels, no offset correction)
            self._plot_2d_timewalk_hist(hist=hist_tdc_dist, xedges=xedges_timewalk, yedges=yedges_timewalk, title='TDC Trigger Distance')

            # Plot TDC trigger distance [ns] vs charge as 2D map (all pixels, with individual offset correction)
            self._plot_2d_timewalk_hist(hist=hist_tdc_dist_corr, xedges=xedges_timewalk, yedges=yedges_timewalk, title='Timewalk (per pixel offset correction)')

            # Plot TDC trigger distance [ns] vs charge as 2D map (single pixel, offset corrected)
            self._plot_2d_timewalk_hist(hist=hist_tdc_dist_single_pixel, xedges=xedges_timewalk, yedges=yedges_timewalk, title='Timewalk for pixel (%i, %i)' % (selected_pixel[0], selected_pixel[1]))

            # Plot mean timewalk vs charge as scatter plot (single pixel)
            self._plot_timewalk_scatter(x=delta_vcal_range, y=mean_single_pixel,
                                        perc_low=perc_low_single_pixel, perc_high=perc_high_single_pixel, in_time_thr=in_time_thr_pixel,
                                        label='Mean Timewalk for pixel (%i, %i)' % (selected_pixel[0], selected_pixel[1]),
                                        max_timewalk=max_timewalk, single_pixel=True)

            # Plot mean timewalk vs charge as scatter plot and 2D map as overlay (single pixel)
            self._plot_timewalk_scatter(x=delta_vcal_range, y=mean_single_pixel,
                                        perc_low=perc_low_single_pixel, perc_high=perc_high_single_pixel, in_time_thr=in_time_thr_pixel,
                                        label='Mean Timewalk for pixel (%i, %i)' % (selected_pixel[0], selected_pixel[1]),
                                        hist=hist_tdc_dist_single_pixel, xedges=xedges_timewalk, yedges=yedges_timewalk,
                                        max_timewalk=max_timewalk, single_pixel=True)

            # Plot mean timewalk vs charge as scatter plot and 2D map as overlay (single pixel)
            self._plot_timewalk_scatter(x=delta_vcal_range, y=mean_single_pixel,
                                        perc_low=perc_low_single_pixel, perc_high=perc_high_single_pixel, in_time_thr=in_time_thr_pixel,
                                        label='Mean Timewalk for pixel (%i, %i)' % (selected_pixel[0], selected_pixel[1]),
                                        hist=hist_tdc_dist_single_pixel, xedges=xedges_timewalk, yedges=yedges_timewalk,
                                        max_timewalk=max_timewalk, plot_range=(delta_vcal_range[0], 500), single_pixel=True)

            # Plot mean timewalk vs charge as scatter plot (all pixels, with individual offset correction)
            self._plot_timewalk_scatter(x=delta_vcal_range, y=mean_pixel_corr,
                                        perc_low=perc_low_pixel_corr, perc_high=perc_high_pixel_corr, in_time_thr=in_time_thr_pixel_corr,
                                        label='Mean Timewalk (each pixel corrected)',
                                        max_timewalk=max_timewalk)

            # Plot mean timewalk vs charge as scatter plot and 2D map (all pixels, with individual offset correction)
            self._plot_timewalk_scatter(x=delta_vcal_range, y=mean_pixel_corr,
                                        perc_low=perc_low_pixel_corr, perc_high=perc_high_pixel_corr, in_time_thr=in_time_thr_pixel_corr,
                                        label='Mean Timewalk (each pixel corrected)',
                                        hist=hist_tdc_dist_corr, xedges=xedges_timewalk, yedges=yedges_timewalk,
                                        max_timewalk=max_timewalk)

            # Plot mean timewalk vs charge as scatter plot and 2D map (all pixels, with individual offset correction, close-up)
            self._plot_timewalk_scatter(x=delta_vcal_range, y=mean_pixel_corr,
                                        perc_low=perc_low_pixel_corr, perc_high=perc_high_pixel_corr, in_time_thr=in_time_thr_pixel_corr,
                                        label='Mean Timewalk (each pixel corrected)',
                                        hist=hist_tdc_dist_corr, xedges=xedges_timewalk, yedges=yedges_timewalk,
                                        max_timewalk=max_timewalk, plot_range=(delta_vcal_range[0], 500))

        except Exception as e:
            self.log.error('Could not create timewalk plots! {0}'.format(e))

    ''' Internal functions '''

    def _mask_disabled_pixels(self, enable_mask, scan_config):
        mask = np.invert(enable_mask)
        mask[:scan_config['start_column'], :] = True
        mask[scan_config['stop_column']:, :] = True
        mask[:, :scan_config['start_row']] = True
        mask[:, scan_config['stop_row']:] = True

        return mask

    def _save_plots(self, fig, suffix=None, tight=False):
        increase_count = False
        bbox_inches = 'tight' if tight else ''
        if suffix is None:
            suffix = str(self.plot_cnt)
        self.out_file.savefig(fig, bbox_inches=bbox_inches)
        if self.save_png:
            fig.savefig(self.filename[:-4] + '_' +
                        suffix + '.png', bbox_inches=bbox_inches)
            increase_count = True
        if self.save_single_pdf:
            fig.savefig(self.filename[:-4] + '_' +
                        suffix + '.pdf', bbox_inches=bbox_inches)
            increase_count = True
        if increase_count:
            self.plot_cnt += 1

    def _gauss(self, x, *p):
        amplitude, mu, sigma = p
        return amplitude * np.exp(- (x - mu)**2.0 / (2.0 * sigma**2.0))

    def _double_gauss(self, x, a1, a2, m1, m2, sd1, sd2):
        return self._gauss(x, a1, m1, sd1) + self._gauss(x, a2, m2, sd2)

    def _lin(self, x, *p):
        m, b = p
        return m * x + b

    def _add_text(self, fig):
        fig.subplots_adjust(top=0.85)
        y_coord = 0.92
        if self.qualitative:
            fig.text(0.1, y_coord, '{0} qualitative'.format(self.chip_type), fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
            fig.text(0.7, y_coord, 'Chip S/N: 0x0000', fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
        else:
            fig.text(0.1, y_coord, '{0} {1}'.format(self.chip_type, self.level), fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
            chip_sn = self.run_config['chip_sn']
            fig.text(0.7, y_coord, 'Chip S/N: {0}'.format(chip_sn), fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
        if self.internal:
            fig.text(0.1, 1, 'RD53 Internal', fontsize=16, color='r', rotation=45,
                     bbox=dict(boxstyle='round', facecolor='white', edgecolor='red', alpha=0.7), transform=fig.transFigure)

    def _convert_to_e(self, dac, use_offset=True):
        if use_offset:
            e = dac * self.calibration['e_conversion_slope'] + self.calibration['e_conversion_offset']
            de = math.sqrt((dac * self.calibration['e_conversion_slope_error'])**2 + self.calibration['e_conversion_offset_error']**2)
        else:
            e = dac * self.calibration['e_conversion_slope']
            de = dac * self.calibration['e_conversion_slope_error']
        return e, de

    def _add_electron_axis(self, fig, ax, use_electron_offset=True):
        fig.subplots_adjust(top=0.75)
        ax.title.set_position([.5, 1.15])

        fig.canvas.draw()
        ax2 = ax.twiny()

        xticks = []
        for t in ax.get_xticks(minor=False):
            xticks.append(int(self._convert_to_e(float(t), use_offset=use_electron_offset)[0]))

        ax2.set_xticklabels(xticks)

        l1 = ax.get_xlim()
        l2 = ax2.get_xlim()

        def f(x):
            return l2[0] + (x - l1[0]) / (l1[1] - l1[0]) * (l2[1] - l2[0])

        ticks = f(ax.get_xticks())
        ax2.xaxis.set_major_locator(matplotlib.ticker.FixedLocator(ticks))
        ax2.set_xlabel('Electrons', labelpad=7)

        return ax2

    def _plot_parameter_page(self):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.axis('off')
        if self.chip_type.lower() == 'rd53a':
            logo = plt.imread(os.path.join(os.path.dirname(__file__), '..', 'chips', 'RD53A_logo.bmp'))
            logo = logo[::4, ::4]
        if self.chip_type.lower() in ['itkpixv1', 'crocv1']:
            logo = plt.imread(os.path.join(os.path.dirname(__file__), '..', 'chips', 'RD53B_logo.bmp'))
            logo = logo[::8, ::8]
        fig.figimage(logo, fig.bbox.xmax - 50 - 10, fig.bbox.ymax - 50 - 10)

        scan_id = self.run_config['scan_id']
        run_name = self.run_config['run_name']
        chip_sn = self.run_config['chip_sn']

        sw_ver = self.run_config['software_version']
        timestamp = datetime.datetime.strptime(' '.join(run_name.split('_')[:2]), '%Y%m%d %H%M%S')  # FIXME: Workaround while there is no timestamp saved in h5 file

        text = 'This is a bdaq53 {0} for chip {1}.\nRun {2} was started {3}.'.format(scan_id, chip_sn, run_name, timestamp)

        ax.text(0.01, 0.9, text, fontsize=10)
        ax.text(-0.1, -0.11, 'Software version: {0}'.format(sw_ver), fontsize=3)

        if scan_id in ['threshold_scan', 'fast_threshold_scan', 'fast_threshold_scan_ptot', 'autorange_threshold_scan', 'crosstalk_scan']:
            txt = 'Charge calibration: $y \\; [e^-] = x \\; [\\Delta VCAL] \\cdot ({0:1.2f} \\pm {1:1.2f}) \\; [\\frac{{e^-}}{{\\Delta VCAL}}] + ({2:1.0f} \\pm {3:1.2f}) \\; [e^-]$'.format(self.calibration['e_conversion_slope'],
                                                                                                                                                                                             self.calibration['e_conversion_slope_error'],
                                                                                                                                                                                             self.calibration['e_conversion_offset'],
                                                                                                                                                                                             self.calibration['e_conversion_offset_error'])
            ax.text(0.01, 0.02, txt, fontsize=6)

        if 'maskfile' in self.scan_config.keys() and self.scan_config['maskfile'] is not None and not self.scan_config['maskfile'] == 'None':
            ax.text(0.01, -0.05, 'Maskfile:\n{0}'.format(self.scan_config['maskfile']), fontsize=6)

        scan_config_dict = OrderedDict()
        dac_dict = OrderedDict()
        scan_config_trg_tdc_dict = OrderedDict()

        exclude_run_conf_items = ['scan_id', 'run_name', 'timestamp', 'chip_sn', 'software_version', 'maskfile', 'TDAC']
        run_conf_trg_tdc = ['TRIGGER_MODE', 'TRIGGER_SELECT', 'TRIGGER_INVERT', 'TRIGGER_LOW_TIMEOUT', 'TRIGGER_VETO_SELECT',
                            'TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES', 'DATA_FORMAT', 'EN_TLU_VETO', 'TRIGGER_DATA_DELAY',
                            'EN_WRITE_TIMESTAMP', 'EN_TRIGGER_DIST', 'EN_NO_WRITE_TRIG_ERR', 'EN_INVERT_TDC', 'EN_INVERT_TRIGGER']

        for key, value in sorted(self.scan_config.items()):
            if key not in (exclude_run_conf_items + run_conf_trg_tdc):
                if key == 'module' and value.startswith('module_'):  # Nice formatting
                    value = value.split('module_')[1]
                if key == 'trigger_pattern':  # Nice formatting
                    value = hex(value)
                scan_config_dict[key] = value
            if key in run_conf_trg_tdc:
                scan_config_trg_tdc_dict[key] = value

        for flavor in DACS.keys():
            dac_dict[flavor] = OrderedDict()
            for reg, value in self.registers.items():
                if self.chip_type.lower() == 'rd53a':
                    if reg in DACS[flavor] and flavor in reg:
                        dac_dict[flavor][reg] = value
                elif self.chip_type.lower() == 'itkpixv1':
                    if reg in DACS[flavor]:
                        dac_dict[flavor][reg] = value
        tb_list = []
        for i in range(max(len(scan_config_dict), len(dac_dict['SYNC']), len(dac_dict['LIN']), len(dac_dict['DIFF']), len(dac_dict['ITKPIXV1']), len(scan_config_dict))):
            try:
                key1 = list(scan_config_dict.keys())[i]
                value1 = scan_config_dict[key1]
            except IndexError:
                key1 = ''
                value1 = ''
            try:
                key2 = list(dac_dict['SYNC'].keys())[i]
                value2 = dac_dict['SYNC'][key2]
            except IndexError:
                key2 = ''
                value2 = ''
            try:
                key3 = list(dac_dict['LIN'].keys())[i]
                value3 = dac_dict['LIN'][key3]
            except IndexError:
                key3 = ''
                value3 = ''
            if self.chip_type.lower() == 'rd53a':
                try:
                    key4 = list(dac_dict['DIFF'].keys())[i]
                    value4 = dac_dict['DIFF'][key4]
                except IndexError:
                    key4 = ''
                    value4 = ''
            elif self.chip_type.lower() == 'itkpixv1':
                try:
                    key4 = list(dac_dict['ITKPIXV1'].keys())[i]
                    value4 = dac_dict['ITKPIXV1'][key4]
                except IndexError:
                    key4 = ''
                    value4 = ''
            try:
                key5 = list(scan_config_dict.keys())[i]
                value5 = scan_config_dict[key5]
            except IndexError:
                key5 = ''
                value5 = ''

            if scan_config_trg_tdc_dict:
                tb_list.append([key1, value1, '', key2, value2, '', key3, value3, '', key4, value4, '', key5, value5])
            elif self.chip_type.lower() == 'itkpixv1':
                tb_list.append([key1, value1, '', key4, value4])
            else:
                tb_list.append([key1, value1, '', key2, value2, '', key3, value3, '', key4, value4])

        if self.chip_type.lower() == 'rd53a':
            if scan_config_trg_tdc_dict:
                widths = [0.18, 0.10, 0.03, 0.18, 0.10, 0.03, 0.18, 0.10, 0.03, 0.18, 0.10, 0.03, 0.18, 0.10]
                labels = ['Scan config', 'Value', '', 'SYNC config', 'Value', '', 'LIN config', 'Value', '', 'DIFF config', 'Value', '', 'Trg/TDC config', 'Value']
            else:
                widths = [0.18, 0.10, 0.03, 0.18, 0.10, 0.03, 0.18, 0.10, 0.03, 0.18, 0.10]
                labels = ['Scan config', 'Value', '', 'SYNC config', 'Value', '', 'LIN config', 'Value', '', 'DIFF config', 'Value']
        else:
            if scan_config_trg_tdc_dict:
                widths = [0.18, 0.10, 0.03, 0.18, 0.10, 0.03, 0.18, 0.10]
                labels = ['Scan config', 'Value', '', 'ITkPixV1 config', 'Value', '', 'Trg/TDC config', 'Value']
            else:
                widths = [0.18, 0.10, 0.03, 0.18, 0.10]
                labels = ['Scan config', 'Value', '', 'ITkPixV1 config', 'Value']
        table = ax.table(cellText=tb_list, colWidths=widths, colLabels=labels, cellLoc='left', loc='center')
        table.scale(0.8, 0.8)
        table.auto_set_font_size(False)

        for key, cell in table.get_celld().items():
            cell.set_fontsize(3.5)
            row, col = key
            if row == 0:
                cell.set_color('#ffb300')
                cell.set_fontsize(5)
            if col in [2, 5, 8, 11]:
                cell.set_color('white')
            if col in [1, 4, 7, 10, 13]:
                cell._loc = 'center'

        self._save_plots(fig, suffix='parameter_page')

    def _plot_2d_scatter(self, data, title=None, x_axis_title=None, y_axis_title=None, invert_x=False, invert_y=False, log_y=False, color='b', suffix=None):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.plot(data[0], data[1], 'o', color=color)

        if title is not None:
            ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        ax.grid()

        if invert_x:
            ax.invert_xaxis()
        if invert_y:
            ax.invert_yaxis()
        if log_y:
            ax.set_yscale('log')

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_1d_hist(self, hist, yerr=None, title=None, x_axis_title=None, y_axis_title=None, x_ticks=None, color='r',
                      plot_range=None, log_y=False, suffix=None):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        hist = np.array(hist)
        if plot_range is None:
            plot_range = range(0, len(hist))
        plot_range = np.array(plot_range)
        plot_range = plot_range[plot_range < len(hist)]
        if yerr is not None:
            ax.bar(x=plot_range, height=hist[plot_range],
                   color=color, align='center', yerr=yerr)
        else:
            ax.bar(x=plot_range,
                   height=hist[plot_range], color=color, align='center')
        ax.set_xlim((min(plot_range) - 0.5, max(plot_range) + 0.5))

        ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        if x_ticks is not None:
            ax.set_xticks(plot_range)
            ax.set_xticklabels(x_ticks)
            ax.tick_params(which='both', labelsize=8)
        if np.allclose(hist, 0.0):
            ax.set_ylim((0, 1))
        else:
            if log_y:
                ax.set_yscale('log')
                ax.set_ylim((1e-1, np.amax(hist) * 2))
        ax.grid(True)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        # Add mean and rms in case of TOT plot
        if suffix == 'tot' and not self.qualitative:
            textright = '$\mu={0:1.1f}$\nRMS$={1:1.1f}$'.format(
                au.get_mean_from_histogram(hist, np.arange(hist.shape[0])), au.get_std_from_histogram(hist, np.arange(hist.shape[0])))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax.text(0.05, 0.9, textright, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

        self._save_plots(fig, suffix=suffix)

    def _plot_1d_vs_time(self, data, title=None, x_axis_title='Timestamp', y_axis_title=None, invert_x=False, invert_y=False, log_y=False, color='b', suffix=None):
        '''
            Plot 1-dimensional data (e.g. error count) vs. time.
            Data format:
            data : list of lists
                first list (x-axis) : Unix timestamps
                second list (y-axis) : data
        '''

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        timestamps = []
        for i in range(len(data[0])):
            timestamps.append(datetime.datetime.fromtimestamp(data[0][i]))

        ax.plot(timestamps, data[1], 'o', color=color)

        if title is not None:
            ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        ax.grid()

        if invert_x:
            ax.invert_xaxis()
        if invert_y:
            ax.invert_yaxis()
        if log_y:
            ax.set_yscale('log')

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_tot(self, hist, title=None, plot_range=range(0, 16)):
        if title is None:
            if self.qualitative:
                title = 'Time-over-Threshold distribution'
            else:
                title = ('Time-over-Threshold distribution ($\\Sigma$ = {0:1.0f})'.format(np.sum(hist)))
        self._plot_1d_hist(hist=hist, title=title, log_y=True, plot_range=plot_range,
                           x_axis_title='ToT code', y_axis_title='# of hits', color='b', suffix='tot')

    def _plot_ptot(self, hist, title=None, plot_range=range(0, 16)):
        if title is None:
            if self.qualitative:
                title = 'Precision Time-over-Threshold distribution'
            else:
                title = ('Precision Time-over-Threshold distribution ($\\Sigma$ = {0:1.0f})'.format(np.sum(hist)))
        self._plot_1d_hist(hist=hist, title=title, log_y=True, plot_range=plot_range,
                           x_axis_title='PToT code', y_axis_title='# of hits', color='b', suffix='ptot')

    def _plot_relative_bcid(self, hist, title=None):
        if title is None:
            if self.qualitative:
                title = 'Relative BCID'
            else:
                title = ('Relative BCID' + ' ($\Sigma$ = {0:1.0f})'.format(np.sum(hist)))
        self._plot_1d_hist(hist=hist, title=title, log_y=True, plot_range=range(len(str(bin(self.scan_config['trigger_pattern']))[2:]) + 1),
                           x_axis_title='Relative BCID [25 ns]', y_axis_title='# of hits', suffix='rel_bcid')

    def _plot_event_status(self, hist, title=None):
        self._plot_1d_hist(hist=hist,
                           title=('Event status ($\\Sigma$ = {0:1.0f})'.format(np.sum(hist))) if title is None else title,
                           log_y=True,
                           plot_range=range(0, 11),
                           x_ticks=('User K\noccured', 'Ext\ntrigger', 'TDC\nword', 'BCID\nerror', 'TRG ID\nerror',
                                    'TDC\nambig.', 'Event\nTruncated', 'Unknown\nword', 'Wrong\nstructure', 'Ext. Trig.\nerror', 'Wrong\nRX ID'),
                           color='g', y_axis_title='Number of events', suffix='event_status')

    def _plot_tdc_status(self, hist, title=None):
        for i in range(4):  # one histogram for each HitOr line
            self._plot_1d_hist(hist=hist[i, :],
                               title=('TDC hit status (HitOr line {0})'.format(i + 1) + ' ($\\Sigma$ = {0:1.0f})'.format(np.sum(hist[i, :]))) if title is None else title,
                               log_y=True,
                               plot_range=range(0, 4),
                               x_ticks=('TDC\nword', 'TDC\nOVF', 'TDC\nambig.', 'TDC\nerror'),
                               color='r', y_axis_title='Number of hits', suffix='tdc_status')

    def _plot_bcid_error(self, hist, title=None):
        self._plot_1d_hist(hist=hist,
                           title=('BCID error ($\\Sigma$ = {0:1.0f})'.format(np.sum(hist))) if title is None else title,
                           log_y=True,
                           plot_range=range(0, 32),
                           x_axis_title='Trigger ID',
                           y_axis_title='Number of event header', color='g',
                           suffix='bcid_error')

    def _plot_cl_size(self, hist):
        ''' Create 1D cluster size plot w/wo log y-scale '''
        self._plot_1d_hist(hist=hist, title='Cluster size',
                           log_y=False, plot_range=range(0, 10),
                           x_axis_title='Cluster size',
                           y_axis_title='# of clusters', suffix='cluster_size')
        self._plot_1d_hist(hist=hist, title='Cluster size (log)',
                           log_y=True, plot_range=range(0, 100),
                           x_axis_title='Cluster size',
                           y_axis_title='# of clusters', suffix='cluster_size_log')

    def _plot_cl_tot(self, hist):
        ''' Create 1D cluster size plot w/wo log y-scale '''
        self._plot_1d_hist(hist=hist, title='Cluster ToT',
                           log_y=False, plot_range=range(0, 96),
                           x_axis_title='Cluster ToT [25 ns]',
                           y_axis_title='# of clusters', suffix='cluster_tot')

    def _plot_cl_shape(self, hist):
        ''' Create a histogram with selected cluster shapes '''
        x = np.arange(12)
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        selected_clusters = hist[[1, 3, 5, 6, 9, 13, 14, 7, 11, 19, 261, 15]]
        ax.bar(x, selected_clusters, align='center')
        ax.xaxis.set_ticks(x)
        fig.subplots_adjust(bottom=0.2)
        ax.set_xticklabels(["\u2004\u2596",
                            # 2 hit cluster, horizontal
                            "\u2597\u2009\u2596",
                            # 2 hit cluster, vertical
                            "\u2004\u2596\n\u2004\u2598",
                            "\u259e",  # 2 hit cluster
                            "\u259a",  # 2 hit cluster
                            "\u2599",  # 3 hit cluster, L
                            "\u259f",  # 3 hit cluster
                            "\u259b",  # 3 hit cluster
                            "\u259c",  # 3 hit cluster
                            # 3 hit cluster, horizontal
                            "\u2004\u2596\u2596\u2596",
                            # 3 hit cluster, vertical
                            "\u2004\u2596\n\u2004\u2596\n\u2004\u2596",
                            # 4 hit cluster
                            "\u2597\u2009\u2596\n\u259d\u2009\u2598"])
        ax.set_title('Cluster shapes', color=TITLE_COLOR)
        ax.set_xlabel('Cluster shape')
        ax.set_ylabel('# of clusters')
        ax.grid(True)
        ax.set_yscale('log')
        ax.set_ylim(ymin=1e-1)

        if self.qualitative:
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix='cluster_shape')

    def _plot_boolean_map(self, hist, z_label, title, suffix):
        '''
            Create a map with values between 0 and 1.
            FIXME / TODO: If value is larger 0.99 (i.e. e.g. 1), then it is changed by this function
            to 0.99 prior to plotting in order to plot the value within scale [0,1].
        '''
        hist = hist.copy()
        hist[hist > 0.99] = 0.99
        self._plot_occupancy(hist=np.ma.masked_array(hist, self.enable_mask).T,
                             electron_axis=False, z_label=z_label, title=title,
                             use_electron_offset=False, show_sum=False,
                             z_min=0, z_max=1, suffix=suffix, extend_upper_bound=False)

    def _plot_bump_connectivity_map(self, hist):
        ''' Create a map of (dis-)connected bump bonds '''
        self._plot_boolean_map(hist, z_label='Connected', title='Bump Bond Connection Map', suffix='bump_connectivity_map')

    def _plot_bump_connectivity_hist(self, connected_bumps, num_poorly_connected_bumps, disconnected_bumps, unknown_state_bumps):
        ''' Create a histogram of bump connectivity statuses '''

        x = np.arange(4)
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        bump_connection_state = np.array([connected_bumps, num_poorly_connected_bumps, disconnected_bumps, unknown_state_bumps])

        ax.bar(x, bump_connection_state, align='center')
        ax.xaxis.set_ticks(x)
        fig.subplots_adjust(bottom=0.2)
        ax.set_xticklabels(['Connected', 'Poorly connected', 'Disconnected', 'Unknown'])
        ax.set_title('Bump Connection Statistics', color=TITLE_COLOR)
        ax.set_xlabel('Bump Connectivity')
        ax.set_ylabel('# Of Bumps')
        ax.grid(True)
        ax.set_yscale('log')
        ax.set_ylim(ymin=1e-1)

        if self.qualitative:
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix='bump_connectivity_hist')

    def _plot_occupancy(self, hist, electron_axis=False, use_electron_offset=True, title='Occupancy', z_label='# of hits', z_min=None, z_max=None, show_sum=True, suffix=None, extend_upper_bound=True):
        if z_max == 'median':
            z_max = 2 * np.ma.median(hist)
        elif z_max == 'maximum':
            z_max = np.ma.max(hist)
        elif z_max is None:
            try:
                z_max = np.nanpercentile(hist.filled(np.nan), q=90)
                if np.any(hist[np.isfinite(hist)] > z_max):
                    z_max = 1.1 * z_max
            except TypeError:
                z_max = np.ma.max(hist)
        if z_max < 1 or hist.all() is np.ma.masked:
            z_max = 1.0

        if z_min is None:
            z_min = np.ma.min(hist)
            if z_min < 0:
                z_min = 0
        if z_min == z_max or hist.all() is np.ma.masked:
            z_min = 0

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.set_adjustable('box')
        extent = self.plot_box_bounds
        bounds = np.linspace(start=z_min, stop=z_max + (1 if extend_upper_bound else 0), num=255, endpoint=True)
        cmap = copy.copy(cm.get_cmap('plasma'))
        cmap.set_bad('w')
        cmap.set_over('r')  # Make noisy pixels red
        cmap.set_under('g')
        norm = colors.BoundaryNorm(bounds, cmap.N)

        im = ax.imshow(hist, interpolation='none', aspect='equal', cmap=cmap, norm=norm, extent=extent)  # TODO: use pcolor or pcolormesh
        ax.set_ylim((self.plot_box_bounds[2], self.plot_box_bounds[3]))
        ax.set_xlim((self.plot_box_bounds[0], self.plot_box_bounds[1]))
        if self.qualitative or not show_sum:
            ax.set_title(title, color=TITLE_COLOR)
        else:
            ax.set_title(title + ' ($\\Sigma$ = {0})'.format((0 if hist.all() is np.ma.masked else np.ma.sum(hist))), color=TITLE_COLOR)
        ax.set_xlabel('Column')
        ax.set_ylabel('Row')

        divider = make_axes_locatable(ax)
        if electron_axis:
            pad = 1.0
        else:
            pad = 0.6

        cax = divider.append_axes("bottom", size="5%", pad=pad)
        cb = fig.colorbar(im, cax=cax, ticks=np.linspace(start=z_min, stop=z_max, num=10, endpoint=True), orientation='horizontal')
        cax.set_xticklabels([int(round(float(x.get_text().replace('\u2212', '-').encode('utf8')))) for x in cax.xaxis.get_majorticklabels()])
        cb.set_label(z_label)

        if electron_axis:
            fig.canvas.draw()
            ax2 = cb.ax.twiny()

            pos = ax2.get_position()
            pos.y1 = 0.14
            ax2.set_position(pos)

            for spine in ax2.spines.values():
                spine.set_visible(False)

            xticks = [int(round(self._convert_to_e(float(x.get_text().replace('\u2212', '-').encode('utf8')), use_offset=use_electron_offset)[0])) for x in cax.xaxis.get_majorticklabels()]
            ax2.set_xticklabels(xticks)

            l1 = cax.get_xlim()
            l2 = ax2.get_xlim()

            def f(x):
                return l2[0] + (x - l1[0]) / (l1[1] - l1[0]) * (l2[1] - l2[0])

            ticks = f(cax.get_xticks())
            ax2.xaxis.set_major_locator(matplotlib.ticker.FixedLocator(ticks))
            ax2.set_xlabel('{0} [Electrons]'.format(z_label), labelpad=7)
            cb.set_label('{0} [$\\Delta$ VCAL]'.format(z_label))

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())
            cb.formatter = plt.NullFormatter()
            cb.update_ticks()

        self._save_plots(fig, suffix=suffix)

    def _create_2d_pixel_hist(self, fig, ax, hist2d, title=None, x_axis_title=None, y_axis_title=None, z_min=0, z_max=None, cmap=None):
        extent = self.plot_box_bounds
        if z_max is None:
            if hist2d.all() is np.ma.masked:  # check if masked array is fully masked
                z_max = 1.0
            else:
                z_max = 2 * np.ma.median(hist2d)
        bounds = np.linspace(start=z_min, stop=z_max, num=255, endpoint=True)
        if cmap is None:
            cmap = copy.copy(cm.get_cmap('coolwarm'))
        cmap.set_bad('w', 1.0)
        norm = colors.BoundaryNorm(bounds, cmap.N)
        im = ax.imshow(hist2d, interpolation='none', aspect="auto", cmap=cmap, norm=norm, extent=extent)
        if title is not None:
            ax.set_title(title)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(im, boundaries=bounds, cmap=cmap, norm=norm, ticks=np.linspace(start=0, stop=z_max, num=9, endpoint=True), cax=cax)

    def _plot_three_way(self, hist, title, x_axis_title=None, z_min=None, z_max=None, bins=101, suffix='threeway'):  # the famous 3 way plot (enhanced)
        cmap = copy.copy(cm.get_cmap('plasma'))
        # TODO: set color for bad pixels
        # set nan to special value
        # masked_array = np.ma.array (a, mask=np.isnan(a))
        # cmap = matplotlib.cm.jet
        # cmap.set_bad('w',1.0)
        # ax.imshow(masked_array, interpolation='none', cmap=cmap)
        hist = np.ma.masked_invalid(hist)
        if z_min is None:
            z_min = 0.0
        elif z_min == 'minimum':
            z_min = np.ma.min(hist)
        if z_max == 'median' or z_max is None:
            z_max = 2 * np.ma.median(hist)
        elif z_max == 'maximum':
            z_max = np.ma.max(hist)
        if z_max < 1 or hist.all() is np.ma.masked:
            z_max = 1.0

        x_axis_title = '' if x_axis_title is None else x_axis_title
        fig = Figure()
        FigureCanvas(fig)
        fig.patch.set_facecolor('white')
        ax1 = fig.add_subplot(311)
        self._create_2d_pixel_hist(fig, ax1, hist, title=title, x_axis_title="column", y_axis_title="row", z_min=z_min if z_min else 0, z_max=z_max, cmap=cmap)
        ax2 = fig.add_subplot(312)
        self._create_1d_hist(ax2, hist, bins=bins, x_axis_title=x_axis_title, y_axis_title="#", x_min=z_min, x_max=z_max)
        ax3 = fig.add_subplot(313)
        self._create_pixel_scatter_plot(ax3, hist, x_axis_title='channel=row + column*{0}'.format(self.rows), y_axis_title=x_axis_title, y_min=z_min, y_max=z_max)
        self._add_text(fig)
        self._save_plots(fig, suffix=suffix)

    def _create_1d_hist(self, ax, hist, title=None, x_axis_title=None, y_axis_title=None, bins=101, x_min=None, x_max=None):
        if x_min is None:
            x_min = 0.0
        if x_max is None:
            if hist.all() is np.ma.masked:  # check if masked array is fully masked
                x_max = 1.0
            else:
                x_max = hist.max()
        hist_bins = int(x_max - x_min) + 1 if bins is None else bins
        if hist_bins > 1:
            bin_width = (x_max - x_min) / (hist_bins - 1)
        else:
            bin_width = 1.0
        hist_range = (x_min - bin_width / 2, x_max + bin_width / 2)
        masked_hist_compressed = np.ma.masked_invalid(np.ma.masked_array(hist)).compressed()
        if masked_hist_compressed.size == 0:
            ax.plot([])
        else:
            _, _, _ = ax.hist(x=masked_hist_compressed, bins=hist_bins, range=hist_range, align='mid')  # re-bin to 1d histogram, x argument needs to be 1D
        # BUG: np.ma.compressed(np.ma.masked_array(hist, copy=True)) (2D) is not equal to np.ma.masked_array(hist, copy=True).compressed() (1D) if hist is ndarray
        ax.set_xlim(hist_range)  # overwrite xlim
        if hist.all() is np.ma.masked:  # or np.allclose(hist, 0.0):
            ax.set_ylim((0, 1))
            ax.set_xlim((-0.5, +0.5))
        elif masked_hist_compressed.size == 0:  # or np.allclose(hist, 0.0):
            ax.set_ylim((0, 1))
        if title is not None:
            ax.set_title(title)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        xmin, xmax = ax.get_xlim()
        points = np.linspace(xmin, xmax, 500)
        param = norm.fit(masked_hist_compressed)
        pdf_fitted = norm.pdf(points, loc=param[0], scale=param[1]) * (len(masked_hist_compressed) * bin_width)
        ax.plot(points, pdf_fitted, "r--", label='Normal distribution')
        try:
            median = np.median(masked_hist_compressed)
        except IndexError:
            self.log.warning('Cannot create 1D histogram named {0}'.format(title))
            return
        ax.axvline(x=median, color="g")

        textleft = '$\\Sigma={0}$\n$\\mathrm{{mean\\,\\mu={1:1.2f}}}$\n$\\mathrm{{std\\,\\sigma={2:1.2f}}}$\n$\\mathrm{{median={3:1.2f}}}$'.format(len(masked_hist_compressed), param[0], param[1], median)
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(0.05, 0.9, textleft, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

    def _create_pixel_scatter_plot(self, ax, hist, title=None, x_axis_title=None, y_axis_title=None, y_min=None, y_max=None):
        scatter_y_mean = np.ma.mean(hist, axis=0)
        scatter_y = hist.flatten('F')
        ax.scatter(range(self.cols * self.rows), scatter_y, marker='o', s=0.8, rasterized=True)
        p1, = ax.plot(range(self.rows // 2, self.cols * self.rows + self.rows // 2, self.rows), scatter_y_mean, 'o')
        ax.plot(range(self.rows // 2, self.cols * self.rows + self.rows // 2, self.rows), scatter_y_mean, linewidth=2.0)
        ax.legend([p1], ['column mean'], prop={'size': 6})
        ax.set_xlim((0, self.cols * self.rows))
        if y_min is None:
            y_min = 0.0
        if y_max is None:
            if hist.all() is np.ma.masked:  # check if masked array is fully masked
                y_max = 1.0
            else:
                y_max = hist.max()
        ax.set_ylim(ymin=y_min)
        ax.set_ylim(ymax=y_max)
        if title is not None:
            ax.title(title)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)

    def _plot_fancy_occupancy(self, hist, title='Occupancy', z_label='#', z_min=None, z_max=None, log_z=True, norm_projection=False, show_sum=True, suffix='fancy_occupancy'):
        if log_z:
            title += '\n(logarithmic scale)'
        title += '\nwith projections'

        if z_min is None:
            z_min = np.ma.min(hist)
        if log_z and z_min == 0:
            z_min = 0.1
        if z_max is None:
            z_max = np.ma.max(hist)

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        extent = self.plot_box_bounds
        if log_z:
            bounds = np.logspace(start=np.log10(z_min), stop=np.log10(z_max), num=255, endpoint=True)
        else:
            bounds = np.linspace(start=z_min, stop=z_max, num=int((z_max - z_min) + 1), endpoint=True)
        cmap = copy.copy(cm.get_cmap('plasma'))
        cmap.set_bad('w')
        norm = colors.BoundaryNorm(bounds, cmap.N)

        im = ax.imshow(hist, interpolation='none', aspect='auto', cmap=cmap, norm=norm, extent=extent)  # TODO: use pcolor or pcolormesh
        ax.set_ylim((self.plot_box_bounds[2], self.plot_box_bounds[3]))
        ax.set_xlim((self.plot_box_bounds[0], self.plot_box_bounds[1]))
        ax.set_xlabel('Column')
        ax.set_ylabel('Row')

        # create new axes on the right and on the top of the current axes
        # The first argument of the new_vertical(new_horizontal) method is
        # the height (width) of the axes to be created in inches.
        divider = make_axes_locatable(ax)
        axHistx = divider.append_axes("top", 1.2, pad=0.2, sharex=ax)
        axHisty = divider.append_axes("right", 1.2, pad=0.2, sharey=ax)

        cax = divider.append_axes("right", size="5%", pad=0.1)
        if log_z:
            cb = fig.colorbar(im, cax=cax, ticks=np.logspace(start=np.log10(z_min), stop=np.log10(z_max), num=9, endpoint=True))
        else:
            cb = fig.colorbar(im, cax=cax, ticks=np.linspace(start=z_min, stop=z_max, num=int((z_max - z_min) + 1), endpoint=True))
        cb.set_label(z_label)
        # make some labels invisible
        setp(axHistx.get_xticklabels() + axHisty.get_yticklabels(), visible=False)
        if norm_projection:
            hight = np.ma.mean(hist, axis=0)
        else:
            hight = np.ma.sum(hist, axis=0)

        axHistx.bar(x=range(1, hist.shape[1] + 1), height=hight, align='center', linewidth=0)
        axHistx.set_xlim((self.plot_box_bounds[0], self.plot_box_bounds[1]))
        if hist.all() is np.ma.masked:
            axHistx.set_ylim((0, 1))
        axHistx.locator_params(axis='y', nbins=3)
        axHistx.ticklabel_format(style='sci', scilimits=(0, 4), axis='y')
        axHistx.set_ylabel(z_label)
        if norm_projection:
            width = np.ma.mean(hist, axis=1)
        else:
            width = np.ma.sum(hist, axis=1)

        axHisty.barh(y=range(1, hist.shape[0] + 1), width=width, align='center', linewidth=0)
        axHisty.set_ylim((self.plot_box_bounds[2], self.plot_box_bounds[3]))
        if hist.all() is np.ma.masked:
            axHisty.set_xlim((0, 1))
        axHisty.locator_params(axis='x', nbins=3)
        axHisty.ticklabel_format(style='sci', scilimits=(0, 4), axis='x')
        axHisty.set_xlabel(z_label)

        if self.qualitative or not show_sum:
            ax.set_title(title, color=TITLE_COLOR, x=1.35, y=1.2)
        else:
            ax.set_title(title + '\n($\\Sigma$ = {0})'.format((0 if hist.all() is np.ma.masked else np.ma.sum(hist))), color=TITLE_COLOR, x=1.35, y=1.2)

        self._save_plots(fig, suffix=suffix)

    def _plot_tot_perpixel(self, mean_tot, std_tot, scan_parameter_range,
                           ylabel='ToT Code', title='Tot curve ', suffix='tot_perpixel'):
        x_bins = scan_parameter_range

        fig = Figure()
        ax = fig.add_subplot(111)
        ax.errorbar(x_bins, mean_tot, yerr=std_tot, fmt='o', label='TOT')

        ax.ticklabel_format(useOffset=True)
        ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x))
        ax.xaxis.set_major_formatter(ticks_x)
        ax.set_ylim(bottom=0)
        ax.set_xlabel('$\\Delta$VCAL')
        ax.set_ylabel(ylabel)
        ax.set_title(title, color=TITLE_COLOR)
        ax.grid()
        ax.legend()
        self._add_text(fig)
        self._save_plots(fig, suffix=suffix)

    def _plot_tdc_tot_perpixel(self, mean_tot, mean_tdc, std_tot, std_tdc, lookup_values, scan_parameter_range, use_ptot=False, title='Tot/TDC curve ', suffix='Tot/TDC perpixel'):
        if use_ptot:
            tot_to_ns = 1.5625  # Precision TOT uses 640 MHz sampling clock
            tot_label = 'PTOT'
        else:
            tot_to_ns = 25.0  # Normal TOT uses 40 MHz sampling clock
            tot_label = 'TOT'
        mean_tot = mean_tot * tot_to_ns  # Convert TOT values into ns
        std_tot = std_tot * tot_to_ns
        mean_tdc = mean_tdc * 1.5625
        std_tdc = std_tdc * 1.5625
        x_bins = scan_parameter_range

        fig = Figure()
        ax1 = fig.add_subplot(111)
        # Plot TOT curve
        ax1.errorbar(x_bins, mean_tot, yerr=std_tot, fmt='o', label=tot_label)
        ax1.errorbar(x_bins, mean_tdc, yerr=std_tdc, fmt='o', color='g', label='TDC')
        # Plot values from lookup table
        ax1.plot(lookup_values[::4], 1.5625 * np.arange(0, len(lookup_values), 4), label='Lookup values from interpolation', ls='', marker='+')
        ax1.set_xlabel('$\\Delta$VCAL')
        ax1.set_ylabel(tot_label + '/TDC [ns]')
        ax1.set_title(title, color=TITLE_COLOR)

        # Create axes for TDC and TOT code
        ax2 = ax1.twinx()
        ax3 = ax1.twinx()
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos: '{0:g}'.format(np.int16(y / 1.5625))))
        ax2.set_ylabel('TDC', rotation=0, fontsize=9)
        ax2.spines['right'].set_position(('outward', 1))
        ax2.yaxis.set_label_coords(1.04, 1.05)
        ax2.set_ylim(ax1.get_ylim()[0], ax1.get_ylim()[-1])
        ax3.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos: '{0:g}'.format(np.int16(y / tot_to_ns))))
        ax3.set_ylabel(tot_label, rotation=0, fontsize=9)
        ax3.spines['right'].set_position(('outward', 40))
        ax3.yaxis.set_label_coords(1.15, 1.05)
        ax3.set_ylim(ax1.get_ylim()[0], ax1.get_ylim()[-1])
        ax1.grid()
        ax1.legend()
        self._add_text(fig)
        self._save_plots(fig, tight=True, suffix=suffix)

    def _plot_scurves(self, scurves, scan_parameters, electron_axis=False, scan_parameter_name=None, suffix='scurves', title='S-curves', ylabel='Occupancy', n_failed_scurves=None):
        max_occ = np.max(scurves) + 5
        n_injections = self.scan_config.get('n_injections', 100)
        y_max = int(n_injections * 1.1)
        x_bins = scan_parameters  # np.arange(-0.5, max(scan_parameters) + 1.5)
        y_bins = np.arange(-0.5, max_occ + 0.5)

        coords = {}
        for col in range(self.cols):
            for row in range(self.rows):
                coords[col * self.rows + row] = (col, row)
        noisy_pixels = []
        for param in scurves:
            for pixel_num, pixel_occ in enumerate(param):
                c = coords[pixel_num]
                if pixel_occ > y_max and c not in noisy_pixels:
                    noisy_pixels.append(c)
        n_noisy_pixels = len(noisy_pixels)

        param_count = scurves.shape[0]
        hist = np.empty([param_count, max_occ], dtype=np.uint32)

        for param in range(param_count):
            hist[param] = np.bincount(scurves[param, ~self.enable_mask.reshape((scurves.shape[-1]))], minlength=max_occ)

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        fig.patch.set_facecolor('white')
        cmap = copy.copy(cm.get_cmap('cool'))
        if np.allclose(hist, 0.0) or hist.max() <= 1:
            z_max = 1.0
        else:
            z_max = hist.max()
        # for small z use linear scale, otherwise log scale
        if z_max <= 10.0:
            bounds = np.linspace(start=0.0, stop=z_max, num=255, endpoint=True)
            norm = colors.BoundaryNorm(bounds, cmap.N)
        else:
            bounds = np.linspace(start=1.0, stop=z_max, num=255, endpoint=True)
            norm = colors.LogNorm()

        im = ax.pcolormesh(x_bins, y_bins, hist.T, norm=norm, rasterized=True)
        ax.set_ylim(-0.5, y_max)

        if z_max <= 10.0:
            cb = fig.colorbar(im, ticks=np.linspace(start=0.0, stop=z_max, num=min(
                11, math.ceil(z_max) + 1), endpoint=True), fraction=0.04, pad=0.05)
        else:
            cb = fig.colorbar(im, fraction=0.04, pad=0.05)
        cb.set_label("# of pixels")
        ax.set_title(title + ' for {0} pixel(s)'.format(self.n_enabled_pixels), color=TITLE_COLOR)
        if scan_parameter_name is None:
            ax.set_xlabel('Scan parameter')
        else:
            ax.set_xlabel(scan_parameter_name)
        ax.set_ylabel(ylabel)

        if self.run_config['scan_id'] == 'bump_connection_bias_thr_shift_scan':
            text = 'Failed fits: {0}\nNoisy pixels: {1}'.format(n_failed_scurves, n_noisy_pixels)
        else:
            text = 'Failed fits: {0}\nNoisy pixels: {1}'.format(self.n_failed_scurves, n_noisy_pixels)
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(0.05, 0.88, text, transform=ax.transAxes,
                fontsize=8, verticalalignment='top', bbox=props)

        if electron_axis:
            self._add_electron_axis(fig, ax)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())
            cb.formatter = plt.NullFormatter()
            cb.update_ticks()

        self._save_plots(fig, suffix=suffix)

    def _plot_2d_param_hist(self, hist, scan_parameters, y_max=None, electron_axis=False, scan_parameter_name=None, title='Scan Parameter Histogram', ylabel='', suffix=None):

        if y_max is None:
            y_max = hist.shape[0]

        x_bins = scan_parameters[:]  # np.arange(-0.5, max(scan_parameters) + 1.5)
        y_bins = np.arange(-0.5, y_max + 0.5)

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        fig.patch.set_facecolor('white')

        # log scale
        norm = colors.LogNorm()

        im = ax.pcolormesh(x_bins, y_bins, hist, norm=norm, rasterized=True)
        ax.set_xlim(x_bins[0], x_bins[-1])
        ax.set_ylim(-0.5, y_max)

        cb = fig.colorbar(im, fraction=0.04, pad=0.05)

        cb.set_label("# of pixels")
        ax.set_title(title + " for {0} pixel(s)".format(self.n_enabled_pixels), color=TITLE_COLOR)
        if scan_parameter_name is None:
            ax.set_xlabel('Scan parameter')
        else:
            ax.set_xlabel(scan_parameter_name)
        ax.set_ylabel(ylabel)

        if electron_axis:
            self._add_electron_axis(fig, ax)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())
            cb.formatter = plt.NullFormatter()
            cb.update_ticks()

        self._save_plots(fig, suffix='histogram')

    def _plot_distribution(self, data, plot_range=None, x_axis_title=None, electron_axis=False, use_electron_offset=True, y_axis_title='# of hits', log_y=False, align='edge', title=None, print_failed_fits=False, fit_gauss=True, n_failed_scurves=None, suffix=None):
        if plot_range is None:
            diff = np.amax(data) - np.amin(data)
            if (np.amax(data)) > np.median(data) * 5:
                plot_range = np.arange(np.amin(data), np.median(data) * 2, np.median(data) / 100.)
            else:
                plot_range = np.arange(np.amin(data), np.amax(data) + diff / 100., diff / 100.)
        tick_size = np.diff(plot_range)[0]

        hist, bins = np.histogram(np.ravel(data), bins=plot_range)

        bin_centres = (bins[:-1] + bins[1:]) / 2
        p0 = (np.amax(hist), np.nanmean(bins),
              (max(plot_range) - min(plot_range)) / 3)

        if fit_gauss:
            try:
                coeff, _ = curve_fit(self._gauss, bin_centres, hist, p0=p0)
            except Exception:
                coeff = None
                self.log.warning('Gauss fit failed!')
        else:
            coeff = None

        if coeff is not None:
            points = np.linspace(min(plot_range), max(plot_range), 500)
            gau = self._gauss(points, *coeff)

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.bar(bins[:-1], hist, width=tick_size, align=align)
        if coeff is not None:
            ax.plot(points, gau, "r-", label='Normal distribution')

        if log_y:
            if title is not None:
                title += ' (logscale)'
            ax.set_yscale('log')

        ax.set_xlim(min(plot_range), max(plot_range))
        ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        ax.grid(True)

        if fit_gauss:
            if not self.qualitative:
                mean = np.nanmean(data)
                rms = np.nanstd(data)
                if np.nanmean(data) < 10:
                    if electron_axis:
                        textright = '$\\mu={0:1.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({1[0]:1.0f} \\pm {1[1]:1.0f}) \\; e^-$\n\n$\\sigma={2:1.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({3[0]:1.0f} \\pm {3[1]:1.0f}) \\; e^-$'.format(mean, self._convert_to_e(mean, use_offset=use_electron_offset), rms, self._convert_to_e(rms, use_offset=False))
                    else:
                        textright = '$\\mu={0:1.1f}\\;\\Delta$VCAL\n$\\sigma={1:1.1f}\\;\\Delta$VCAL'.format(mean, rms)
                else:
                    if electron_axis:
                        textright = '$\\mu={0:1.0f}\\;\\Delta$VCAL\n$\\;\\;\\,=({1[0]:1.0f} \\pm {1[1]:1.0f}) \\; e^-$\n\n$\\sigma={2:0.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({3[0]:1.0f} \\pm {3[1]:1.0f}) \\; e^-$'.format(mean, self._convert_to_e(mean, use_offset=use_electron_offset), rms, self._convert_to_e(rms, use_offset=False))
                    else:
                        textright = '$\\mu={0:1.0f}\\;\\Delta$VCAL\n$\\sigma={1:1.0f}\\;\\Delta$VCAL'.format(mean, rms)
                if print_failed_fits:
                    if self.run_config['scan_id'] == 'bump_connection_bias_thr_shift_scan':
                        textright += '\n\nFailed fits: {0}'.format(n_failed_scurves)
                    else:
                        textright += '\n\nFailed fits: {0}'.format(self.n_failed_scurves)

                # Fit results
                if coeff is not None:
                    textright += '\n\nFit results:\n'
                    if coeff[1] < 10:
                        if electron_axis:
                            textright += '$\\mu={0:1.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({1[0]:1.0f} \\pm {1[1]:1.0f}) \\; e^-$\n\n$\\sigma={2:1.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({3[0]:1.0f} \\pm {3[1]:1.0f}) \\; e^-$'.format(abs(coeff[1]), self._convert_to_e(abs(coeff[1]), use_offset=use_electron_offset), abs(coeff[2]), self._convert_to_e(abs(coeff[2]), use_offset=False))
                        else:
                            textright += '$\\mu={0:1.1f}\\;\\Delta$VCAL\n$\\sigma={1:1.1f}\\;\\Delta$VCAL'.format(abs(coeff[1]), abs(coeff[2]))
                    else:
                        if electron_axis:
                            textright += '$\\mu={0:1.0f}\\;\\Delta$VCAL\n$\\;\\;\\,=({1[0]:1.0f} \\pm {1[1]:1.0f}) \\; e^-$\n\n$\\sigma={2:0.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({3[0]:1.0f} \\pm {3[1]:1.0f}) \\; e^-$'.format(abs(coeff[1]), self._convert_to_e(abs(coeff[1]), use_offset=use_electron_offset), abs(coeff[2]), self._convert_to_e(abs(coeff[2]), use_offset=False))
                        else:
                            textright += '$\\mu={0:1.0f}\\;\\Delta$VCAL\n$\\sigma={1:1.0f}\\;\\Delta$VCAL'.format(abs(coeff[1]), abs(coeff[2]))
                    if print_failed_fits:
                        textright += '\n\nFailed fits: {0}'.format(self.n_failed_scurves)

                props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
                ax.text(0.03, 0.95, textright, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

        if electron_axis:
            self._add_electron_axis(fig, ax, use_electron_offset=use_electron_offset)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_stacked_threshold(self, data, tdac_mask, plot_range=None, electron_axis=False, x_axis_title=None, y_axis_title='# of hits',
                                title=None, suffix=None, min_tdac=15, max_tdac=0, range_tdac=16, n_failed_scurves=None):

        if plot_range is None:
            diff = np.amax(data) - np.amin(data)
            if (np.amax(data)) > np.median(data) * 5:
                plot_range = np.arange(
                    np.amin(data), np.median(data) * 5, diff / 100.)
            else:
                plot_range = np.arange(np.amin(data), np.amax(data) + diff / 100., diff / 100.)

        tick_size = plot_range[1] - plot_range[0]

        hist, bins = np.histogram(np.ravel(data), bins=plot_range)

        bin_centres = (bins[:-1] + bins[1:]) / 2
        p0 = (np.amax(hist), np.nanmean(bins), (max(plot_range) - min(plot_range)) / 3)

        try:
            coeff, _ = curve_fit(self._gauss, bin_centres, hist, p0=p0)
        except Exception:
            coeff = None
            self.log.warning('Gauss fit failed!')

        if coeff is not None:
            points = np.linspace(min(plot_range), max(plot_range), 500)
            gau = self._gauss(points, *coeff)

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        cmap = copy.copy(cm.get_cmap('viridis', (range_tdac)))
        # create dicts for tdac data
        data_thres_tdac = {}
        hist_tdac = {}
        tdac_bar = {}

        # select threshold data for different tdac values according to tdac map
        for tdac in range(range_tdac):
            data_thres_tdac[tdac] = data[tdac_mask == tdac - abs(min_tdac)]
            # histogram threshold data for each tdac
            hist_tdac[tdac], _ = np.histogram(np.ravel(data_thres_tdac[tdac]), bins=bins)

            if tdac == 0:
                tdac_bar[tdac] = ax.bar(bins[:-1], hist_tdac[tdac], width=tick_size, align='edge', color=cmap(.9 / range_tdac * tdac), linewidth=0)
            elif tdac == 1:
                tdac_bar[tdac] = ax.bar(bins[:-1], hist_tdac[tdac], bottom=hist_tdac[0], width=tick_size, align='edge', color=cmap(1. / range_tdac * tdac), linewidth=0)
            else:
                tdac_bar[tdac] = ax.bar(bins[:-1], hist_tdac[tdac], bottom=np.sum([hist_tdac[i] for i in range(tdac)], axis=0), width=tick_size, align='edge', color=cmap(1. / range_tdac * tdac), linewidth=0)

        fig.subplots_adjust(right=0.85)
        cax = fig.add_axes([0.89, 0.11, 0.02, 0.645])
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=colors.Normalize(vmin=min_tdac, vmax=max_tdac))
        sm.set_array([])
        cb = fig.colorbar(sm, cax=cax, ticks=np.linspace(start=min_tdac, stop=max_tdac, num=range_tdac, endpoint=True))
        cb.set_label('TDAC')

        if coeff is not None:
            ax.plot(points, gau, "r-", label='Normal distribution')

        ax.set_xlim((min(plot_range), max(plot_range)))
        ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        ax.grid(True)

        if not self.qualitative:
            mean = np.nanmean(data)
            rms = np.nanstd(data)
            if np.nanmean(data) < 10:
                if electron_axis:
                    textright = '$\\mu={0:1.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({1[0]:1.0f} \\pm {1[1]:1.0f}) \\; e^-$\n\n$\\sigma={2:1.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({3[0]:1.0f} \\pm {3[1]:1.0f}) \\; e^-$'.format(mean, self._convert_to_e(mean), rms, self._convert_to_e(rms, use_offset=False))
                else:
                    textright = '$\\mu={0:1.1f}\\;\\Delta$VCAL\n$\\sigma={1:1.1f}\\;\\Delta$VCAL'.format(mean, rms)
            else:
                if electron_axis:
                    textright = '$\\mu={0:1.0f}\\;\\Delta$VCAL\n$\\;\\;\\,=({1[0]:1.0f} \\pm {1[1]:1.0f}) \\; e^-$\n\n$\\sigma={2:0.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({3[0]:1.0f} \\pm {3[1]:1.0f}) \\; e^-$'.format(mean, self._convert_to_e(mean), rms, self._convert_to_e(rms, use_offset=False))
                else:
                    textright = '$\\mu={0:1.0f}\\;\\Delta$VCAL\n$\\sigma={1:1.0f}\\;\\Delta$VCAL'.format(mean, rms)

            # Fit results
            if coeff is not None:
                textright += '\n\nFit results:\n'
                if coeff[1] < 10:
                    if electron_axis:
                        textright += '$\\mu={0:1.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({1[0]:1.0f} \\pm {1[1]:1.0f}) \\; e^-$\n\n$\\sigma={2:1.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({3[0]:1.0f} \\pm {3[1]:1.0f}) \\; e^-$'.format(abs(coeff[1]), self._convert_to_e(abs(coeff[1])), abs(coeff[2]), self._convert_to_e(abs(coeff[2]), use_offset=False))
                    else:
                        textright += '$\\mu={0:1.1f}\\;\\Delta$VCAL\n$\\sigma={1:1.1f}\\;\\Delta$VCAL'.format(abs(coeff[1]), abs(coeff[2]))
                else:
                    if electron_axis:
                        textright += '$\\mu={0:1.0f}\\;\\Delta$VCAL\n$\\;\\;\\,=({1[0]:1.0f} \\pm {1[1]:1.0f}) \\; e^-$\n\n$\\sigma={2:0.1f}\\;\\Delta$VCAL\n$\\;\\;\\,=({3[0]:1.0f} \\pm {3[1]:1.0f}) \\; e^-$'.format(abs(coeff[1]), self._convert_to_e(abs(coeff[1])), abs(coeff[2]), self._convert_to_e(abs(coeff[2]), use_offset=False))
                    else:
                        textright += '$\\mu={0:1.0f}\\;\\Delta$VCAL\n$\\sigma={1:1.0f}\\;\\Delta$VCAL'.format(abs(coeff[1]), abs(coeff[2]))

                if self.run_config['scan_id'] == 'bump_connection_bias_thr_shift_scan':
                    textright += '\n\nFailed fits: {0}'.format(n_failed_scurves)
                else:
                    textright += '\n\nFailed fits: {0}'.format(self.n_failed_scurves)
                props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
                ax.text(0.03, 0.95, textright, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

        if electron_axis:
            self._add_electron_axis(fig, ax)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _fit_LIN(self, x, y):
        ''' Creates linear fit parameters by ignoring saturative effects at the maximum and minimum. '''
        fit_start_index = np.where(y == np.min(y))[-1][-1]
        fit_stop_index = np.where(y == np.max(y))[0][0]
        if fit_stop_index < fit_start_index:
            index_buffer = fit_stop_index
            fit_stop_index = fit_start_index
            fit_start_index = index_buffer
        p0 = (1., 0.)
        coeff, _ = curve_fit(self._lin, x[fit_start_index:fit_stop_index], y[fit_start_index:fit_stop_index], p0=p0)
        m, b = coeff
        return m, b

    def _plot_dac_linearity(self, data, title=None, x_axis_title=None, y_axis_title=None, second_axis=True, suffix='linearity', adc_ref=None, I_ref=None):
        x, y = [], []
        for element in data:
            x = np.append(x, element[0])
            y = np.append(y, element[1])

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.plot(x, y, 'o', color='#07529a', label='Data')

        try:
            m, b = self._fit_LIN(x, y)
            coeff = (m, b)
        except Exception:
            coeff = (0, 0)
            self.log.error('Linear fit failed!')

        m, b = coeff
        ax.plot(x, self._lin(x, *coeff), 'r-', label='Fit')

        if y[0] < y[-3]:
            coords = (max(x), min(y))
            halign = 'right'
        else:
            coords = (0, 0)
            halign = 'left'
        if b >= 0:
            sign = '+'
        else:
            sign = '-'
        if self.scan_config['type'] == 'U':
            if adc_ref:
                ax.text(coords[0], coords[1], 'f(x) = {0:1.2f} x {1} {2:1.2f}\nVREF_ADC_IN = {3:1.2f} V'.format(
                    abs(m), sign, abs(b), adc_ref),
                    bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5), horizontalalignment=halign)
            else:
                ax.text(coords[0], coords[1], 'f(x) = {0:1.2f} x {1} {2:1.2f}'.format(abs(m), sign, abs(b)),
                        bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5), horizontalalignment=halign)
        if self.scan_config['type'] == 'I':
            if I_ref:
                ax.text(coords[0], coords[1], 'f(x) = {0:1.2f} x {1} {2:1.2f}\nIref = {3:1.2f} $\\mu$A'.format(
                    abs(m), sign, abs(b), I_ref),
                    bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5), horizontalalignment=halign)
            else:
                ax.text(coords[0], coords[1], 'f(x) = {0:1.2f} x {1} {2:1.2f}'.format(abs(m), sign, abs(b)),
                        bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5), horizontalalignment=halign)

        ax.legend()
        ax.grid()
        ax.set_title(str(title), color=TITLE_COLOR)

        if second_axis:
            ax2 = ax.twinx()
            ax2.plot(x, y * 0.00019, linestyle='None')
            ax2.set_ylabel('ADC [V]')

        if x_axis_title is None or self.qualitative:
            ax.set_xlabel('Register Setting [LSB]')
        else:
            if '[LSB]' not in x_axis_title:
                x_axis_title += ' [LSB]'
            ax.set_xlabel(x_axis_title)

        if y_axis_title is None:
            ax.set_ylabel('Voltmeter [V]')
        else:
            ax.set_ylabel(y_axis_title)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_dac_non_linearity(self, data, title=None, typ='DNL', x_axis_title=None, y_axis_title=None,
                                suffix='linearity', x_column=0, y_column=1):
        x, y = [], []
        if typ == 'DNL':
            for element in data:
                x = np.append(x, element[0])
                y = np.append(y, element[2])
            title = 'Differential Non-Linearity'
        elif typ == 'INL':
            for element in data:
                x = np.append(x, element[0])
                y = np.append(y, element[3])
            title = 'Integrated Non-Linearity'
        else:
            for element in data:
                x = np.append(x, element[x_column])
                y = np.append(y, element[y_column])
            title = title
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.plot(x, y, '.', color='#07529a', markersize=2,
                linewidth=0.5, label='Data')

        ax.grid()
        ax.set_title(title, color=TITLE_COLOR)

        if x_axis_title is None:
            ax.set_xlabel('Register Setting [LSB]')
        else:
            if '[LSB]' not in x_axis_title:
                x_axis_title += ' [LSB]'
            ax.set_xlabel(x_axis_title)

        if y_axis_title is None:
            ax.set_ylabel('Voltmeter [V]')
        else:
            ax.set_ylabel(y_axis_title)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_one_core_overlay(self, maps, title='Core-Relative_BCID-Plot_DIFF', cb_label='relative BCID avg', suffix='_core'):
        core = np.zeros((8, 8))
        i = 0
        start_column = self.scan_config['start_column']
        stop_column = self.scan_config['stop_column']
        start_row = self.scan_config['start_row']
        stop_row = self.scan_config['stop_row']
        for col in range(start_column, stop_column, 8):
            for row in range(start_row, stop_row, 8):
                core = core + maps[col:col + 8, row:row + 8]
                i = i + 1
        core = core / i
        core = core.T

        fig = Figure()
        FigureCanvas(fig)
        ax = plt.subplot2grid((10, 1), (0, 1), rowspan=19, fig=fig)
        self._add_text(fig)
        fig.subplots_adjust(top=0.82, right=0.85)

        ax.set_title(title)
        bounds = np.linspace(start=np.min(core), stop=np.max(core), num=255, endpoint=True)
        cmap = copy.copy(cm.get_cmap('plasma'))
        cmap.set_bad('w', 1.0)
        norm = colors.BoundaryNorm(bounds, cmap.N)

        im = ax.imshow(core, interpolation='none', aspect='equal', cmap=cmap,
                       norm=norm)  # TODO: use pcolor or pcolormesh

        ax.set_xlabel('Core-Column')
        ax.set_ylabel('Core-Row')

        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.1)
        cb = fig.colorbar(im, cax=cax, ticks=np.linspace(
            start=np.min(core), stop=np.max(core), num=10, endpoint=True))
        cb.set_label(cb_label)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_calibration_line(self, x, y, y_err=0.0, x_err=0.0, point_label=None, suffix=None, xlabel=None, ylabel=None,
                               plot_offset_from_fit=False, plot_transfer_function=False):
        '''
           Plots data points and performs linear fit using iminuit. It is used mostly for energy and charge calibration.
           It can also be used for other linear fits.
        '''

        self.log.info('Performing %s', suffix)
        # Initialization of x_err and y_err is 0.0, because the size of x and y is not known.
        # Here x_err and y_err are defined in case they are not provided
        if not isinstance(y_err, type(y)):
            y_err = np.zeros_like(y)
        if not isinstance(x_err, type(x)):
            x_err = np.zeros_like(x)

        fig = plt.figure()
        # If True add a second small plot at the bottom indicating the offset of each point from the fit
        if plot_offset_from_fit:
            gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
            ax = fig.add_subplot(gs[0])
            ax2 = fig.add_subplot(gs[1])
            plt.subplots_adjust(hspace=0.4)
        else:
            ax = fig.add_subplot(111)

        # Add label on the points
        if point_label is not None:
            for X, Y, Z in zip(x, y, point_label):
                ax.annotate('{}'.format(Z), xy=(X, Y), xytext=(15, -15), ha='right', textcoords='offset points', fontsize=9)

        # If set to True, plot the transfer function that is currently used for comparison
        if plot_transfer_function:
            p1 = [10.37, 180]  # TODO: Take the numbers from the calibration constants in the yaml
            uncert = [0.10, 60]  # TODO: Also here
            current_transfer_function = (suffix + ':  $ax + b$\n$a=(%.2f \\pm %.2f) \\; [\\frac{e^-}{DAC}]$\n$b=(%.2f \\pm %.2f) \\; [e^-]$\n') % (
                p1[0], uncert[0], p1[1], uncert[1])
            ax.plot(x, self._lin(x, *p1), linestyle='-.', color='blue', label=current_transfer_function)

        # In case of asymmetric errors
        if y_err.shape[0] > y.shape[0]:
            y_err_lower = np.array([y_err[i] for i in range(0, y_err.shape[0], 2)])
            y_err_upper = np.array([y_err[i] for i in range(1, y_err.shape[0], 2)])
            ax.errorbar(x, y, xerr=x_err, yerr=(y_err_lower, y_err_upper), fmt='o', color='black', markersize=2)  # plot points
            y_err = (y_err_lower + y_err_upper) / 2  # Needed for chi2 calculation in iminuit due to asymmetric errors
        elif x_err.shape[0] > x.shape[0]:
            x_err_left = np.array([x_err[i] for i in range(0, x_err.shape[0], 2)])
            x_err_right = np.array([x_err[i] for i in range(1, x_err.shape[0], 2)])
            ax.errorbar(x, y, xerr=(x_err_left, x_err_right), yerr=y_err, fmt='o', color='black', markersize=2)  # plot points
        else:  # In case of symmetric errors
            ax.errorbar(x, y, xerr=x_err, yerr=y_err, fmt='o', color='black', markersize=2)  # plot points

        # Perform the fit with iminuit
        coeff, merrors, chi2 = self._lin_fit_iminuit(x=x, y=y, y_err=y_err, p0=(10, 1))
        a, b = coeff
        a_e, b_e = np.array(list([merrors[0][i], merrors[1][i]] for i in range(0, 2)))
        if suffix == 'Energy Calibration':
            label_fit = suffix + ': $ax + b$\n$a= \\; {a:.2f}_{{-{neg_error:.2f}}}^{{+{pos_error:.2f}}}$'.format(a=a, neg_error=a_e[0], pos_error=a_e[1]) + '$ \\; [\\frac{DAC}{keV}]$\n$b= \\; {b:.2f}_{{-{neg_error:.2f}}}^{{+{pos_error:.2f}}}$'.format(b=b, neg_error=b_e[0], pos_error=b_e[1]) + '$ \\; [DAC]$\n$\\frac{\\chi^2}{ndof}={chi2:.2f}$'.format(chi2=chi2)
            ax.set_ylim(y[0] - 200, y[-1] + 200)
        elif suffix == 'Charge Calibration':
            label_fit = suffix + ': $ax + b$\n$a= \\; {a:.2f}_{{-{neg_error:.2f}}}^{{+{pos_error:.2f}}}$'.format(a=a, neg_error=a_e[0], pos_error=a_e[1]) + '$ \\; [\\frac{e^-}{DAC}]$\n$b= \\; {b:.2f}_{{-{neg_error:.2f}}}^{{+{pos_error:.2f}}}$'.format(b=b, neg_error=b_e[0], pos_error=b_e[1]) + '$ \\; [e^-]$\n$\\frac{\\chi^2}{ndof}={chi2:.2f}$'.format(chi2=chi2)
            ax.set_ylim(y[0] - 2000, y[-1] + 2000)
        else:
            label_fit = suffix + ': $ax + b$\n$a= \\; {a:.2f}_{{-{neg_error:.2f}}}^{{+{pos_error:.2f}}}$'.format(a=a, neg_error=a_e[0], pos_error=a_e[1]) + '\n$b= \\; {b:.2f}_{{-{neg_error:.2f}}}^{{+{pos_error:.2f}}}$'.format(b=b, neg_error=b_e[0], pos_error=b_e[1]) + '\n$\\frac{\\chi^2}{ndof}={chi2:.2f}$'.format(chi2=chi2)
        ax.plot(x, self._lin(x, *coeff), linestyle='--', color='red', label=label_fit)
        ax.set_title(suffix)
        ax.legend()
        ax.set_ylabel(ylabel, fontsize=9)
        ax.set_xlabel(xlabel)
        ax.grid(True)
        if plot_offset_from_fit:
            difference = y - self._lin(x, *coeff)
            bar_width = (x[-1] - x[0]) / 100
            ax2.bar(x, difference, align='center', width=bar_width, color='gray')
            ax2.grid(True)
            ax2.set_ylim(- 1.1 * np.amax(np.abs(difference)), 1.1 * np.amax(np.abs(difference)))
            ax2.set_ylabel('Offset from the fit', fontsize=8)
            ax2.set_xlabel(xlabel)
            ax2.plot(plt.xlim(), [0, 0], '-', color='black')  # black line
        return fig

    def _lin_fit_iminuit(self, x, y, y_err, p0):
        ''' Fit Gaussian with optimal error estimation (iminuit)
        '''

        y_uncertainty = y_err

        # Function used in iminuit to minimize chi2
        def minimizeMe(a, b):
            chi2 = np.sum(np.square(y - self._lin(x, a, b)) /
                          np.square(y_uncertainty.astype(np.double)))
            return chi2.astype(np.double) / (x.shape[0] - len(p0) - 1)  # devide by NDF

        limit_a = None
        limit_b = None

        m = iminuit.Minuit(minimizeMe,
                           a=p0[0],
                           limit_a=limit_a,
                           error_a=1,
                           b=p0[1],
                           error_b=3,
                           limit_b=limit_b,
                           errordef=1,
                           print_level=0)
        m.migrad()
        m.minos()

        chi2_ndf = minimizeMe(**m.values)
        return m.np_values(), m.np_merrors(), chi2_ndf

    def _plot_and_fit_spectrum_in_dvcal(self, energies, hit_file, fit_range,
                                        cluster_size_selected, spectrum_key,
                                        bin_size=5,
                                        dvcal_values=[0, 4095],
                                        start_column=0, stop_column=400,
                                        start_row=0, stop_row=192,
                                        binned_spectrum=False):
        ''' Plot spectrum in Delta_VCAL and fit with (double) gaussian peak(s) using iminuit
        '''

        self.log.info('Plotting and fitting spectrum for %s', spectrum_key)

        with tb.open_file(hit_file, mode='r') as out_file_h5:
            clusters = out_file_h5.root.Cluster[:]
            # Check for nan or 0 values and exclude them
            charge_dvcal = clusters['cluster_charge']
            cluster_size = clusters['size'][~np.isnan(charge_dvcal)]
            seed_col = clusters['seed_col'][~np.isnan(charge_dvcal)]
            seed_row = clusters['seed_row'][~np.isnan(charge_dvcal)]
            charge_dvcal = charge_dvcal[~np.isnan(charge_dvcal)]
            cluster_size = cluster_size[charge_dvcal > 0]
            seed_col = seed_col[charge_dvcal > 0]
            seed_row = seed_row[charge_dvcal > 0]
            charge_dvcal = charge_dvcal[charge_dvcal > 0]
            seed_col = seed_col[cluster_size == cluster_size_selected]
            seed_row = seed_row[cluster_size == cluster_size_selected]
            charge_dvcal = charge_dvcal[cluster_size == cluster_size_selected]
            charge_dvcal = charge_dvcal[np.logical_and(np.logical_and(seed_col >= start_column,
                                                                      seed_col < stop_column),
                                                       np.logical_and(seed_row >= start_row,
                                                                      seed_row < stop_row))]
            fig = plt.figure()
            ax = fig.add_subplot(111)
            hist_data, edges = np.histogram(charge_dvcal, bins=np.arange(dvcal_values[0], dvcal_values[-1], bin_size))
            x, y = edges[:-1], hist_data
            y_uncertainty = np.sqrt(y)
            y_uncertainty[y_uncertainty < 1] = 1
            ax.grid(True)
            ax.set_ylabel('Counts')
            ax.set_title(spectrum_key + ' spectrum')
            # bin_size is only 10 for DIFF FE.
            # Due to the square root-like form of the HitOR in DIFF, the TDC resolution is better at the beginning and thus bin_size=5 for Fe and Cu
            if bin_size == 10 and (spectrum_key == 'Cu' or spectrum_key == 'Fe'):
                bin_size = 5
            if binned_spectrum:  # Plot the spectrum using bins
                ax.bar(x, y, width=bin_size)
            else:  # Plot the spectrum using a line
                ax.plot(x, y, color='#0c00fd', label="Data")
                ax.fill_between(x, y, color='#689aff')

            # Function needed to automatically find the initial fitting parameters for the gaussian fit
            def _init_param_for_gaus_fit(energies, fit_range, spectrum_key, x, y, y_uncertainty):

                # Automatically gives initial parameters for (double) gaussian fit based on the selected fitting range by the user
                x_fit = x[np.logical_and(x > fit_range[0], x < fit_range[1])]
                y_fit = y[np.logical_and(x > fit_range[0], x < fit_range[1])]
                y_fit_uncertainty = y_uncertainty[np.logical_and(x > fit_range[0], x < fit_range[1])]
                # This is the sigma used as initial parameter for the fitting. The same sigma is assumed for both Ka and Kb.
                fwhm_arg = np.argmax(y_fit) + np.abs(y[np.logical_and(x >= x_fit[np.argmax(y_fit)], x < fit_range[1])] - (np.max(y_fit) / 2)).argmin()
                if y_fit[fwhm_arg] > np.max(y_fit) / 2:
                    try:
                        x_fwhm = (x_fit[fwhm_arg] + x_fit[fwhm_arg + 1]) / 2
                    except IndexError:
                        x_fwhm = x_fit[fwhm_arg]
                else:
                    x_fwhm = (x_fit[fwhm_arg] + x_fit[fwhm_arg - 1]) / 2
                gaussian_sigma = x_fwhm - x_fit[np.argmax(y_fit)]

                # Initial fitting parameters for double gaussian fit
                if energies.size > 1:
                    # K_b peak position in dvcal
                    x2_fit_dvcal = int(x_fit[np.argmax(y_fit)] * energies[1] / energies[0] + 0.5)
                    x2_fit = (np.abs(x - x2_fit_dvcal)).argmin()
                    # Since energy calibration always has an offset, the lower the energies the more inaccurate are the intial fitting parameters
                    # This seems to be a problem only for Cu (probably for Fe as well) and thus a correction is introduced here
                    if spectrum_key == 'Cu':
                        x2_fit = (np.abs(x - 240)).argmin()
                    x2_fit_check = (np.abs(x_fit - x2_fit_dvcal)).argmin()  # If it is out of range it will give the last element of x_fit
                    # Check if x2_fit_dvcal value is out of fitting range
                    if x2_fit_check == x_fit.size - 1:
                        # If x2_fit_dvcal is out of range then assign the element that is closer to the right boundary of the fitting range to x2_fit
                        x2_fit = (np.abs(x - fit_range[1])).argmin()
                    initial_fit_parameters = (np.max(y_fit), y[x2_fit], x_fit[np.argmax(y_fit)], x[x2_fit], gaussian_sigma, gaussian_sigma)
                else:
                    # Initial fitting parameters for one gaussian peak
                    initial_fit_parameters = (np.max(y_fit), x_fit[np.argmax(y_fit)], gaussian_sigma)

                return initial_fit_parameters, x_fit, y_fit, y_fit_uncertainty

            try:
                p0, x_fit, y_fit, y_fit_uncertainty = _init_param_for_gaus_fit(energies, fit_range, spectrum_key, x, y, y_uncertainty)
                if len(p0) > 3:  # Two gaussians to fit
                    coeff, merrors, chi2_ndf = self._fit_double_gauss_iminuit(x=x_fit, y=y_fit, y_uncertainty=y_fit_uncertainty, p0=p0)
                    a1, a2, m1, m2, sd1, sd2 = coeff
                    a1_e, a2_e, m1_e, m2_e, sd1_e, sd2_e = np.array(list([merrors[0][i], merrors[1][i]] for i in range(0, 6)))
                    peaks = np.array([m1, m2])
                    peaks_stat_uncertainties = np.array([m1_e, m2_e])
                    sigmas = np.array([sd1, sd2])
                    sigmas_stat_uncertainties = np.array([sd1_e, sd2_e])
                    y_fit = self._gauss(x_fit, a1, m1, sd1) + self._gauss(x_fit, a2, m2, sd2)
                    y1_fit = self._gauss(x, a1, m1, sd1)
                    y2_fit = self._gauss(x, a2, m2, sd2)
                    label1 = '$K_{\\alpha}:\\ \\mu={mu:.1f}_{{-{neg_error:.1f}}}^{{+{pos_error:.1f}}} \\; [DAC]$'.format(mu=m1, neg_error=m1_e[0], pos_error=m1_e[1]) + '\n       $\\sigma={sigma:.1f}_{{-{neg_error:.1f}}}^{{+{pos_error:.1f}}} \\; [DAC]$'.format(sigma=sd1, neg_error=sd1_e[0], pos_error=sd1_e[1])
                    label2 = '$K_{\\beta}:\\ \\mu={mu:.1f}_{{-{neg_error:.1f}}}^{{+{pos_error:.1f}}} \\; [DAC]$'.format(mu=m2, neg_error=m2_e[0], pos_error=m2_e[1]) + '\n       $\\sigma={sigma:.1f}_{{-{neg_error:.1f}}}^{{+{pos_error:.1f}}} \\; [DAC]$'.format(sigma=sd2, neg_error=sd2_e[0], pos_error=sd2_e[1])
                    ax.plot(x, y1_fit, '-.', color='black', linewidth=1, label=label1)  # standardization
                    ax.plot(x, y2_fit, '--', color='black', linewidth=1, label=label2)  # standardization
                    ax.set_xlim(m1 - 8 * sd1, m2 + 8 * sd2)
                    ax.plot([m1, m1], [0, np.amax(y1_fit)], '-.', color='black', linewidth=1)  # mean position of First peak
                    ax.plot([m2, m2], [0, np.amax(y2_fit)], '--', color='black', linewidth=1)  # mean position of second peak
                    ax.plot(x_fit, y_fit, '-', label='Fitted', color='black', linewidth=1.5)  # combine the two fits
                    ax.axvline(x_fit[0], linewidth=1, color='red', linestyle='--')
                    ax.axvline(x_fit[-1], linewidth=1, color='red', linestyle='--', label='fitting range')
                    ax.plot([], [], ' ', label='$\\frac{\\chi^2}{ndof}={chi2:.2f}$'.format(chi2=chi2_ndf))
                    ax.legend()
                    ax.set_xlabel('$\\Delta$VCAL [DAC]')
                    return peaks, peaks_stat_uncertainties, sigmas, sigmas_stat_uncertainties, fig
                else:  # Fit with one gaussian
                    coeff, merrors, chi2_ndf = self._fit_single_gauss_iminuit(x=x_fit, y=y_fit, y_uncertainty=y_fit_uncertainty, p0=p0)
                    a, m, sd = coeff
                    a_e, m_e, sd_e = np.array(list([merrors[0][i], merrors[1][i]] for i in range(0, 3)))
                    peak = np.array([m])
                    peak_stat_uncertainty = np.array([m_e])
                    sigma = np.array([sd])
                    sigma_stat_uncertainty = np.array([sd_e])
                    y_fit = self._gauss(x, a, m, sd)
                    label = '$K_{\\;}:\\ \\mu={mu:.1f}_{{-{neg_error:.1f}}}^{{+{pos_error:.1f}}} \\; [DAC]$'.format(mu=m, neg_error=m_e[0], pos_error=m_e[1]) + '\n      $\\sigma={sigma:.1f}_{{-{neg_error:.1f}}}^{{+{pos_error:.1f}}} \\; [DAC]$'.format(sigma=sd, neg_error=sd_e[0], pos_error=sd_e[1])
                    ax.plot(x, y_fit, '-.', color='black', linewidth=1.5, label=label)
                    y_fit = self._gauss(x_fit, a, m, sd)  # This is needed to plot the part of the gaussian that is actually fitted
                    ax.plot(x_fit, y_fit, '-', label='Fitted', color='black', linewidth=1.5)
                    ax.set_xlim((m - 8 * sd, m + 8 * sd))
                    ax.plot([m, m], [0, np.amax(y_fit)], '-.', color='black', linewidth=1.5)
                    ax.axvline(x_fit[0], linewidth=1, color='red', linestyle='--')
                    ax.axvline(x_fit[-1], linewidth=1, color='red', linestyle='--', label='fitting range')
                    ax.plot([], [], ' ', label='$\\frac{\\chi^2}{ndof}={chi2:.2f}$'.format(chi2=chi2_ndf))
                    ax.legend()
                    ax.set_xlabel('$\\Delta$VCAL [DAC]')
                    return peak, peak_stat_uncertainty, sigma, sigma_stat_uncertainty, fig
            except RuntimeError as e:  # failed fit
                self.log.exception(e)

    def _fit_double_gauss_iminuit(self, x, y, y_uncertainty, p0):
        ''' Fit double gaussian with optimal error estimation (iminuit)
        '''

        def minimizeMe_double_gauss(a1, a2, m1, m2, sd1, sd2):
            chi2 = np.sum(np.square(y - self._double_gauss(x, a1, a2, m1, m2, sd1, sd2)) /
                          np.square(y_uncertainty.astype(np.double)))
            return chi2.astype(np.double) / (x.shape[0] - len(p0) - 1)  # devide by NDF

        limit_a1 = (p0[0] * 0.5, p0[0] * 1.5)
        limit_m1 = None
        limit_sd1 = (p0[4] * 0.1, p0[4] * 10)
        limit_a2 = (p0[1] * 0.5, p0[1] * 1.5)
        limit_m2 = None
        limit_sd2 = (p0[5] * 0.1, p0[5] * 10)

        m = iminuit.Minuit(minimizeMe_double_gauss,
                           a1=p0[0],
                           limit_a1=limit_a1,
                           error_a1=1,
                           m1=p0[2],
                           error_m1=3,
                           limit_m1=limit_m1,
                           sd1=p0[4],
                           error_sd1=1,
                           limit_sd1=limit_sd1,
                           a2=p0[1],
                           limit_a2=limit_a2,
                           error_a2=1,
                           m2=p0[3],
                           error_m2=3,
                           limit_m2=limit_m2,
                           sd2=p0[5],
                           error_sd2=1,
                           limit_sd2=limit_sd2,
                           errordef=1,
                           print_level=0)
        m.migrad()
        m.minos()
        chi2_ndf = minimizeMe_double_gauss(**m.values)
        return m.np_values(), m.np_merrors(), chi2_ndf

    def _fit_single_gauss_iminuit(self, x, y, y_uncertainty, p0):
        ''' Fit single gaussian with optimal error estimation (iminuit)
        '''

        def minimizeMe_single_gauss(a, m, sd):
            chi2 = np.sum(np.square(y - self._gauss(x, a, m, sd)) /
                          np.square(y_uncertainty.astype(np.double)))
            return chi2.astype(np.double) / (x.shape[0] - len(p0) - 1)  # devide by NDF

        limit_a = (p0[0] * 0.5, p0[0] * 1.5)
        limit_m = None
        limit_sd = (p0[2] * 0.1, p0[2] * 10)

        m = iminuit.Minuit(minimizeMe_single_gauss,
                           a=p0[0],
                           limit_a=limit_a,
                           error_a=1,
                           m=p0[1],
                           error_m=3,
                           limit_m=limit_m,
                           sd=p0[2],
                           error_sd=1,
                           limit_sd=limit_sd,
                           errordef=1,
                           print_level=0)
        m.migrad()
        m.minos()
        chi2_ndf = minimizeMe_single_gauss(**m.values)
        return m.np_values(), m.np_merrors(), chi2_ndf

    def _plot_2d_timewalk_hist(self, hist, xedges, yedges, plot_range=None, title=None):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        X, Y = np.meshgrid(xedges, yedges)
        im = ax.pcolormesh(X, Y,
                           np.ma.masked_where(hist == 0, hist).T,  # mask zeros
                           norm=colors.LogNorm(vmin=1, vmax=np.max(hist)),  # log scale
                           edgecolors=None)
        a = min(np.nonzero(hist)[1]) - 2
        b = max(np.nonzero(hist)[1]) + 2
        if a < 0:
            a = 0
        if b > hist.shape[1] - 1:
            b = hist.shape[1] - 1
        ax.set_ylim(yedges[a], yedges[b])
        if plot_range is not None:
            ax.set_xlim(plot_range)
        ax.set_ylabel('Timewalk / ns')
        ax.set_xlabel(r'$\Delta$ VCAL')
        ax.set_aspect('auto')

        if title is not None:
            ax.set_title(title, color=TITLE_COLOR)
        cax = inset_axes(ax,
                         width="4%",
                         height="100%",
                         loc='lower left',
                         bbox_to_anchor=(1.03, 0., 1, 1),
                         bbox_transform=ax.transAxes,
                         borderpad=0)
        fig.colorbar(im, ax=ax, cax=cax)
        self._add_text(fig)
        self._add_electron_axis(fig, ax)
        self._save_plots(fig, tight=True)

    def _plot_timewalk_scatter(self, x, y, perc_low, perc_high, in_time_thr=None, label=None, hist=None, xedges=None, yedges=None, plot_range=None, max_timewalk=50, single_pixel=False):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        if hist is None:
            ax.fill_between(x, perc_low, perc_high, alpha=0.3, facecolor=OVERTEXT_COLOR, color=OVERTEXT_COLOR, label='99.7 % band (from TDC trigger distance dist.)')
            ax.grid()
        else:
            # Plot 2D map
            X, Y = np.meshgrid(xedges, yedges)
            im = ax.pcolormesh(X, Y, hist.T,
                               norm=colors.LogNorm(vmin=1, vmax=np.max(hist)),  # log scale
                               edgecolors='none')
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("bottom", size="5%", pad=0.6)
            fig.colorbar(im, cax=cax, orientation='horizontal')
            # indicate percentile levels in case of histogram is plotted
            index = 5
            if single_pixel:
                p_low = np.array((x[index - 1], perc_low[index + 4])) + 2.0
                p_high = np.array((x[index - 1], perc_high[index - 2])) - 2.0
            else:
                p_low = np.array((x[index - 1], perc_low[index + 2]))
                p_high = np.array((x[index - 1], perc_high[index - 2]))
            angle_high = np.arctan((perc_high[index + 2] - perc_high[index - 2]) / (x[index + 2] - x[index - 2])) * 180.0 / np.pi  # in degree
            angle_low = np.arctan((perc_low[index + 2] - perc_low[index - 2]) / (x[index + 2] - x[index - 2])) * 180.0 / np.pi  # in degree
            trans_angle_high = ax.transData.transform_angles(np.array((angle_high,)),
                                                             p_high.reshape((1, 2)))[0]
            trans_angle_low = ax.transData.transform_angles(np.array((angle_low,)),
                                                            p_low.reshape((1, 2)))[0]
            # Plot percentile levels and plot labels
            ax.plot(x, perc_high, color='lightgrey', lw=1.0)
            ax.plot(x, perc_low, color='lightgrey', lw=1.0)
            # Plot text
            ax.text(p_high[0], p_high[1], '99.85 %', fontsize=8, color='lightgrey' if not single_pixel else 'black',
                    rotation=trans_angle_high, rotation_mode='anchor')
            ax.text(p_low[0], p_low[1], '0.15 %', fontsize=8, color='lightgrey' if not single_pixel else 'black',
                    rotation=trans_angle_low, rotation_mode='anchor')

        # Plot mean and in-time threshold
        ax.plot(x, y, label=label, color='indianred')
        if in_time_thr is not None:
            ax.axhline(y=25, linestyle='--', color='grey')  # 25 ns line
            ax.axvline(x=in_time_thr, label='In-Time-Threshold (@ 25 ns): %i e' % self._convert_to_e(in_time_thr)[0], linestyle='--', color='grey')  # in-time-threshold
        # Modify plot
        ax.set_ylabel('Timewalk / ns')
        ax.set_xlabel('$\\Delta$ VCAL')
        if max_timewalk < 60:
            ax.set_yticks(np.arange(-5, max_timewalk + 1, 5))  # One tick every 5 ns
        else:
            ax.set_yticks(np.arange(-5, max_timewalk + 1, 10))  # One tick every 10 ns
        ax.set_ylim(0, max_timewalk)
        if plot_range is not None:
            ax.set_xlim(plot_range)
        ax.legend()
        self._add_text(fig)
        ax2 = self._add_electron_axis(fig, ax)
        for spine in ax2.spines.values():
            spine.set_visible(False)
        self._save_plots(fig)


if __name__ == "__main__":
    ''' Path to analyzed data file: '''
    analyzed_data_file = '/path/to/_interpreted.h5'

    with Plotting(analyzed_data_file=analyzed_data_file,
                  pdf_file=None,            # Path to output pdf file. If None, the name of the analyzed data file is used
                  level='',                 # Level of the results. For example 'preliminary'
                  qualitative=False,        # Create qualitative plots without numbers on axes
                  internal=False,           # Write 'RD53 internal' on every plot
                  save_single_pdf=False,    # Save every plot as a single pdf file in addition to the one output pdf file
                  save_png=False) as p:     # Save every plot as a single png file in addition to the one output pdf file

        ''' Select which plots to create by uncommenting '''
        p.create_standard_plots()           # Creates all the standard plots like a BDAQ53 scan would
        ''' Either create all standard plots using the line above or select each plot independently below '''
#         p.create_parameter_page()           # Creates a title page containing a table with all used parameters and DAC settings
#         p.create_event_status_plot()        # Creates an event status plot showing a histogram of all errors that occurred during analysis
#         p.create_occupancy_map()            # Creates an occupancy hitmap of the chip
#         p.create_fancy_occupancy()          # Creates an occupancy hitmap with logarithmic color scale and projections on both axes
#         p.create_three_way()                # Creates as occupancy hitmap with projection on the pixel number and a bin count histogram
#         p.create_tot_plot()                 # Creates a histogram of the occurred ToT values
#         p.create_tot_map()                  # Create a map of the mean ToT values of each pixel
#         p.create_tot_perpixel_plot()        # Creates ToT calibration plots for n random pixels
#         p.create_tot_curves_plot()          # Creates a plot showing the ToT calibration for all enabled pixels
#         p.create_rel_bcid_plot()            # Creates a histogram of the occurred relative BCIDs
#         p.create_rel_bcid_plot()            # Creates a map of the mean relative BCID for each pixel
#         p.create_tdac_plot()                # Creates a histogram of the TDAC distribution
#         p.create_occupancy_core_map()       # Creates an occupancy map averaged over all froent end cores

        ''' The following plots can only be created from threshold scan or tuning script data '''
#         p.create_scurves_plot()             # Create a 2D-histogram of the S-Curves of all pixels
#         p.create_threshold_plot()           # Creates a threshold distribution plot including a gaussian fit function
#         p.create_stacked_threshold_plot()   # Creates a threshold distribution plot where each bar shows the TDAC distribution in this bin
#         p.create_threshold_map()            # Creates a hitmap showing the threshold value of each pixel
#         p.create_noise_plot()               # Creates a noise distribution plot including a gaussian fit function
#         p.create_noise_map()                # Creates a hitmap showing the noise value of each pixel
#         p.create_chi2_map()                 # Creates a hitmap showing the chi squared values from the S-Curve fit of each pixel

        ''' The following plots can only be created from clusterized data '''
#         p.create_cluster_tot_plot()         # Creates a histogram of the clustered ToT values
#         p.create_cluster_shape_plot()       # Creates a histogram of the occurred cluster shapes
#         p.create_cluster_size_plot()        # Creates a histogram of the occurred cluster sizes

        ''' More plots from special scans'''
#         p.create_dac_linearity_plot()       # Creates a plot showing the linearity of the scanned DAC
