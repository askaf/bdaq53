#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Source scan with RD53A. This is equivalent to external trigger scan with HitOr (BDAQ self-trigger).
    Please note that for a correct clustering the interpreted file from HitOR calibration is needed.
    The above-mentioned interpreted file contains a lookup table which assigns a DeltaVCAL value to a TDC value.
    This lookup table is used in the analysis of the raw data of this scan for clustering with DeltaVCAL.
    The _analyze() method of this scan uses the latest HitOR calibration interpreted file
    found in each chip's directory, if 'hitor_calib_file' is set to 'auto.
    Otherwise please directly assign the path to your HitOR calibration interpreted file to 'hitor_calib_file'.

    Note:
    When you read out more than one chip, the TLU veto length parameter has to be adjusted to higher values.
    Increasing the veto length limits the maximal trigger rate, so use less chips on the same board for high trigger rates.
    Recommended values for the veto length are:
    +––––––––––––––––––––––––––+–––––––––––––––––––+
    | # of chips | veto_length | max. trigger rate |
    |––––––––––––+–––––––––––––+–––––––––––––––––––|
    | 1          | 500         | ~80kHz            |
    | 2          | 1000        | ~40kHz            |
    | 3          | 1500        | ~25kHz            |
    | 4          | 2500        | ~14kHz            |
    +––––––––––––––––––––––––––+–––––––––––––––––––+
    If the analysis indicates errors (such as ext. trigger errors), try to slightly increase the veto length (if the trigger rate allows it).
    Also note that the recommended veto length can be slightly reduced when a higher trigger rate is needed,
    if reducing the veto length does not cause (increased number of) errors.
'''

import tables as tb

from bdaq53.utils import get_latest_h5file
from bdaq53.scans.calibrate_hitor import HitorCalib
from bdaq53.scans.scan_ext_trigger import ExtTriggerScan
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 128,        # Start column for mask
    'stop_column': 264,         # Stop column for mask
    'start_row': 0,             # Start row for mask
    'stop_row': 192,            # Stop row for mask

    # Stop conditions (choose one)
    'scan_timeout': 10,          # Timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,          # Number of maximum received triggers after stopping readout, if False no limit on received trigger
    'min_spec_occupancy': False,    # Minimum hits for each pixel above which the scan will be stopped; only a fraction of all pixels needs to reach this limit (see below)

    # For stop condition 'min_spec_occupancy' only
    'fraction': 0.99,   # Fraction of enabled pixels that need to reach the minimum occupancy (no hits in dead/disconnected pixels!)

    'trigger_latency': 100,     # Latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 65,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips/hitors. Should also be adjusted for longer trigger length.

    'use_tdc': False,           # Enable TDC modules

    'hitor_calib_file': None,

    # Trigger configuration
    'bdaq': {'TLU': {
        'TRIGGER_MODE': 0,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 1     # Selecting trigger input: HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }
}


class SourceScan(ExtTriggerScan):
    scan_id = 'source_scan'

    def _configure(self, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, trigger_length=32, trigger_delay=57, veto_length=500, trigger_latency=100, start_column=0, stop_column=400, start_row=0, stop_row=192, use_tdc=False, **kwargs):
        '''
        Parameters
        ----------
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        trigger_length : int
            Amount of BCIDs to read out on every trigger.
        trigger_delay : int
            Delay the trigger command by this amount in units of 25ns.
        veto_length : int
            Length of TLU veto in units of 25ns.
        trigger_latency : int
            Latency of trigger in units of 25ns.
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        super(SourceScan, self)._configure(scan_timeout=scan_timeout, max_triggers=max_triggers, min_spec_occupancy=min_spec_occupancy, trigger_length=trigger_length, trigger_delay=trigger_delay, veto_length=veto_length, trigger_latency=trigger_latency, start_column=start_column, stop_column=stop_column, start_row=start_row, stop_row=stop_row, use_tdc=use_tdc, **kwargs)

        self.chip.masks['hitbus'][start_column:stop_column, start_row:stop_row] = True
#         self.chip.masks.load_logo_mask(masks=['hitbus'])
        self.chip.masks.update(force=True)

    def _scan(self, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, fraction=0.99, use_tdc=False, **_):
        self.enable_hitor(True)
        super(SourceScan, self)._scan(scan_timeout=scan_timeout, max_triggers=max_triggers, min_spec_occupancy=min_spec_occupancy, fraction=fraction, use_tdc=use_tdc, **_)
        self.enable_hitor(False)

    def _analyze(self):
        hitor_calib_file = self.configuration['scan'].get('hitor_calib_file', None)
        self.configuration['bench']['analysis']['cluster_hits'] = True
        self.configuration['bench']['analysis']['store_hits'] = True
        self.configuration['bench']['analysis']['analyze_tdc'] = self.configuration['scan'].get('use_tdc', False)

        if hitor_calib_file == 'auto':
            hitor_calib_file = get_latest_h5file(directory=self.output_directory, scan_pattern=HitorCalib.scan_id, interpreted=True)

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', hitor_calib_file=hitor_calib_file, **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

            with tb.open_file(a.analyzed_data_file) as in_file:
                clusters = in_file.root.Cluster[:]
                n_clusters = clusters.shape[0]

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return n_clusters


if __name__ == '__main__':
    with SourceScan(scan_config=scan_configuration) as scan:
        try:
            scan.configure()
            scan.scan()
            scan.notify('BDAQ53 source scan has finished!')
            scan.analyze()
        except Exception as e:
            scan.log.error(e)
            scan.notify('ERROR: BDAQ53 source scan has failed!')
