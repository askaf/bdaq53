#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test writes random data into the specified registers,
    then reads back the data and compares read and written data.
'''

import random
import time
import tables as tb

from bdaq53.system.scan_base import ScanBase


scan_configuration = {
    'ignore': ['PIX_PORTAL',
               'GLOBAL_PULSE_ROUTE'
               ]
}


class ValueTable(tb.IsDescription):
    register = tb.StringCol(64, pos=0)
    write = tb.UInt16Col(pos=1)
    read = tb.UInt16Col(pos=2)


class RegisterTest(ScanBase):
    scan_id = 'register_test'

    def _scan(self, ignore=[], **_):
        '''
        Register test main loop

        Parameters
        ----------
        addresses : list
            List of registers to ignore
        '''

        value_table = self.h5_file.create_table(self.h5_file.root, name='values', title='Values', description=ValueTable)

        for reg in self.chip.registers.values():
            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore or reg['address'] in ignore:
                continue

            value = int(random.getrandbits(reg['size']))
            for outer_loop in range(2):

                row = value_table.row
                row['register'] = reg['name']
                row['write'] = value

                self.log.debug('Writing random data %s to register %s at address %s' % (bin(value), reg['name'], reg['address']))
                reg.write(value)
                time.sleep(0.01)
                row['read'] = reg.read()
                row.append()

                invalue = ''
                for n in bin(value)[2:]:
                    if n == '0':
                        invalue += '1'
                    if n == '1':
                        invalue += '0'
                value = int(invalue, 2)

        self.chip.reset()
        self.log.success('Scan finished')

    def _analyze(self):
        result = True
        self.log.info('Comparing data...')
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
            data = in_file.root.values[:]

        for res in data:
            if res[2] != res[1]:
                self.log.error('Register %s read back a wrong value!' % (res[0]))
                result = False
        self.log.success('Successfully tested %i registers.' % (len(data) / 2))
        return result


if __name__ == '__main__':
    with RegisterTest(scan_config=scan_configuration) as test:
        test.start()
