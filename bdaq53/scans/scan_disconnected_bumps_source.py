#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Special source scan that stops at a given occupancy and also detects disconnected bumps while scanning.

    For use with TLU (use RJ45) or RD53A HitOR (BDAQ self-trigger).

    When using HitOr trigger, please note that for a correct clustering the interpreted file from HitOr calibration is needed.
    The above-mentioned interpreted file contains a lookup table which assigns a DeltaVCAL value to a TDC value.
    This lookup table is used in the analysis of the raw data of this scan for clustering with DeltaVCAL.
    The _analyze() method of this scan uses the latest HitOR calibration interpreted file
    found in each chip's directory, if 'hitor_calib_file' is set to 'auto.
    Otherwise please directly assign the path to your HitOR calibration interpreted file to 'hitor_calib_file'.

    Note:
    When you read out more than one chip, the TLU veto length parameter has to be adjusted to higher values.
    Increasing the veto length limits the maximal trigger rate, so use less chips on the same board for high trigger rates.
    Recommended values for the veto length are:
    +––––––––––––––––––––––––––+–––––––––––––––––––+
    | # of chips | veto_length | max. trigger rate |
    |––––––––––––+–––––––––––––+–––––––––––––––––––|
    | 1          | 500         | ~80kHz            |
    | 2          | 1000        | ~40kHz            |
    | 3          | 1500        | ~25kHz            |
    | 4          | 2500        | ~14kHz            |
    +––––––––––––––––––––––––––+–––––––––––––––––––+
    If the analysis indicates errors (such as ext. trigger errors), try to slightly increase the veto length (if the trigger rate allows it).
    Also note that the recommended veto length can be slightly reduced when a higher trigger rate is needed,
    if reducing the veto length does not cause (increased number of) errors.
'''

import time
import numpy as np
import tables as tb
import threading
import scipy
import scipy.stats
import scipy.optimize

from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import online as oa
from bdaq53.analysis import plotting
from bdaq53.scans.calibrate_hitor import HitorCalib
from bdaq53.utils import get_latest_h5file


scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'min_spec_occupancy': 50,   # Minimum hits for each pixel above which the scan will be stopped; identifies dead pixels; if False no limit on number of hits
    'max_noise_occ': 1e-6,      # Target max noise occupancy from NoiseOccScan
    'certainty': 0.2,           # Expected/tolerable number of wrongly classified pixels (connected vs. disconnected)

    'trigger_latency': 100,     # Latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 57,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips/hitors. Should also be adjusted for longer trigger length.

    'use_hitor': True,          # Trigger via HitOr or external trigger?
    'use_tdc': False,           # Analyze TDC words?
    'hitor_calib_file': None,   # Enter specific filename or set to 'auto' to load the latest HitOr calibration result

    # Trigger configuration
    'bdaq': {'TLU': {
        'TRIGGER_MODE': 0,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 1     # HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }
}


class Block():
    '''
    Subdivide pixel matrix into blocks to counteract non-uniformity of source occupancy over the whole matrix
    '''
    def __init__(self, start_col, start_row, width, height, dead_pixs, conn_pixs, pix_false_neg_prob, num_blocks, max_noise_occ):
        self.start_col = start_col
        self.stop_col = start_col + width
        self.start_row = start_row
        self.stop_row = start_row + height
        self.width = width
        self.height = height
        self.dead_pixels = dead_pixs
        self.connected_pixels = conn_pixs
        self.pix_exclusion_prob = pix_false_neg_prob
        self.num_blocks = num_blocks
        self.max_noise_occ = max_noise_occ
        self.mu_underest_err_prob = 1. / (self.num_blocks * 4 * 10000)      # Chips per module times number of modules?...
        self.mu = 0

    def analyze(self, occupancy):
        # Limit occupancy of noisy pixels, estimate max occupancy based on previous mean
        max_true_hits = int(scipy.optimize.root_scalar(f=au.pixel_hits_prob_root_more_n,
                                                       args=(self.mu, self.pix_exclusion_prob),
                                                       method='bisect', bracket=[-0.5, 3 * self.mu], xtol=0.4).root)
        (occupancy[self.start_col:self.stop_col, self.start_row:self.stop_row])[occupancy[self.start_col:self.stop_col, self.start_row:self.stop_row] > max_true_hits] = max_true_hits

        # Save current mean for next call
        self.mu = int(np.mean((occupancy[self.start_col:self.stop_col, self.start_row:self.stop_row])[np.invert(self.dead_pixels[self.start_col:self.stop_col, self.start_row:self.stop_row])]))

        # Ignore dead pixels for mean estimation by setting their occupancy to the mean value
        t_occupancy = occupancy.copy()
        (t_occupancy[self.start_col:self.stop_col, self.start_row:self.stop_row])[self.dead_pixels[self.start_col:self.stop_col, self.start_row:self.stop_row]] = self.mu

        chi2_dist = scipy.stats.chi2(2 * np.sum(t_occupancy[self.start_col:self.stop_col, self.start_row:self.stop_row]))
        min_mu_estimate = 0.5 * chi2_dist.ppf(self.mu_underest_err_prob)
        if np.isnan(min_mu_estimate):
            min_mu_estimate = 0

        min_mu_estimate /= (self.width * self.height)

        print('min_mu_estimate', min_mu_estimate)

        min_true_hits = 0

        try:
            if int(min_mu_estimate) > 0:
                min_true_hits = int(scipy.optimize.root_scalar(f=au.pixel_hits_prob_root_less_n,
                                                               args=(int(min_mu_estimate), self.pix_exclusion_prob),
                                                               method='bisect', bracket=[-0.5, 3 * int(min_mu_estimate)], xtol=0.4).root)
        except Exception:
            return

        if min_true_hits == 0:
            return

        print('min_true_hits', min_true_hits)

        for col in range(self.start_col, self.stop_col):
            for row in range(self.start_row, self.stop_row):
                if occupancy[col, row] < int(min_true_hits):
                    self.dead_pixels[col, row] = True
                else:
                    self.dead_pixels[col, row] = False


class BumpConSourceScan(ScanBase):
    scan_id = 'bump_connection_source_scan'

    is_parallel_scan = True     # Parallel readout of ExtTrigger-type scans

    stop_scan = threading.Event()

    def _configure(self, min_spec_occupancy=50, max_noise_occ=1e-6, certainty=1.0, trigger_length=32, trigger_delay=57, veto_length=500, use_hitor=True, use_tdc=False, trigger_latency=100, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        trigger_length : int
            Amount of BCIDs to read out on every trigger.
        trigger_delay : int
            Delay the trigger command by this amount in units of 25ns.
        veto_length : int
            Length of TLU veto in units of 25ns.
        trigger_latency : int
            Latency of trigger in units of 25ns.
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''
        self.bdaq.configure_tlu_module()                                                                    # Configure tlu module using trigger configuration
        self.bdaq.configure_trigger_cmd_pulse(trigger_length=trigger_length, trigger_delay=trigger_delay)   # Configure trigger command pulse
        self.bdaq.configure_tlu_veto_pulse(veto_length=veto_length)                                         # Configure veto pulse

        if use_tdc:
            # Configure all four TDC modules
            self.bdaq.configure_tdc_modules()

        self.chip.registers['LATENCY_CONFIG'].write(trigger_latency)                                        # Set trigger latency
        self.chip.registers['GP_LVDS_ROUTE'].write(0)
        self.data.n_trigger = 0                                                                             # Count trigger words in rawdata stream

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        if use_hitor:
            self.chip.masks['hitbus'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][:] = False
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update(force=True)

        if any(x in range(0, 128) for x in range(start_column, stop_column)):
            self.log.info('SYNC enabled: Enabling auto-zeroing')
            self.chip.az_setup(delay=80, repeat=0, width=6, synch=6)
            self.chip.az_start()

        self.data.hist_occ = oa.OccupancyHistogramming()
        self.data.occupancy = np.zeros(shape=(400, 192))
        self.data.dead_pixels = np.full(shape=(400, 192), fill_value=False)
        self.data.connected_pixels = np.full(shape=(400, 192), fill_value=False)
        self.data.enabled_pixels = np.logical_and(self.chip.masks['enable'], self.chip.masks.disable_mask)

        # Subdivide pixel matrix into blocks in order to analyze disconnected bumps with locally averaged occupancy; load noise occupancy map
        cols = 400
        rows = 192
        target_block_width = 64
        target_block_height = 64
        c_blocks = int(np.floor(cols / target_block_width))
        r_blocks = int(np.floor(rows / target_block_height))
        num_blocks = c_blocks * r_blocks

        self.data.quantile_alpha = certainty / 2. / cols / rows

        self.data.blocks = []
        for i in range(c_blocks):
            if i < c_blocks - 1:
                width = target_block_width
            else:
                width = cols - (c_blocks - 1) * target_block_width

            for j in range(r_blocks):
                if j < r_blocks - 1:
                    height = target_block_height
                else:
                    height = rows - (r_blocks - 1) * target_block_height

                self.data.blocks.append(Block(start_col=(i * target_block_width), start_row=(j * target_block_height), width=width, height=height,
                                              dead_pixs=self.data.dead_pixels, conn_pixs=self.data.connected_pixels, pix_false_neg_prob=self.data.quantile_alpha,
                                              num_blocks=num_blocks, max_noise_occ=max_noise_occ))

    def _scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, min_spec_occupancy=50, use_hitor=True, use_tdc=False, **_):
        '''
        ExtTriggerScan scan main loop

        Parameters
        ----------
        scan_timeout : int / False
            Number of seconds to records triggers. Set to False for no limit.
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        '''
        self.data.pbar = tqdm(total=100, unit=' % Hits')

        if use_hitor:
            self.enable_hitor(True)

        if use_tdc:
            self.bdaq.enable_tdc_modules()

        with self.readout():
            self.stop_scan.clear()

            self.bdaq.enable_ext_trigger()  # Enable external trigger
            self.bdaq.enable_tlu_module()   # Enable TLU module

            # Scan loop
            while not (self.stop_scan.is_set()):
                try:
                    # Read tlu error counters
                    trig_low_timeout_errors, trig_accept_errors = self.bdaq.get_tlu_erros()
                    if trig_low_timeout_errors != 0 or trig_accept_errors != 0:
                        self.log.warning('TLU errors detected! TRIGGER_LOW_TIMEOUT_ERROR_COUNTER: {0}, TLU_TRIGGER_ACCEPT_ERROR_COUNTER: {1}'.format(trig_low_timeout_errors, trig_accept_errors))
                    time.sleep(1)

                    self.data.occupancy += self.data.hist_occ.get()

                    # Identify dead pixels
                    self.find_dead_pixels()

                    # Update progress bar
                    try:
                        clipped_occ = self.data.occupancy.copy()
                        clipped_occ[clipped_occ > min_spec_occupancy] = min_spec_occupancy
                        clipped_num_hits = np.sum(clipped_occ)

                        num_enabled_and_alive_pixels = np.count_nonzero(np.logical_and(self.data.enabled_pixels, np.invert(self.data.dead_pixels)))

                        num_required = min_spec_occupancy * num_enabled_and_alive_pixels

                        self.data.pbar.n = int(100. * clipped_num_hits / num_required)
                        self.data.pbar.refresh()
                    except ValueError:
                        pass

                    self.log.debug('Number of disconnected pixels: {0}'.format(np.sum(np.logical_and(self.data.dead_pixels, self.data.enabled_pixels))))

                    # Stop scan if reached minimum hits per pixel
                    if np.all(np.logical_or(np.logical_or(self.data.occupancy >= min_spec_occupancy, self.data.dead_pixels), np.invert(self.data.enabled_pixels))):
                        self.stop_scan.set()
                        self.data.pbar.close()
                        self.log.info('Reached required minimal number of hits per pixel ({0})'.format(min_spec_occupancy))
                        self.log.info('Found {0} disconnected pixels'.format(np.count_nonzero(np.logical_and(self.data.dead_pixels, self.data.enabled_pixels))))

                except KeyboardInterrupt:  # React on keyboard interupt
                    self.stop_scan.set()
                    self.data.pbar.close()
                    self.log.info('Scan was stopped due to keyboard interrupt')

            self.data.pbar.close()

        self.chip.az_stop()

        self.bdaq.disable_tlu_module()      # disable TLU module
        self.bdaq.disable_ext_trigger()     # disable external trigger

        if use_tdc:
            self.bdaq.disable_tdc_modules()

        if use_hitor:
            self.enable_hitor(False)

        self.data.hist_occ.close()

        self.n_trigger = self.data.n_trigger    # Print number of triggers in ScanBase
        self.chip.registers.reset_all()

        # Create array for bump connectivity plots
        disconnected_pixels_map = np.logical_and(self.data.dead_pixels, self.data.enabled_pixels)
        hist_bump_bonds_connection = np.full(shape=(400, 192), fill_value=1)
        hist_bump_bonds_connection[disconnected_pixels_map] = 0

        self.h5_file.create_carray(self.h5_file.root, name='BumpConnectivityMap', title='Bump Connectivity', obj=hist_bump_bonds_connection,
                                   filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        self.log.success('Scan finished')

    def _analyze(self, create_pdf=True):
        hitor_trigger = self.configuration['scan'].get('use_hitor', False)
        analyze_tdc = self.configuration['scan'].get('use_tdc', False)
        hitor_calib_file = self.configuration['scan'].get('hitor_calib_file', None)
        if hitor_trigger and hitor_calib_file == 'auto':
            hitor_calib_file = get_latest_h5file(directory=self.output_directory, scan_pattern=HitorCalib.scan_id, interpreted=True)

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', hitor_calib_file=hitor_calib_file, store_hits=True, cluster_hits=True, analyze_tdc=analyze_tdc) as a:
            a.analyze_data()

        with tb.open_file(self.output_filename + '.h5', 'r') as raw_file:
            bump_bonds_connection_map = raw_file.root.BumpConnectivityMap[:]

        with tb.open_file(self.output_filename + '_interpreted.h5', 'r+') as in_file:
            in_file.create_carray(in_file.root, name='BumpConnectivityMap', title='Bump Connectivity', obj=bump_bonds_connection_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            clusters = in_file.root.Cluster[:]
            n_clusters = clusters.shape[0]

            # Get scan parameters
            run_config = au.ConfigDict(in_file.root.configuration.run_config[:])

            start_column = run_config['start_column']
            stop_column = run_config['stop_column']
            start_row = run_config['start_row']
            stop_row = run_config['stop_row']

            enable_mask = np.zeros(shape=(400, 192), dtype=bool)
            enable_mask[start_column:stop_column, start_row:stop_row] = True
            disable_mask = in_file.root.masks.disable[:]

            enabled_pixels = np.logical_and(enable_mask, disable_mask)

        # TODO: add bump disconnection ToT-hist analysis here

        conn_state_count = dict(zip(*np.unique(bump_bonds_connection_map[enabled_pixels], return_counts=True)))
        num_connected_bumps = conn_state_count.get(1, 0)
        num_poorly_connected_bumps = conn_state_count.get(0.5, 0)
        num_disconnected_bumps = conn_state_count.get(0, 0)
        num_unknown_state_bumps = conn_state_count.get(-12, 0)

        self.log.info('Found {0} connected pixels'.format(num_connected_bumps))
        self.log.info('Found {0} disconnected pixels'.format(num_disconnected_bumps))
#        if num_poorly_connected_bumps > 0:
#            self.log.info('Found {0} poorly connected pixels'.format(num_poorly_connected_bumps))
#        if num_unknown_state_bumps > 0:
#            self.log.info('Found {0} pixels of unknown connection state'.format(num_unknown_state_bumps))
        self.log.info('Ignored {0} non-enabled pixels'.format(np.count_nonzero(np.invert(enabled_pixels))))

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

                p._plot_bump_connectivity_map(bump_bonds_connection_map)

                p._plot_bump_connectivity_hist(connected_bumps=num_connected_bumps,
                                               num_poorly_connected_bumps=num_poorly_connected_bumps,
                                               disconnected_bumps=num_disconnected_bumps,
                                               unknown_state_bumps=num_unknown_state_bumps)

        return n_clusters

    def find_dead_pixels(self):
        for block in self.data.blocks:
            block.analyze(self.data.occupancy)

    def handle_data(self, data_tuple):
        ''' Check recorded trigger number '''
        super(BumpConSourceScan, self).handle_data(data_tuple)

        raw_data = data_tuple[0]

        self.data.hist_occ.add(raw_data)

        sel = np.bitwise_and(raw_data, au.TRIGGER_HEADER) > 0
        trigger_words = raw_data[sel]
        trigger_number = np.bitwise_and(trigger_words, au.TRG_MASK)
        trigger_inc = np.diff(trigger_number)
        trigger_issues = np.logical_and(trigger_inc != 1, trigger_inc != -2**31)

        self.data.n_trigger += trigger_number.shape[0]

        if np.any(trigger_issues):
            self.log.warning('Trigger numbers not strictly increasing')
            if np.count_nonzero(trigger_issues > 0) <= 10:
                self.log.warning('Trigger delta(s): {0}'.format(str(trigger_inc[trigger_issues > 0])))
            else:
                self.log.warning('More than 10 trigger numbers not strictly increasing in this readout!')


if __name__ == '__main__':
    with BumpConSourceScan(scan_config=scan_configuration) as scan:
        try:
            scan.configure()
            scan.scan()
            scan.notify('BDAQ53 external trigger scan has finished!')
            scan.analyze()
        except Exception as e:
            scan.log.error(e)
            scan.notify('ERROR: BDAQ53 external trigger scan has failed!')
