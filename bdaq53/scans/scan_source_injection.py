#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Source scan using analog injection (no trigger w/o trigger command) instead of a real source.
    Works only for LIN and DIFF. Autozeroing for SYNC is not implemented.

    Note: This scan is only for testing/debugging purposes of the TLU state machine.
'''

from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject, get_scan_loop_mask_steps
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 128,        # Start column for mask
    'stop_column': 400,         # Stop column for mask
    'start_row': 0,             # Start row for mask
    'stop_row': 192,            # Stop row for mask

    'max_triggers': False,      # Number of maximum received triggers after stopping readout, if False no limit on received trigger
    'n_injections': 2,

    'trigger_latency': 42,     # Latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 4,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips/hitors. Should also be adjusted for longer trigger length.

    # Trigger configuration
    'bdaq': {'TLU': {
        'TRIGGER_MODE': 0,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 1     # Selecting trigger input: HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }
}


class SourceScanInj(ScanBase):
    scan_id = 'source_scan_injection'

    def _configure(self, max_triggers=False, trigger_length=32, trigger_delay=57, veto_length=500, trigger_latency=100, start_column=0, stop_column=400, start_row=0, stop_row=192, use_tdc=False, **kwargs):
        '''
        Parameters
        ----------
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        trigger_length : int
            Amount of BCIDs to read out on every trigger.
        trigger_delay : int
            Delay the trigger command by this amount in units of 25ns.
        veto_length : int
            Length of TLU veto in units of 25ns.
        trigger_latency : int
            Latency of trigger in units of 25ns.
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        self.bdaq.configure_tlu_module(max_triggers=max_triggers)                                           # Configure tlu module using trigger configuration
        self.bdaq.configure_trigger_cmd_pulse(trigger_length=trigger_length, trigger_delay=trigger_delay)   # Configure trigger command pulse
        self.bdaq.configure_tlu_veto_pulse(veto_length=veto_length)                                         # Configure veto pulse

        self.chip.registers['LATENCY_CONFIG'].write(trigger_latency)

        self.chip.masks['hitbus'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update(force=True)
        self.chip.setup_analog_injection(vcal_high=2500, vcal_med=500)

    def _scan(self, n_injections=1, **kwargs):
        print(n_injections, kwargs)
        self.enable_hitor(True)
        self.bdaq.enable_ext_trigger()  # enable external trigger
        self.bdaq.enable_tlu_module()   # enable TLU module

        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self), unit=' Mask steps')
        with self.readout():
            shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, send_trigger=False)

        pbar.close()
        self.log.success('Scan finished')

        self.bdaq.disable_tlu_module()      # disable TLU module
        self.bdaq.disable_ext_trigger()     # disable external trigger
        self.enable_hitor(False)

    def _analyze(self):
        self.configuration['bench']['analysis']['cluster_hits'] = True
        self.configuration['bench']['analysis']['store_hits'] = True
        self.configuration['bench']['analysis']['analyze_tdc'] = self.configuration['scan'].get('use_tdc', False)

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


if __name__ == '__main__':
    with SourceScanInj(scan_config=scan_configuration) as scan:
        try:
            scan.configure()
            scan.scan()
            scan.notify('BDAQ53 source scan has finished!')
            scan.analyze()
        except Exception as e:
            scan.log.error(e)
            scan.notify('ERROR: BDAQ53 source scan has failed!')
