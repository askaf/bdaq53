#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Tune global and local threshold and scan for disconnected bump bonds using the threshold/noise shift method.
    Also refer to scan_disconnected_bumps_threshold.py.

    Note:
    The tuning part of this meta scan should be optimally done without high voltage applied (HV = 0V).
'''

from bdaq53.scans.tune_global_threshold import GDACTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.scan_disconnected_bumps_threshold import BumpConnThrShScan


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    # Start threshold scan at injection setting where a fraction of min_response of the selected
    # pixels see the fraction of hits min_hits
    'min_response': 0.001,
    'min_hits': 0.05,
    # Stop threshold scan at injection setting where a fraction of max_response of the selected
    # pixels see the fraction of hits max_hits
    'max_response': 0.99,
    'max_hits': 0.99,

    'n_injections': 100,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,         # Start value of injection
    'VCAL_HIGH_stop': 2000,         # Maximum injection, can be None
    'VCAL_HIGH_step_fine': 25,      # Step size during threshold scan
    'VCAL_HIGH_step_coarse': 50,    # Step when seaching start

    'bias_voltage_forward': +1,     # Be careful!
    'bias_voltage_reverse': -40,
    'bias_current_limit': 10e-6
}

tuning_configuration = {
    'maskfile': None,

    'VCAL_MED': 500,
    'VCAL_HIGH': 1150,

    'chip': {'registers': {'IBIASP1_SYNC': 38,
                           'IBIASP2_SYNC': 10,
                           'IBIAS_SF_SYNC': 45,
                           'PA_IN_BIAS_LIN': 30,
                           'PRMP_DIFF': 10
                           }
             }
}


if __name__ == '__main__':
    tuning_configuration['start_column'] = scan_configuration['start_column']
    tuning_configuration['stop_column'] = scan_configuration['stop_column']
    tuning_configuration['start_row'] = scan_configuration['start_row']
    tuning_configuration['stop_row'] = scan_configuration['stop_row']

    with GDACTuning(scan_config=tuning_configuration) as global_tuning:
        global_tuning.start()

    with TDACTuning(scan_config=tuning_configuration) as local_tuning:
        local_tuning.start()

    with BumpConnThrShScan(scan_config=scan_configuration) as disconn_bumps_scan:
        disconn_bumps_scan.start()
