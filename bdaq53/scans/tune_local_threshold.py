#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Injection tuning:
    Iteratively inject target charge and evaluate if more or less than 50% of expected hits are seen in any pixel
'''

from tqdm import tqdm
import numpy as np

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import online as oa

scan_configuration = {
    'start_column': 128,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    'maskfile': None,

    'n_injections': 100,

    # Target threshold
    'VCAL_MED': 500,
    'VCAL_HIGH': 580
}


class TDACTuning(ScanBase):
    scan_id = 'local_threshold_tuning'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, VCAL_MED=1000, VCAL_HIGH=4000, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''

        # Ignore SYNC flavor
        start_column = np.clip(start_column, 128, None)

        self.data.flavors = []
        if start_column < 264:
            self.data.flavors.append('LIN')
        if stop_column > 264:
            self.data.flavors.append('DIFF')

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

        self.data.hist_occ = oa.OccupancyHistogramming()

    def _scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, n_injections=100, maskfile=None, **_):
        '''
        Global threshold tuning main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        '''

        # Ignore SYNC flavor
        start_column = np.clip(start_column, 128, None)

        target_occ = n_injections / 2

        self.data.tdac_map = np.zeros((400, 192), dtype=int)
        best_results_map = np.zeros((400, 192, 2), dtype=float)
        retune = False
        if maskfile is not None:
            self.data.tdac_map[:] = self.chip.masks['tdac'][:]
            if 'LIN' in self.data.flavors and not np.all(self.data.tdac_map[max(128, start_column):min(264, stop_column), start_row:stop_row] == 7):
                retune = True
                steps = [1, 1, 1, 1]
            elif 'DIFF' in self.data.flavors and not np.all(self.data.tdac_map[max(264, start_column):min(400, stop_column), start_row:stop_row] == 0):
                retune = True
                steps = [1, 1, 1, 1]

        # Define stepsizes and startvalues for TDAC
        # Binary search will not converge if all TDACs are centered, so set
        #   half to 7 and half to 8 for LIN
        #   leave half at 0 and divide the other half between +1 and -1 for DIFF
        if not retune and 'LIN' in self.data.flavors:
            steps = [4, 2, 1, 1]
            self.data.tdac_map[max(128, start_column):min(264, stop_column), start_row:stop_row] = 7
            self.data.tdac_map[max(128, start_column):min(264, stop_column), start_row:stop_row:2] = 8
        if not retune and 'DIFF' in self.data.flavors:
            steps = [8, 4, 2, 1, 1, 1]
            self.data.tdac_map[max(264, start_column):min(400, stop_column), start_row:stop_row:2] = 1
            self.data.tdac_map[max(264, start_column):min(400, stop_column), start_row:stop_row:4] = -1

        self.log.info('Searching optimal local threshold settings for {0}.'.format(', '.join(self.data.flavors)))
        pbar = tqdm(total=self.chip.masks.get_mask_steps() * len(steps) * 2, unit=' Mask steps')
        for scan_param, step in enumerate(steps):
            self.chip.revive()
            self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = self.data.tdac_map[start_column:stop_column, start_row:stop_row]
            self.chip.masks.update()
            with self.readout(scan_param_id=scan_param, callback=self.analyze_data_online):
                for fe, _ in self.chip.masks.shift(masks=['enable', 'injection']):
                    if not fe == 'skipped':
                        self.chip.inject_analog_single(repetitions=n_injections, send_ecr=False)
                    pbar.update(1)

            occupancy = self.data.hist_occ.get()
            self.chip.revive()

            # Scan stuck pixels
            with self.readout(scan_param_id=scan_param, callback=self.analyze_data_online_no_save):
                for fe, _ in self.chip.masks.shift(masks=['enable']):
                    if not fe == 'skipped':
                        self.chip.toggle_output_select(repetitions=10, send_ecr=False)
                    pbar.update(1)
            stuck = self.data.hist_occ.get()

            # FIXME: Maybe this can be done with numpy magic?
            for col in range(start_column, stop_column):
                if col in range(128, 264):
                    flavor = 'LIN'
                else:
                    flavor = 'DIFF'

                for row in range(start_row, stop_row):
                    diff = abs(occupancy[col, row] - target_occ)
                    if diff <= best_results_map[col, row, 1] or best_results_map[col, row, 1] == 0.:
                        best_results_map[col, row, 0] = self.data.tdac_map[col, row]
                        best_results_map[col, row, 1] = diff

                    if occupancy[col, row] > target_occ + round(target_occ * 0.02):
                        if flavor == 'LIN':
                            self.data.tdac_map[col, row] -= step
                        elif flavor == 'DIFF':
                            self.data.tdac_map[col, row] += step
                    elif occupancy[col, row] < target_occ - round(target_occ * 0.02):
                        if flavor == 'LIN':
                            if stuck[col, row]:
                                self.data.tdac_map[col, row] -= step
                            else:
                                self.data.tdac_map[col, row] += step
                        elif flavor == 'DIFF':
                            if stuck[col, row]:
                                self.data.tdac_map[col, row] += step
                            else:
                                self.data.tdac_map[col, row] -= step

            # Make sure no invalid TDACs are used
            self.data.tdac_map[128:264, :] = np.clip(self.data.tdac_map[128:264, :], 0, 15)
            self.data.tdac_map[264:400, :] = np.clip(self.data.tdac_map[264:400, :], -15, 15)

        # Finally use TDAC value which yielded the closest to target occupancy
        self.data.tdac_map[:, :] = best_results_map[:, :, 0]

        pbar.close()
        self.data.hist_occ.close()   # stop analysis process
        self.log.success('Scan finished')

        if len(self.data.flavors) == 1:
            enable_mask = self.chip.masks['enable'][start_column:stop_column, start_row:stop_row]
            tdac_mask = self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row]
            mean_tdac = np.mean(tdac_mask[enable_mask])
            self.log.success('Mean TDAC is {0:1.2f}.'.format(mean_tdac))
        else:
            enable_mask_lin = self.chip.masks['enable'][max(128, start_column):min(264, stop_column), start_row:stop_row]
            tdac_mask_lin = self.chip.masks['tdac'][max(128, start_column):min(264, stop_column), start_row:stop_row]
            mean_tdac_lin = np.mean(tdac_mask_lin[enable_mask_lin])
            enable_mask_diff = self.chip.masks['enable'][max(264, start_column):min(400, stop_column), start_row:stop_row]
            tdac_mask_diff = self.chip.masks['tdac'][max(264, start_column):min(400, stop_column), start_row:stop_row]
            mean_tdac_diff = np.mean(tdac_mask_diff[enable_mask_diff])
            self.log.success('Mean TDAC is {0:1.2f} for LIN and {1:1.2f} for DIFF.'.format(mean_tdac_lin, mean_tdac_diff))
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = self.data.tdac_map[start_column:stop_column, start_row:stop_row]

    def analyze_data_online(self, data_tuple, receiver=None):
        raw_data = data_tuple[0]
        self.data.hist_occ.add(raw_data)
        super(TDACTuning, self).handle_data(data_tuple, receiver)

    def analyze_data_online_no_save(self, data_tuple, receiver=None):
        raw_data = data_tuple[0]
        self.data.hist_occ.add(raw_data)

    def _analyze(self):
        pass


if __name__ == '__main__':
    with TDACTuning(scan_config=scan_configuration) as tuning:
        tuning.start()
