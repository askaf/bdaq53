#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This scan identifies pixels with disconnected bump bonds by doing two threshold scans,
    one with reverse biased sensor and one with slightly forward biased sensor.
    A bump is assumed to be connected if the threshold and or the noise shift by a certain amount.
    The used cuts are hard-coded. They should be outside of the shift distributions of a bare chip.
    For cross checking, the shift distributions are plotted in the output PDF file.

    Note:
    To obtain correct results over the whole chip, all flavors have to be tuned to 650 DVCAL prior to running this scan,
    optimally without hight voltage (HV = 0V). The tuning must be done using the same custom register settings as used in this scan:
    'chip': {'registers': {'IBIASP1_SYNC': 38,
                           'IBIASP2_SYNC': 10,
                           'IBIAS_SF_SYNC': 45,
                           'PA_IN_BIAS_LIN': 30,
                           'PRMP_DIFF': 10
                           }
             }
'''

import numpy as np
import tables as tb

from tqdm import tqdm

from bdaq53.chips import rd53a
from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import online as oa
from bdaq53.analysis import plotting

scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    # Start threshold scan at injection setting where a fraction of min_response of the selected
    # pixels see the fraction of hits min_hits
    'min_response': 0.001,
    'min_hits': 0.05,
    # Stop threshold scan at injection setting where a fraction of max_response of the selected
    # pixels see the fraction of hits max_hits
    'max_response': 0.99,
    'max_hits': 0.99,

    'n_injections': 100,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,         # Start value of injection
    'VCAL_HIGH_stop': 2000,         # Maximum injection, can be None
    'VCAL_HIGH_step_fine': 25,      # Step size during threshold scan
    'VCAL_HIGH_step_coarse': 50,    # Step when seaching start

    'bias_voltage_forward': +1,     # Be careful!
    'bias_voltage_reverse': -40,
    'bias_current_limit': 10e-6
}


class BumpConnThrShScan(ScanBase):
    scan_id = 'bump_connection_bias_thr_shift_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''
        self.data.module_name = self.module_settings['name']

        if not self.periphery.enabled:
            raise Exception('Periphery module needs to be enabled!')
        if not (self.data.module_name in list(self.periphery.module_devices.keys()) and 'HV' in self.periphery.module_devices[self.data.module_name].keys()):
            raise Exception('No sensor bias device defined for {}!'.format(self.data.module_name))

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

        # Need to use modified register settings. Do not change.
        self.chip.registers['IBIASP1_SYNC'].write(38)
        self.chip.registers['IBIASP2_SYNC'].write(10)
        self.chip.registers['IBIAS_SF_SYNC'].write(45)
        self.chip.registers['PA_IN_BIAS_LIN'].write(30)
        self.chip.registers['PRMP_DIFF'].write(10)

        self.data.hist_occ = oa.OccupancyHistogramming()

    def _scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192,
              n_injections=200, VCAL_MED=500, VCAL_HIGH_start=500, VCAL_HIGH_stop=2000, VCAL_HIGH_step_fine=10, VCAL_HIGH_step_coarse=50,
              min_response=0.01, max_response=0.99, min_hits=0.2, max_hits=1.,
              bias_voltage_forward=+0.0, bias_voltage_reverse=-20, bias_current_limit=1e-6, **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.

        min_response: float 0.01,
            Minimal needed fraction of pixels seeing 'min_hits' before starting scan
        min_hits: float 0.1,
            Minimal fraction of hits needed to be seen by 'min_response' fraction of pixels, before starting scan
        max_response: float 0.99,
            Scan stops at this fraction of pixels seeing 'max_hits'
        max_hits: float 1.0,
            Minimal fraction of hits needed to be seen by 'max_response' fraction of pixels before stopping scan

        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step_fine : int
            VCAL_HIGH interval.
        VCAL_HIGH_step_coarse : int
            VCAL_HIGH interval for searching injection start value.

        bias_voltage_reverse: int
            Reverse bias voltage
        bias_voltage_forward: int
            Forward bias voltage
        bias_current_limit: float
            Current limit for sensor bias device
        '''

        biases = [bias_voltage_reverse, bias_voltage_forward]

        # Remember scan param id for each bias setting to restore the two different s-curves
        scan_param_ids_list = []

        # Calculated max pixels that can respond
        sel_pix = self.chip.masks['enable'][start_column:stop_column, start_row:stop_row]
        n_sel_pix = np.count_nonzero(sel_pix)

        self.periphery.power_off_HV(self.data.module_name)
        try:
            scan_param_id = -1                      # Do not store data before S-curve sampling

            for scan_param_id_coarse, bias in zip([0, 1], biases):
                self.periphery.power_on_HV(self.data.module_name, hv_voltage=0, hv_current_limit=bias_current_limit)
                self.periphery.power_on_HV(self.data.module_name, hv_voltage=bias, hv_current_limit=bias_current_limit)

                # Remember scan param id for current bias setting
                scan_param_ids_list.append(scan_param_id + 1)

                self.data.start_data_taking = False     # Indicates if threshold scan has already started
                vcal_high = VCAL_HIGH_start             # Start value

                pbar = None

                self.log.info('Search for maximum response...')

                self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_stop, vcal_med=VCAL_MED)
                with self.readout(scan_param_id=scan_param_id, callback=self.analyze_data_online):
                    for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                        if not fe == 'skipped' and fe == 'SYNC':
                            self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, latency=122, wait_cycles=400)
                        elif not fe == 'skipped':
                            self.chip.inject_analog_single(repetitions=n_injections, latency=122, wait_cycles=400)

                occupancy = self.data.hist_occ.get()    # Interpret raw data and create occupancy histogram
                max_n_pix = np.count_nonzero(occupancy >= n_injections * max_hits)

                self.log.info('Maximum reponse is {0} of {1} pixels ({2:1.2f}%)'.format(max_n_pix, n_sel_pix, float(max_n_pix) / n_sel_pix * 100.))

                self.log.info('Search for best start parameter...')

                while vcal_high < VCAL_HIGH_stop:
                    if not self.data.start_data_taking:
                        self.log.info('Try Delta VCAL = {0}...'.format(vcal_high - VCAL_MED))

                    self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
                    with self.readout(scan_param_id=scan_param_id, callback=self.analyze_data_online):
                        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                            if not fe == 'skipped' and fe == 'SYNC':
                                self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, latency=122, wait_cycles=400)
                            elif not fe == 'skipped':
                                self.chip.inject_analog_single(repetitions=n_injections, latency=122, wait_cycles=400)
                        if self.data.start_data_taking:
                            self.store_scan_par_values(scan_param_id=scan_param_id, vcal_high=vcal_high, vcal_med=VCAL_MED)

                    occupancy = self.data.hist_occ.get()    # Interpret raw data and create occupancy histogram
                    if not self.data.start_data_taking:     # Log responding pixels
                        n_pix = np.count_nonzero(occupancy >= n_injections * min_hits)
                        self.log.info('{0} of {1} pixels ({2:1.2f}%) see {3}% of injections'.format(n_pix, n_sel_pix, float(n_pix) / n_sel_pix * 100., int(min_hits * 100.)))
                    else:
                        n_pix = np.count_nonzero(occupancy >= n_injections * max_hits)
                        self.log.info('{0} of {1} pixels ({2:1.2f}%) see {3}% of injections'.format(n_pix, n_sel_pix, float(n_pix) / n_sel_pix * 100., int(max_hits * 100.)))

                    if self.data.start_data_taking and n_pix >= max_n_pix:
                        pbar.update(n_sel_pix - pbar.n)
                        pbar.close()
                        self.log.info('S-curve fully sampled. Stop sampling.')
                        break

                    if pbar is not None:
                        try:
                            pbar.update(n_pix - pbar.n)
                        except ValueError:
                            pass

                    if np.count_nonzero(occupancy >= n_injections * max_hits) >= n_sel_pix * max_response:  # Check for stop condition: number of pixels that see all hits
                        if not self.data.start_data_taking:     # Overlooked the start condition
                            self.log.info('Start sampling S-curves for {0} enabled pixels...'.format(n_sel_pix))
                            self.data.start_data_taking = True
                            pbar = tqdm(total=n_sel_pix, unit=' Pixels')
                            scan_param_id += 1
                            # Go back two steps
                            if vcal_high - 2 * VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine > VCAL_HIGH_start:
                                vcal_high = vcal_high - 2 * VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine
                            else:
                                vcal_high = VCAL_HIGH_start
                            continue
                        else:
                            pbar.update(n_sel_pix - pbar.n)
                            pbar.close()
                            self.log.info('S-curve fully sampled. Stop sampling.')
                            break

                    if not self.data.start_data_taking:
                        # Check start condition: number of pixels with hits
                        if np.count_nonzero(occupancy >= n_injections * min_hits) >= float(n_sel_pix * min_response):
                            self.log.info('Start sampling S-curves for {0} enabled pixels...'.format(n_sel_pix))
                            self.data.start_data_taking = True
                            pbar = tqdm(total=n_sel_pix, unit=' Pixels')
                            scan_param_id += 1
                            # Go back one step
                            if vcal_high - VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine > VCAL_HIGH_start:
                                vcal_high = vcal_high - VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine
                            else:
                                vcal_high = VCAL_HIGH_start
                        else:
                            vcal_high += VCAL_HIGH_step_coarse
                    else:  # already scanning, just increase scan pars and value
                        scan_param_id += 1
                        vcal_high += VCAL_HIGH_step_fine
                # Maximum charge reached and no abort triggered (break of while loop)
                else:
                    scan_param_id -= 1
                    self.log.warning('Maximum injection reached. Abort.')

        except Exception as e:
            self.log.error('An error occurred: %s' % e)
        finally:
            self.periphery.power_off_HV(self.data.module_name)

        # Store start indices of first and second s-curve
        self.h5_file.create_carray(self.h5_file.root.configuration, name='scan_parameter_start_indices', title='Start Indices Of S-curves',
                                   obj=np.array(scan_param_ids_list), filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        # Stop analysis process
        self.data.hist_occ.close()

        self.log.success('Scan finished')

    def analyze_data_online(self, data_tuple, receiver=None):
        raw_data = data_tuple[0]
        self.data.hist_occ.add(raw_data)
        # Only store s-curve data when scan started
        if self.data.start_data_taking:
            super(BumpConnThrShScan, self).handle_data(data_tuple, receiver)

    def _analyze(self, create_pdf=True):
        with tb.open_file(self.output_filename + '.h5', 'r') as raw_data_file:
            scan_param_ids_list = list(raw_data_file.root.configuration.scan_parameter_start_indices[:])    # Get start indices of subsequent s-curves

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=False) as a:
            a.analyze_data()
            scan_params_raw = a.get_scan_param_values(scan_parameter='vcal_high') - a.get_scan_param_values(scan_parameter='vcal_med')

        with tb.open_file(self.output_filename + '_interpreted.h5', 'r+') as in_file:
            run_config = au.ConfigDict(in_file.root.configuration.run_config[:])

            start_column, stop_column, start_row, stop_row = run_config['start_column'], run_config['stop_column'], run_config['start_row'], run_config['stop_row']

            enable_mask = np.zeros(shape=(400, 192), dtype=bool)
            enable_mask[start_column:stop_column, start_row:stop_row] = True
            disable_mask = in_file.root.masks.disable[:]

            enabled_pixels = np.logical_and(enable_mask, disable_mask)

            hist_tot = in_file.root.HistTot[:]
            hist_rel_bcid = in_file.root.HistRelBCID[:]

            hist_occ_raw = in_file.root.HistOcc[:]

            hist_occ_reverse = hist_occ_raw[:, :, scan_param_ids_list[0]:scan_param_ids_list[1]]
            hist_occ_forward = hist_occ_raw[:, :, scan_param_ids_list[1]:-1]

            scan_params_reverse = scan_params_raw[scan_param_ids_list[0]:scan_param_ids_list[1]]
            scan_params_forward = scan_params_raw[scan_param_ids_list[1]:-1]

#            threshold_map_reverse, noise_map_reverse, threshold_unc_map_reverse, noise_unc_map_reverse, chi2_map_reverse, _ = au.fit_scurves_multithread(hist_occ_reverse.reshape((192 * 400, -1)), scan_params_reverse, run_config['n_injections'], optimize_fit_range=False)
            threshold_map_reverse, noise_map_reverse, chi2_map_reverse = au.fit_scurves_multithread(hist_occ_reverse.reshape((192 * 400, -1)), scan_params_reverse, run_config['n_injections'], optimize_fit_range=False)

#            threshold_map_forward, noise_map_forward, threshold_unc_map_forward, noise_unc_map_forward, chi2_map_forward, _ = au.fit_scurves_multithread(hist_occ_forward.reshape((192 * 400, -1)), scan_params_forward, run_config['n_injections'], optimize_fit_range=False)
            threshold_map_forward, noise_map_forward, chi2_map_forward = au.fit_scurves_multithread(hist_occ_forward.reshape((192 * 400, -1)), scan_params_forward, run_config['n_injections'], optimize_fit_range=False)

            in_file.create_carray(in_file.root, name='ThresholdMapReverse', title='Reverse Biased Threshold Map',
                                  obj=threshold_map_reverse,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='NoiseMapReverse', title='Reverse Biased Noise Map',
                                  obj=noise_map_reverse,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
#            in_file.create_carray(in_file.root, name='ThresholdUncMapReverse', title='Reverse Biased Threshold Uncertainty Map',
#                                  obj=threshold_unc_map_reverse,
#                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
#            in_file.create_carray(in_file.root, name='NoiseUncMapReverse', title='Reverse Biased Noise Uncertainty Map',
#                                  obj=noise_unc_map_reverse,
#                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='Chi2MapReverse', title='Reverse Biased Chi2 / ndf Map',
                                  obj=chi2_map_reverse,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_carray(in_file.root, name='ThresholdMapForward', title='Forward Biased Threshold Map',
                                  obj=threshold_map_forward,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='NoiseMapForward', title='Reverse Forward Noise Map',
                                  obj=noise_map_forward,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
#            in_file.create_carray(in_file.root, name='ThresholdUncMapForward', title='Forward Biased Threshold Uncertainty Map',
#                                  obj=threshold_unc_map_forward,
#                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
#            in_file.create_carray(in_file.root, name='NoiseUncMapForward', title='Forward Biased Noise Uncertainty Map',
#                                  obj=noise_unc_map_forward,
#                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='Chi2MapForward', title='Forward Biased Chi2 / ndf Map',
                                  obj=chi2_map_forward,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            # Calculate shift of theshold and noise with bias and identify disconnected bumps

            threshold_shift_map = threshold_map_forward - threshold_map_reverse
            noise_shift_map = noise_map_forward - noise_map_reverse

            # Cut values determined from shift distribution plots for a bare chip
            thr_cut_SYNC_low = -25
            thr_cut_SYNC_high = 40
            thr_cut_LIN_low = -20
            thr_cut_LIN_high = 20
            thr_cut_DIFF_low = -10
            thr_cut_DIFF_high = 15
            noise_cut_SYNC_low = -15
            noise_cut_SYNC_high = 30
            noise_cut_LIN_low = -8
            noise_cut_LIN_high = 8
            noise_cut_DIFF_low = -8
            noise_cut_DIFF_high = 15

            connected_bumps_map = np.zeros(shape=(400, 192), dtype=bool)

            connected_bumps_map[0:128, :] = np.logical_or(np.logical_or(threshold_shift_map[0:128, :] < thr_cut_SYNC_low,
                                                                        threshold_shift_map[0:128, :] > thr_cut_SYNC_high),
                                                          np.logical_or(noise_shift_map[0:128, :] < noise_cut_SYNC_low,
                                                                        noise_shift_map[0:128, :] > noise_cut_SYNC_high))
            connected_bumps_map[128:264, :] = np.logical_or(np.logical_or(threshold_shift_map[128:264, :] < thr_cut_LIN_low,
                                                                          threshold_shift_map[128:264, :] > thr_cut_LIN_high),
                                                            np.logical_or(noise_shift_map[128:264, :] < noise_cut_LIN_low,
                                                                          noise_shift_map[128:264, :] > noise_cut_LIN_high))
            connected_bumps_map[264:400, :] = np.logical_or(np.logical_or(threshold_shift_map[264:400, :] < thr_cut_DIFF_low,
                                                                          threshold_shift_map[264:400, :] > thr_cut_DIFF_high),
                                                            np.logical_or(noise_shift_map[264:400, :] < noise_cut_DIFF_low,
                                                                          noise_shift_map[264:400, :] > noise_cut_DIFF_high))

            num_connected_bumps = np.count_nonzero(connected_bumps_map[enabled_pixels])
            num_disconnected_bumps = np.count_nonzero(np.invert(connected_bumps_map[enabled_pixels]))

            self.log.info('Found {0} connected pixels'.format(num_connected_bumps))
            self.log.info('Found {0} disconnected pixels'.format(num_disconnected_bumps))
            self.log.info('Ignored {0} non-enabled pixels'.format(np.count_nonzero(np.invert(enabled_pixels))))

            # Create array for bump connectivity plots
            hist_bump_bonds_connection = np.full(shape=connected_bumps_map.shape, fill_value=-12.)
            hist_bump_bonds_connection[connected_bumps_map] = 1
            hist_bump_bonds_connection[np.invert(connected_bumps_map)] = 0

            in_file.create_carray(in_file.root, name='BumpConnectivityMap', title='Bump Connectivity', obj=hist_bump_bonds_connection,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5', save_png=False) as p:
                p.create_standard_plots()

                # ToT histogram
                p._plot_2d_param_hist(hist=hist_tot.sum(axis=(0, 1)).T,
                                      y_max=15, scan_parameters=scan_params_raw, electron_axis=True,
                                      scan_parameter_name='$\\Delta$ VCAL', title='ToT Scan Parameter Histogram', ylabel='ToT code',
                                      suffix='tot_param_hist')

                # Relative BCID histogram
                p._plot_2d_param_hist(hist=hist_rel_bcid.sum(axis=(0, 1)).T,
                                      y_max=31, scan_parameters=scan_params_raw, electron_axis=True,
                                      scan_parameter_name='$\\Delta$ VCAL', title='RelBCID Scan Parameter Histogram', ylabel='RelBCID',
                                      suffix='bcid_param_hist')

                # S-curves
                p._plot_scurves(scurves=hist_occ_reverse.reshape((192 * 400, -1)).T,
                                scan_parameters=scan_params_reverse, electron_axis=True,
                                n_failed_scurves=p.n_failed_scurves_1,
                                scan_parameter_name='$\\Delta$ VCAL', title='S-curves (reverse bias)', suffix='scurves_reverse')
                p._plot_scurves(scurves=hist_occ_forward.reshape((192 * 400, -1)).T,
                                scan_parameters=scan_params_forward, electron_axis=True,
                                n_failed_scurves=p.n_failed_scurves_2,
                                scan_parameter_name='$\\Delta$ VCAL', title='S-curves (forward bias)', suffix='scurves_forward')

                # Threshold plots
                p._plot_distribution(threshold_map_reverse[threshold_map_reverse != 0 & ~p.enable_mask].T,
                                     plot_range=scan_params_reverse, electron_axis=True,
                                     n_failed_scurves=p.n_failed_scurves_1,
                                     x_axis_title='$\\Delta$ VCAL', title='Threshold distribution for enabled pixels (reverse bias)',
                                     log_y=False, print_failed_fits=True, suffix='threshold_distribution_reverse')
                p._plot_distribution(threshold_map_forward[threshold_map_forward != 0 & ~p.enable_mask].T,
                                     plot_range=scan_params_forward, electron_axis=True,
                                     n_failed_scurves=p.n_failed_scurves_2,
                                     x_axis_title='$\\Delta$ VCAL', title='Threshold distribution for enabled pixels (forward bias)',
                                     log_y=False, print_failed_fits=True, suffix='threshold_distribution_forward')

                # Stacked threshold plots

                min_tdac, max_tdac, range_tdac, _ = rd53a.get_tdac_range(rd53a.get_flavor(p.run_config['stop_column'] - 1))

                p._plot_stacked_threshold(data=threshold_map_reverse[threshold_map_reverse != 0 & ~p.enable_mask].T,
                                          tdac_mask=p.tdac_mask[threshold_map_reverse != 0 & ~p.enable_mask].T,
                                          n_failed_scurves=p.n_failed_scurves_1,
                                          plot_range=scan_params_reverse, electron_axis=True, x_axis_title='$\\Delta$ VCAL',
                                          title='Threshold distribution for enabled pixels (reverse bias)', suffix='tdac_threshold_distribution_reverse',
                                          min_tdac=min(min_tdac, max_tdac), max_tdac=max(min_tdac, max_tdac), range_tdac=range_tdac)
                p._plot_stacked_threshold(data=threshold_map_forward[threshold_map_forward != 0 & ~p.enable_mask].T,
                                          tdac_mask=p.tdac_mask[threshold_map_forward != 0 & ~p.enable_mask].T,
                                          n_failed_scurves=p.n_failed_scurves_2,
                                          plot_range=scan_params_forward, electron_axis=True, x_axis_title='$\\Delta$ VCAL',
                                          title='Threshold distribution for enabled pixels (forward bias)', suffix='tdac_threshold_distribution_forward',
                                          min_tdac=min(min_tdac, max_tdac), max_tdac=max(min_tdac, max_tdac), range_tdac=range_tdac)

                # Histogram showing shift of threshold between reverse and forward bias
                p._plot_distribution(threshold_shift_map[~p.enable_mask].T,
                                     title='Threshold shift distribution for enabled pixels',
                                     plot_range=range(-200, 200), electron_axis=True, use_electron_offset=False,
                                     x_axis_title='$\\Delta$ VCAL', y_axis_title='# of hits',
                                     log_y=False, print_failed_fits=False,
                                     suffix='threshold_shift_distribution')
                # Map showing shift of threshold between reverse and forward bias
                p._plot_occupancy(hist=np.ma.masked_array(np.abs(threshold_shift_map), p.enable_mask).T,
                                  electron_axis=True, use_electron_offset=False,
                                  z_label='|$\\Delta$ Threshold|', title='Threshold Shift Map',
                                  show_sum=False, z_min=None, z_max=None,
                                  suffix='threshold_shift_map')

                # Noise distribution plots
                p._plot_distribution(noise_map_reverse[noise_map_reverse != 0 & ~p.enable_mask].T,
                                     title='Noise distribution for enabled pixels (reverse bias)',
                                     plot_range=None, electron_axis=True, use_electron_offset=False,
                                     n_failed_scurves=p.n_failed_scurves_1,
                                     x_axis_title='$\\Delta$ VCAL', y_axis_title='# of hits',
                                     log_y=False, print_failed_fits=True,
                                     suffix='noise_distribution_reverse')
                p._plot_distribution(noise_map_forward[noise_map_forward != 0 & ~p.enable_mask].T,
                                     title='Noise distribution for enabled pixels (forward bias)',
                                     plot_range=None, electron_axis=True, use_electron_offset=False,
                                     n_failed_scurves=p.n_failed_scurves_2,
                                     x_axis_title='$\\Delta$ VCAL', y_axis_title='# of hits',
                                     log_y=False, print_failed_fits=True,
                                     suffix='noise_distribution_forward')

                # Histogram showing shift of noise between reverse and forward bias
                p._plot_distribution(noise_shift_map[~p.enable_mask].T,
                                     title='Noise shift distribution for enabled pixels',
                                     plot_range=range(-50, 50), electron_axis=True, use_electron_offset=False,
                                     x_axis_title='$\\Delta$ VCAL', y_axis_title='# of hits',
                                     log_y=False, print_failed_fits=False,
                                     suffix='noise_shift_distribution')
                # Map showing shift of noise between reverse and forward bias
                p._plot_occupancy(hist=np.ma.masked_array(np.abs(noise_shift_map), p.enable_mask).T,
                                  electron_axis=True, use_electron_offset=False,
                                  z_label='|$\\Delta$ Noise|', title='Noise Shift Map',
                                  show_sum=False, z_min=None, z_max=None,
                                  suffix='noise_shift_map')

                # Bump connectivity map
                p._plot_bump_connectivity_map(hist_bump_bonds_connection)

                # Bump connectivity histogram
                p._plot_bump_connectivity_hist(connected_bumps=num_connected_bumps,
                                               num_poorly_connected_bumps=0,
                                               disconnected_bumps=num_disconnected_bumps,
                                               unknown_state_bumps=0)


if __name__ == '__main__':
    with BumpConnThrShScan(scan_config=scan_configuration) as scan:
        scan.start()
