#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import logging
import yaml
import shutil
import os
import numpy as np
import tables as tb

from os.path import dirname, join, abspath

import bdaq53
from bdaq53.scans.scan_digital import DigitalScan

from bdaq53.tests import utils

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config_file = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))

configuration = {
    'n_injections': 2,

    'start_column': 172,
    'stop_column': 174,
    'start_row': 0,
    'stop_row': 1,
}


class TestDigitalScan(unittest.TestCase):
    def test_scan_digital(self):
        ''' Enable ITkPixV1 settings'''

        with open(bench_config_file) as f:
            bench_config = yaml.full_load(f)

        bench_config['hardware']['bypass_mode'] = False
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'ITkPixV1'
        bench_config['modules']['module_0']['chip_0']['chip_id'] = 15
        bench_config['modules']['module_0']['chip_0']['receiver'] = 'rx0'
        bench_config['modules']['module_0']['chip_0']['use_ptot'] = False
        bench_config['general']['abort_on_rx_error'] = False  # simulation too slow for RX sync check
        bench_config['general']['use_database'] = False  # data base feature not working for this chip

        logging.info('Starting digital scan test')
        self.sim_dc = 21
        with DigitalScan(bdaq_conf=utils.setup_cocotb(self.sim_dc, chip='ITkPixV1', rx_lanes=1),
                         scan_config=configuration,
                         bench_config=bench_config) as scan:
            scan.start()

        ''' Assert raw data '''
        with tb.open_file(scan.output_filename + '_interpreted.h5', 'r+') as in_file_h5:
            hist_occ = in_file_h5.root.HistOcc[:]

        self.assertEqual(np.sum(hist_occ[172:174, 0:1]), configuration['n_injections'] * 2, 'Incorrect number of hits has been recorded!')

    def tearDown(self):
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
