from importlib import reload
from unittest import mock

import re

import tables as tb
import numpy as np

import bdaq53
# Required to trigger imports of mocked objects
from bdaq53.system import scan_base  # noqa: F401


class BdaqMock:
    ''' Mocks to use bdaq53 without bdaq53 hardware to a level that scans work in software

        Chip raw data can be simulated by providing a raw_data_file. Timings are not preserved since
        FIFO readout is asynchronous.
    '''

    def __init__(self, n_chips=1, raw_data_file=None):
        self.n_chips = n_chips
        self.raw_data_file = raw_data_file
        self.patches = {}

    def start(self):
        ''' Mock out all methods and modules that require hardware
        '''

        # Mock hardware communication via bdaq53.bdaq53.BDAQ53
        self.bdaq53_patcher = []

        def init_mock(cls):
            cls.fw_version = '0.0'
            cls.board_version = 'BDAQ53Mock'
            cls.board_options = 0
            cls.connector_version = 0
            cls.num_rx_channels = 1
            cls.rx_channels = {'rx0': mock.Mock(), 'rx1': mock.Mock(), 'rx2': mock.Mock(), 'rx3': mock.Mock()}
            cls.rx_lanes = {'rx0': 1, 'rx1': 1, 'rx2': 1, 'rx3': 1}

        self.patch_function('bdaq53.system.bdaq53.BDAQ53.init', init_mock)
        self.patch_function('bdaq53.system.bdaq53.BDAQ53.get_tlu_erros', lambda *args, **kwargs_: (0, 0))
        self.patch_function('bdaq53.system.bdaq53.BDAQ53.wait_for_pll_lock')
        self.patch_function('bdaq53.system.bdaq53.BDAQ53.wait_for_aurora_sync')
#
        self.bdaq53_patcher.append(mock.patch('bdaq53.system.bdaq53.BDAQ53.__getitem__'))  # basil dict access
        self.bdaq53_patcher[-1].return_value = 0
        self.bdaq53_patcher[-1].start()

        # Mock chip communication via the chip class

        # Chips to be mocked
        self.chip_classes = [bdaq53.chips.rd53a.RD53A,
                             bdaq53.chips.ITkPixV1.ITkPixV1]
        self.chip_patcher = []
        for chip_class in self.chip_classes:
            self._patch_chip_read_write(chip_class, chip_class._write_register, chip_class._get_register_value)
            chip_module_str = re.findall("'([^']*)'", str(chip_class))[0]
            self.chip_patcher.append(mock.patch('%s.get_temperature_sensors' % chip_module_str, new=lambda *args, **kwargs_: (0, 0)))
            self.chip_patcher[-1].start()
            # Mock write command, since chip hardware not available
            self.chip_patcher.append(mock.patch('%s.write_command' % chip_module_str))
            self.chip_patcher[-1].return_value = 0b0
            self.chip_patcher[-1].start()
            # Mock ADC read command, since chip hardware not available
            self.chip_patcher.append(mock.patch('%s.get_ADC_value' % chip_module_str))
            self.chip_patcher[-1].return_value = (0, 0)
            self.chip_patcher[-1].start()

        # Mock fifo readout
        def print_readout_status(_, rx_channel=None):
            if rx_channel is None:
                return [0] * self.n_chips, [0] * self.n_chips, [0] * self.n_chips
            return True

        self.patch_function('bdaq53.system.fifo_readout.FifoReadout.print_readout_status', print_readout_status)
        self.patch_function('bdaq53.system.fifo_readout.FifoReadout.reset_sram_fifo')

        def rx_stat(_, rx_channel=None):
            if rx_channel is None:
                return [True] * self.n_chips
            return True

        self.patch_function('bdaq53.system.fifo_readout.FifoReadout.get_rx_sync_status', rx_stat)

        def get_count(_, rx_channel=None):
            if rx_channel is None:
                return [0] * self.n_chips
            return 0

        self.patch_function('bdaq53.system.fifo_readout.FifoReadout.get_rx_fifo_discard_count', get_count)
        self.patch_function('bdaq53.system.fifo_readout.FifoReadout.get_rx_soft_error_count', get_count)
        self.patch_function('bdaq53.system.fifo_readout.FifoReadout.get_rx_hard_error_count', get_count)

        if self.raw_data_file:
            self.in_file_h5 = tb.open_file(self.raw_data_file)
            self.meta_data = self.in_file_h5.root.meta_data[:]
            self.raw_data = self.in_file_h5.root.raw_data
            n_readouts = self.meta_data.shape[0]
            self.i_ro = 0

        def read_data(cls):
            if self.raw_data_file:  # return chip raw data from file
                if self.i_ro < n_readouts:
                    # Raw data indeces of readout
                    i_start = self.meta_data['index_start'][self.i_ro]
                    i_stop = self.meta_data['index_stop'][self.i_ro]
                    self.i_ro += 1
                    return self.raw_data[i_start:i_stop]
                return np.array([], dtype=np.int32)
            else:  # just count upwards
                data = []
                if not cls.stop_readout.isSet():  # Create some fake data
                    # Create one data word per active readout channel with correct rx id to be able to check filtering
                    for rx_id in cls._readout_channels:
                        data.append((cls._record_count // len(cls._readout_channels)) | (rx_id << 20))
                return np.array(data, dtype=np.int32)

        self.patch_function('bdaq53.system.fifo_readout.FifoReadout.read_data', read_data)

        # Periphery mock
        def __init__(cls, bench_config=None, dut_conf=None):
            from bdaq53.system import logger
            import os
            import yaml
            cls.dut = None
            cls.is_monitoring = False
            cls.aux_devices = {}
            cls.module_devices = {}
            cls.log = logger.setup_derived_logger('Periphery')

            proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

            if bench_config is None or isinstance(bench_config, str):
                if bench_config is None:
                    bench_config = os.path.join(proj_dir, 'bdaq53', 'testbench.yaml')
                with open(bench_config) as f:
                    bench_config = yaml.full_load(f)
            if not isinstance(bench_config, dict):
                raise ValueError('bench_config is of unknown type!')

            if not bench_config['periphery'].get('enable_periphery', False):
                cls.log.notice('Periphery is disabled.')
                cls.enabled = False
                return
            else:
                cls.configuration = bench_config['periphery']
                cls.configuration['modules'] = bench_config['modules']
                cls.enabled = True

            if dut_conf is None:
                bdaq53_path = os.path.dirname(bdaq53.__file__)
                cls.dut_conf = os.path.join(bdaq53_path, 'periphery.yaml')
            else:
                cls.dut_conf = dut_conf

            # Create DUT that behaves sufficiently like a basil dut
            with open(cls.dut_conf) as f:
                dut_conf = yaml.full_load(f)

            cls.dut = mock.MagicMock()
            cls.dut.__getitem__.side_effect = mock.MagicMock()

            cls.module_list = list(bench_config['modules'].keys())

        self.patch_function('bdaq53.system.periphery.BDAQ53Periphery.__init__', __init__)

        # Reload mocked import
        reload(bdaq53.system.scan_base)

    def stop(self):
        ''' Remove mocks to allow normal operation '''
        # self.bdaq53_patcher.stop()

        if self.raw_data_file:
            self.in_file_h5.close()

        for patch in self.patches.values():
            patch.stop()

        for patch in self.bdaq53_patcher:
            patch.stop()
        for patch in self.chip_patcher:
            patch.stop()
        for chip_class in self.chip_classes:
            self._unpatch_chip_read_write(chip_class)

    def reset(self):
        self.stop()
        self.start()

    def reset_replay(self):
        ''' Reset raw data replay '''
        self.i_ro = 0

    def patch_function(self, target, function=None):
        if target in self.patches:
            try:
                self.patches[target].stop()
            except RuntimeError:  # stop called on not started patcher
                pass
            del self.patches[target]
        self.patches[target] = mock.patch(target, new=function if function else mock.DEFAULT)
        self.patches[target].start()

    def _patch_chip_read_write(self, chip_class, write_method, read_method):
        ''' Monkey patch RD53A chip write and read

            Add write/read of registers without chip hardware.
            Use storage in software  to keep configuration
        '''
        # Monkey patch register storage in software instead of chip
        chip_class.original_write = write_method
        chip_class.original_read = read_method

        def wrap_write_register(cls, address, data, write=True):
            # BDAQ53 relies on power on reset values
            # Therefore write always to chip storage even if write=false
            try:
                cls.register_values
            except AttributeError:
                cls.register_values = {}
            cls.register_values[address] = data
            return cls.original_write(address, data, write)

        def wrap_read_register(cls, address, *_):
            return cls.register_values[address]

        chip_class._write_register = wrap_write_register
        chip_class._get_register_value = wrap_read_register

    def _unpatch_chip_read_write(self, chip_class):
        ''' Remove patch and restore std. methods '''
        chip_class._write_register = chip_class.original_write
        chip_class._get_register_value = chip_class.original_read
