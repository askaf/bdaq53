#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import unittest
from unittest import mock
import numpy as np
import yaml

import bdaq53  # noqa: E731
from bdaq53.tests import bdaq_mock
from bdaq53.tests import utils  # noqa: E731

from bdaq53.scans.scan_analog import AnalogScan
from bdaq53.scans.scan_digital import DigitalScan
from bdaq53.scans.scan_threshold_fast import FastThresholdScan
from bdaq53.scans.scan_crosstalk import CrosstalkScan
from bdaq53.scans.scan_in_time_threshold import InTimeThrScan
from bdaq53.scans.scan_source_injection import SourceScanInj


bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, 'tests', 'test_software', 'output_data'))
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


class TestShiftInject(unittest.TestCase):

    scan_configuration = {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 192,
        'n_injections': 100,

        'VCAL_MED': 500,
        'VCAL_HIGH': 1300}

    @classmethod
    def setUpClass(cls):
        super(TestShiftInject, cls).setUpClass()

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)
        cls.bench_config['general']['use_database'] = False  # deactivate failing feature

    @classmethod
    def tearDownClass(cls):
        utils.try_remove(os.path.join(data_folder))

    def old_scan_loop(self, n_injections=100, **kwargs):
        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection']):
            if not fe == 'skipped' and fe == 'SYNC':
                self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
            elif not fe == 'skipped':
                self.chip.inject_analog_single(repetitions=n_injections)

    def old_scan_loop_digital(self, n_injections=100, **kwargs):
        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection']):
            if not fe == 'skipped':
                self.chip.inject_digital(repetitions=n_injections)

    def old_scan_loop_crosstalk(self, injection_type='cross_injection', n_injections=100, VCAL_MED=0, VCAL_HIGH_start=0, VCAL_HIGH_stop=4096, VCAL_HIGH_step=102, **_):
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
            for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], pattern=injection_type, cache=True, skip_empty=False):
                if fe == 'SYNC':
                    self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                else:
                    self.chip.inject_analog_single(repetitions=n_injections)

    def old_scan_loop_intime_threshold(self, start_column=0, stop_column=400, start_row=0, stop_row=192, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, **_):
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        finedelay_range = [15]  # Case if no finedelay map is provided
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            for delay in finedelay_range:
                self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED, fine_delay=delay)
                for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                    if not fe == 'skipped' and fe == 'SYNC':
                        self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                    elif not fe == 'skipped':
                        self.chip.inject_analog_single(repetitions=n_injections)

    def old_scan_loop_source_scan_inj(self, n_injections=1, **kwargs):
        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection']):
            if not fe == 'skipped' and fe == 'SYNC':
                self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, send_trigger=False)
            elif not fe == 'skipped':
                self.chip.inject_analog_single(repetitions=n_injections, send_trigger=False)

    def old_scan_loop_ptot(self, n_injections=100, latency=23, **kwargs):
        for fe, active_pixels in (self.chip.masks.shift(masks=['enable', 'injection', 'hitbus'], pattern='ptot')):
            if not fe == 'skipped':
                for n in range(8):
                    self.chip.enable_core_col_clock(core_cols=[i + n for i in range(0, 50, 8)])
                    self.chip.inject_analog_single(repetitions=n_injections, latency=latency)

    def old_scan_loop_digital_ptot(self, n_injections=100, latency=23, cal_edge_width=32, **kwargs):
        for fe, active_pixels in (self.chip.masks.shift(masks=['enable', 'injection', 'hitbus'], pattern='ptot')):
            if not fe == 'skipped':
                self.chip.inject_digital(repetitions=n_injections, latency=23, cal_edge_width=cal_edge_width)

    def old_scan_loop_fast(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, CAL_EDGE_latency=9, wait_cycles=300, **_):
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_start, vcal_med=VCAL_MED)
        if self.chip.chip_type.lower() == 'rd53a':
            trigger_latency = self.chip.registers['LATENCY_CONFIG'].get()
            self.chip.registers['LATENCY_CONFIG'].write(CAL_EDGE_latency * 4 + 12)
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        scan_param_id_range = range(0, len(vcal_high_range))
        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
            for cnt, vcal_high in enumerate(vcal_high_range):
                self.chip.registers['VCAL_HIGH'].write(vcal_high)
                if not fe == 'skipped' and fe == 'SYNC':
                    self.chip.inject_analog_single(repetitions=n_injections, latency=CAL_EDGE_latency,
                                                   wait_cycles=wait_cycles, send_ecr=True)
                elif not fe == 'skipped':
                    self.chip.inject_analog_single(repetitions=n_injections, latency=CAL_EDGE_latency,
                                                   wait_cycles=wait_cycles)
            vcal_high_range = np.flip(vcal_high_range)
            scan_param_id_range = np.flip(scan_param_id_range)
        if self.chip.chip_type.lower() == 'rd53a':
            self.chip.registers['LATENCY_CONFIG'].write(trigger_latency)

    def old_scan_loop_fast_ptot(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, wait_cycles=200, **kwargs):
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        scan_param_id_range = range(0, len(vcal_high_range))
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_start, vcal_med=VCAL_MED)
        for fe, active_pixels in (self.chip.masks.shift(masks=['enable', 'injection', 'hitbus'], pattern='ptot', cache=True)):
            for cnt, vcal_high in enumerate(vcal_high_range):
                self.chip.registers['VCAL_HIGH'].write(vcal_high)
                for n in range(8):
                    self.chip.enable_core_col_clock(core_cols=[i + n for i in range(0, 50, 8)])
                    if not fe == 'skipped' and fe == 'SYNC':
                        self.chip.inject_analog_single(repetitions=n_injections, latency=23,
                                                       wait_cycles=200, send_ecr=True)
                    elif not fe == 'skipped':
                        self.chip.inject_analog_single(repetitions=n_injections, latency=23,
                                                       wait_cycles=200)
            vcal_high_range = np.flip(vcal_high_range)
            scan_param_id_range = np.flip(scan_param_id_range)

    def test_shift_inject_loop_rd53a_digital(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''

        self.bench_config['modules'] = {'module_0':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0001", 'chip_type': "rd53a", 'chip_id': 0,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        try:
            with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
                with DigitalScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                    scan.configure()
                    cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                    scan.scan()
                cmds_old = cmds.copy()

                cmds = []
                with DigitalScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                    self.chip = scan.chip
                    scan._scan = self.old_scan_loop_digital
                    scan.configure()
                    cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                    scan.scan()

                # Use numpy arrays with data since they can be checked for equality much faster
                a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
                self.assertTrue(np.array_equal(a, b))
        finally:
            bhm.stop()

    def test_shift_inject_loop_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''

        self.bench_config['modules'] = {'module_0':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0001", 'chip_type': "rd53a", 'chip_id': 0,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            with AnalogScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()
            cmds_old = cmds.copy()

            cmds = []
            with AnalogScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()

    def test_shift_inject_loop_fast_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A and fast injection loop '''

        self.bench_config['modules'] = {'module_0':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0001", 'chip_type': "rd53a", 'chip_id': 0,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            with FastThresholdScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()
            cmds_old = cmds.copy()

            cmds = []
            with FastThresholdScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_fast
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()

    def test_shift_inject_loop_rd53b(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53B '''

        self.bench_config['modules'] = {'module_1':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0002", 'chip_type': "itkpixv1", 'chip_id': 15, 'use_ptot': False,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.ITkPixV1.ITkPixV1.write_command', side_effect=store_cmd):
            with AnalogScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            cmds_old = cmds.copy()

            cmds = []
            with AnalogScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()

    def test_shift_inject_loop_rd53b_ptot_digital(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53B and PToT enabled '''

        self.bench_config['modules'] = {'module_1':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0002", 'chip_type': "itkpixv1", 'chip_id': 15, 'use_ptot': True,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.ITkPixV1.ITkPixV1.write_command', side_effect=store_cmd):
            with DigitalScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            cmds_old = cmds.copy()

            with DigitalScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_digital_ptot
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()

    def test_shift_inject_loop_rd53b_ptot(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53B and PToT enabled '''

        self.bench_config['modules'] = {'module_1':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0002", 'chip_type': "itkpixv1", 'chip_id': 15, 'use_ptot': True,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.ITkPixV1.ITkPixV1.write_command', side_effect=store_cmd):
            with AnalogScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            cmds_old = cmds.copy()

            with AnalogScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_ptot
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()

    def test_shift_inject_loop_fast_ptot(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53B and PToT enabled and fast injection loop '''

        self.bench_config['modules'] = {'module_1':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0002", 'chip_type': "itkpixv1", 'chip_id': 15, 'use_ptot': True,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.ITkPixV1.ITkPixV1.write_command', side_effect=store_cmd):
            with FastThresholdScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()
            cmds_old = cmds.copy()

            with FastThresholdScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_fast_ptot
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()

    def test_shift_inject_loop_crosstalk_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''

        self.bench_config['modules'] = {'module_0':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0001", 'chip_type': "rd53a", 'chip_id': 0,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            with CrosstalkScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()
            cmds_old = cmds.copy()

            cmds = []
            with CrosstalkScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_crosstalk
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()

    def test_shift_inject_loop_intime_threshold_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''

        self.bench_config['modules'] = {'module_0':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0001", 'chip_type': "rd53a", 'chip_id': 0,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            with InTimeThrScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()
            cmds_old = cmds.copy()

            cmds = []
            with InTimeThrScan(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_intime_threshold
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()

    def test_shift_inject_loop_source_scan_inj_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''

        self.bench_config['modules'] = {'module_0':
                                        {'identifier': "unknown", 'power_cycle': False,
                                         'chip_0': {'chip_sn': "0x0001", 'chip_type': "rd53a", 'chip_id': 0,
                                                    'receiver': "rx0", 'chip_config_file': None, 'record_chip_status': True,
                                                    'use_good_pixels_diff': False, 'send_data': "tcp://127.0.0.1:5500"}}}

        cmds = []  # store all send command unraveled

        # Use hardware mocks to be able to test without hardware
        bhm = bdaq_mock.BdaqMock(n_chips=1)
        bhm.start()

        def store_cmd(cmd, repetitions=100):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            with SourceScanInj(self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()
            cmds_old = cmds.copy()

            cmds = []
            with SourceScanInj(self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_source_scan_inj
                scan.configure()
                cmds = []  # Reset command list, since only insterested in commands sent during scan for this test
                scan.scan()

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds_old, dtype=np.int16), np.array(cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

        bhm.stop()


if __name__ == '__main__':
    unittest.main()
