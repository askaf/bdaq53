#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import copy
import importlib
import pkgutil
import inspect
import logging
import sys
import time
import os
import shutil
import unittest

import yaml

import bdaq53
from bdaq53 import scans
from bdaq53.system import scan_base  # noqa: F401
from bdaq53.chips import rd53a  # noqa: F401
from bdaq53.tests import utils
from bdaq53.tests import bdaq_mock

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


skip_scan = ['Eudaq',  # needs special setup; tested on dedicated runner
             'DAC',  # needs periphery multimeter
             'SensorIVScan',  # needs periphery sourcemeter
             'TimewalkScan',  # takes too long?!
             'PixelRegisterScan',  # has custom analysis
             'BumpConnThrShScan'  # needs periphery
             ]

rd53b_only = [
]

# Skip analysis in scans analysis that need data and otherwise fails
skip_analysis = ['InTimeThrScan',  # complex analysis that crashed on empty data
                 'InjDelayScan',  # complex analysis that crashed on empty data
                 'MergedBumpsScan',  # needs analyzed data file
                 'NoiseOccScan',  # needs analyzed data file
                 'SourceScan',  # needs analyzed data file
                 'StuckPixelScan',  # needs analyzed data file
                 'NoiseOccAdvScan',  # does not call analyze_data function
                 'BumpConnCTalkScan',  # needs analyzed data file
                 'CrosstalkScan'  # mean threshold cannot be determined
                 ]


def import_submodules(package, recursive=True):
    """ Import all submodules of a module, recursively, including subpackages

    :param package: package (name or actual module)
    :type package: str | module
    :rtype: dict[str, types.ModuleType]
    """
    if isinstance(package, str):
        package = importlib.import_module(package)
    results = {}
    for _, name, is_pkg in pkgutil.walk_packages(package.__path__):
        full_name = package.__name__ + '.' + name
        results[full_name] = importlib.import_module(full_name)
        if recursive and is_pkg:
            results.update(import_submodules(full_name))
    return results


def get_scan_classes():
    ''' Get all scan classes with their scan configuration '''
    scan_classes = []
    for mod_name, module in import_submodules(package=scans, recursive=False).items():
        module_classes = [m for m in inspect.getmembers(module, inspect.isclass) if m[1].__module__ == mod_name]
        if module_classes:  # no class defined in meta scans
            if 'Scan' in module_classes[0][0]:  # check if class is a scan (not tuning,  calibration, ...)
                scan_classes.append((module_classes[0][1], module.scan_configuration))
    return scan_classes


class TestScans(unittest.TestCase):
    ''' Test scan scripts at a high level by running them all.

    New Scripts are automatically tested and do not need to be added.
    Mainly useful to catch API related issues, not logical bugs.
    If errors are mentioned in the log or exceptions occur a test fails.
    Since data from chip is not simulated it cannot make sense.
    '''

    @classmethod
    def setUpClass(cls):
        super(TestScans, cls).setUpClass()

        analysis_logger = logging.getLogger('Analysis')
        cls.analysis_log_handler = utils.MockLoggingHandler(level='DEBUG')
        analysis_logger.addHandler(cls.analysis_log_handler)
        cls.analysis_log_messages = cls.analysis_log_handler.messages

        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=4)
        # Speed up testing time drastically, by not calling mask shifting
        cls.bhm.patch_function('bdaq53.chips.rd53a.RD53AMaskObject.update')
        cls.bhm.patch_function('bdaq53.chips.ITkPixV1.ITkPixV1MaskObject.update')
        cls.bhm.start()

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)

        # Use the second chip of a dual module to make setup more complex
        cls.bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(cls.bench_config['modules']['module_0']['chip_0'])
        cls.bench_config['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        cls.bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        cls.bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

    @classmethod
    def tearDownClass(cls):
        cls.bhm.stop()

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls.analysis_log_handler.reset()
        shutil.rmtree('output_data', ignore_errors=True)  # always delete output from previous test

    def check_scan_success(self, scan_log_messages, skip_analysis=False):
        ''' Check the log output if scan was successfull '''
        if scan_log_messages['error'] or self.analysis_log_messages['error']:
            return False
        if 'Scan finished' not in scan_log_messages['success'] or 'All done!' not in scan_log_messages['success']:
            return False
        if not skip_analysis:
            if 'Analyzing data...' not in self.analysis_log_messages['info']:
                return False
        return True

    def test_scans_rd53a(self):
        ''' Run all scans consecutively with a rd53a mock to check for runtime errors.

            If possible also run analysis step.
        '''
        for ScanClass, scan_config in get_scan_classes():
            if any([s in ScanClass.__name__ for s in skip_scan]):
                continue

            # Catch scan output to check for errors reported
            scan_logger = logging.getLogger(ScanClass.__name__)
            scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
            scan_logger.addHandler(scan_log_handler)
            scan_log_messages = scan_log_handler.messages

            if ScanClass.__name__ not in rd53b_only:  # rd53a compatible scans
                with ScanClass(scan_config=scan_config, bench_config=self.bench_config) as scan:
                    skip_ana_step = any([s in ScanClass.__name__ for s in skip_analysis])
                    scan.configure()
                    scan.scan()
                    # Check if analysis can be run
                    if not skip_ana_step:
                        time.sleep(1)  # give slow CERN file system time to close file handle
                        scan.analyze()

                self.assertTrue(self.check_scan_success(scan_log_messages, skip_analysis=skip_ana_step))
                self.analysis_log_handler.reset()

            else:  # running ITkPixV1 scan with rd53a should raise an exception
                with self.assertRaises(NotImplementedError):
                    with ScanClass() as scan:
                        scan.configure()

    def test_scans_rd53b(self):
        ''' Run scans consecutively with a rd53b mock to check for runtime errors.

            If possible also run analysis step.
        '''

        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'ITkPixV1'
        bench_config['modules']['module_0']['chip_1']['chip_type'] = 'ITkPixV1'

        for ScanClass, scan_config in get_scan_classes():
            if not any([s in ScanClass.__name__ for s in rd53b_only]):
                continue
            # First sub test failure cancels this long lasting test, tearDown is not called during tests, thus this simulates running scans in a chain
            with self.subTest(msg='Checking for scan failure using ITkPixV1', scan=ScanClass.__name__):
                # Catch scan output to check for errors reported
                scan_logger = logging.getLogger(ScanClass.__name__)
                scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
                scan_logger.addHandler(scan_log_handler)
                scan_log_messages = scan_log_handler.messages

                scan_config = sys.modules[ScanClass.__module__].scan_configuration

                with ScanClass(bench_config=bench_config, scan_config=scan_config) as scan:
                    skip_ana_step = any([s in ScanClass.__name__ for s in skip_analysis])
                    scan.configure()
                    scan.scan()
                    # Check if analysis can be run
                    if not skip_ana_step:
                        time.sleep(1)  # give slow CERN file system time to close file handle
                        scan.analyze()

                self.assertTrue(self.check_scan_success(scan_log_messages, skip_analysis=skip_ana_step))
                self.analysis_log_handler.reset()


if __name__ == '__main__':
    unittest.main()
