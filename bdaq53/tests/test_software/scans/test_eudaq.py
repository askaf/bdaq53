#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
This test case needs a working eudaq 1.7 installation and the
eudaq/python folder added to the python path, see:
https://gitlab.cern.ch/silab/bdaq53/wikis/Eudaq-integration

The installation has to done at std. location (bin/lib in
eudaq folder)

Otherwise the unit tests are all skipped.
'''

import os
import time
import unittest
import threading
import subprocess
import sys
from queue import Queue, Empty

import tables as tb
import psutil
import numpy as np

import bdaq53
from bdaq53.scans import scan_eudaq
from bdaq53.analysis import analysis_utils as au
from bdaq53.tests import bdaq_mock

from bdaq53.tests import utils

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))
dir_path = os.path.dirname(os.path.realpath(__file__))


def run_process_with_queue(command, arguments=None):
    if os.name == 'nt':
        creationflags = subprocess.CREATE_NEW_PROCESS_GROUP
    else:
        creationflags = 0
    args = [command]
    if arguments:
        args += [str(a) for a in arguments]
    ON_POSIX = 'posix' in sys.builtin_module_names
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         bufsize=1, close_fds=ON_POSIX, creationflags=creationflags)
    print_queue = Queue()
    t = threading.Thread(target=enqueue_output, args=(p.stdout, print_queue))
    t.daemon = True  # thread dies with the program
    t.start()

    return p, print_queue


def kill(proc):
    ''' Kill process by id, including subprocesses.

        Works for Linux and Windows
    '''
    process = psutil.Process(proc.pid)
    for child_proc in process.children(recursive=True):
        child_proc.kill()
    process.kill()


def eurun_control(min_connections=2, run_time=5):
    ''' Start a EUDAQ run control instance

    Waits for min_connections to send configure command.
    Start the run for run_time and then send EOR.
    '''
    from PyEUDAQWrapper import PyRunControl
    prc = PyRunControl("44000")
    while prc.NumConnections < min_connections:
        time.sleep(1)
        print("Number of active connections: ", prc.NumConnections)
    # Load configuration file
    prc.Configure(os.path.join(dir_path, "TestConfig.conf"))
    # Wait that the connections can receive the config
    time.sleep(2)
    # Wait until all connections answer with 'OK'
    while not prc.AllOk:
        time.sleep(0.5)
        print("Successfullly configured!")
    # Start the run
    prc.StartRun()
    # Wait for run start
    while not prc.AllOk:
        time.sleep(0.5)
    # Main run loop
    time.sleep(run_time)
    # Stop run
    prc.StopRun()
    time.sleep(2)


def enqueue_output(out, queue):
    ''' https://stackoverflow.com/questions/375427/non-blocking-read-on-a-subprocess-pipe-in-python '''
    for line in iter(out.readline, b''):
        queue.put(line)
    out.close()


def queue_to_string(queue, max_lines=1000):
    return_str = ''
    for _ in range(max_lines):
        try:
            line = queue.get_nowait()
        except Empty:
            break
        else:  # got line
            return_str += line.decode("utf-8") + '\n'
    return return_str


class TestEudaq(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        try:
            import PyEUDAQWrapper
            eudaq_wrapper_path = os.path.dirname(PyEUDAQWrapper.__file__)
            cls.eudaq_bin_folder = os.path.abspath(os.path.join(eudaq_wrapper_path, '..', 'bin'))
            cls.no_eudaq_install = False
        except ImportError:
            cls.no_eudaq_install = True

        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=4, raw_data_file=os.path.join(data_folder, 'ext_trigger_scan_tb.h5'))
        cls.bhm.start()

    @classmethod
    def tearDownClass(cls):
        os.remove(os.path.join(data_folder, 'ext_trigger_scan_tb_snd.h5'))
        cls.bhm.stop()

    @unittest.skip('Needs update')
    def test_replay_data(self):
        ''' Test the communication with replayed data using the replay data functionality.

            Starts a complete EUDAQ data taking scenario:
                Run control + TestDataConverter + bdaq53 producer

            Success of this test is checked by stdout inspection.
        '''

        if self.no_eudaq_install:
            self.skipTest("No working EUDAQ installation found, skip test!")
        # Start EUDAQ
        t_rc = threading.Thread(target=eurun_control, kwargs={"min_connections": 2,
                                                              "run_time": 5})
        t_rc.start()
        time.sleep(2)
        # Start data collector
        data_coll_process, data_coll_q = run_process_with_queue(command=os.path.join(self.eudaq_bin_folder, 'euDataCollector.exe'))
        time.sleep(2)
        # Start bdaq producer
        bdaq53_prod_process, bdaq53_prod_q = run_process_with_queue(
            command='bdaq_eudaq',
            arguments=['--replay',
                       '%s' % os.path.join(data_folder, 'ext_trigger_scan.h5'),
                       '--delay', '0.001'])
        time.sleep(10)
        t_rc.join()
        time.sleep(5)
        # Read up to 1000 lines from Data Collector output
        coll_output = queue_to_string(data_coll_q)
        prod_output = queue_to_string(bdaq53_prod_q)

        # Subprocesses have to be killed since no terminate signal is available
        # from euRun wrapped with python
        kill(data_coll_process)
        kill(bdaq53_prod_process)

        # Check configurig of entities
        self.assertTrue('Configuring' in coll_output, msg='Cannot find configuring step in log output!')
        self.assertTrue('Configured' in coll_output, msg='Cannot find finished configuring message in log output!')
        self.assertTrue('Received Configuration' in prod_output, msg='Cannot find received configuration message in log output!')

        # Check event building from replayed data
        self.assertTrue('Complete Event' in coll_output)

        # Check stop run signal received
        self.assertTrue('Received EORE Event' in coll_output)
        self.assertTrue('Stop Run received' in prod_output)

    def test_send_data_new(self):
        ''' Run a EUDAQ scan replaying real data.

            Check that all data is send and correctly aligned at the trigger number
        '''
        # Create temporary data structures
        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan_tb.h5')
        raw_data_file_snd = os.path.join(data_folder, 'ext_trigger_scan_tb_snd.h5')
        h5_file_snd = tb.open_file(raw_data_file_snd, mode='w', title='EUDAQ test send data')
        raw_data_earray_snd = h5_file_snd.create_earray(h5_file_snd.root, name='raw_data', atom=tb.UIntAtom(),
                                                        shape=(0,), title='raw_data',
                                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        self.n_calls = 0

        def SendEvent(raw_data):
            ''' Fake EUDAQ send function that is called per event and must have a trigger word
                following the raw data
            '''
            assert raw_data[0] & au.TRIGGER_HEADER  # first raw data word is trigger word
            assert np.where(raw_data & au.TRIGGER_HEADER > 0)[0].shape[0] == 1  # only one trigger word expected
            raw_data_earray_snd.append(raw_data)
            self.n_calls += 1

        scan_cfg = scan_eudaq.scan_configuration.copy()
        scan_cfg['callback'] = SendEvent
        scan_cfg['scan_timeout'] = 30  # 30 seconds are needed to replay raw data file with 603 readouts
        with scan_eudaq.EudaqScan(scan_config=scan_cfg) as scan:
            scan.start()
        h5_file_snd.close()

        # Check raw data is stored locally when using scan_eudaq
        data_equal, error_msg = utils.compare_h5_files(raw_data_file, scan.output_filename + '.h5', node_names=['raw_data'])
        self.assertTrue(data_equal, msg=error_msg)

        # Check send raw data is complete and correct when using scan_eudaq
        data_equal, error_msg = utils.compare_h5_files(raw_data_file, raw_data_file_snd, node_names=['raw_data'])
        self.assertTrue(data_equal, msg=error_msg)

        # Checks if number of calls to the data sending function corresponds
        # to the number of triggers. EUDAQ needs stricly one event per trigger
        # since it does not use the trigger number.
        with tb.open_file(raw_data_file) as in_file:
            raw_data = in_file.root.raw_data[:]
            sel_trg = raw_data & au.TRIGGER_HEADER > 0
            assert np.all(np.diff(raw_data[sel_trg] & au.TRG_MASK) == 1)  # Just as cross check that trigger is ok in the fixture file
            n_trigger = np.count_nonzero(sel_trg)

        self.assertEqual(self.n_calls, n_trigger)


if __name__ == '__main__':
    unittest.main()
