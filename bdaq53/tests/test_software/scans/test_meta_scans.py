#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import copy
import importlib
import logging
import os
import shutil
import types
import unittest

import yaml

import bdaq53
from bdaq53.tests import utils
from bdaq53.tests import bdaq_mock

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


def _run_meta_scan(script):
    ''' Running __main__ entry points for testing is complicated.

        This solution is inspired by:
        https://stackoverflow.com/questions/19009932/import-arbitrary-python-source-file-python-3-3
        https://stackoverflow.com/questions/5850268/how-to-test-or-mock-if-name-main-contents
    '''
    loader = importlib.machinery.SourceFileLoader('__main__', script)
    mod = types.ModuleType(loader.name)
    loader.exec_module(mod)


def _scan_loggers(names):
    loggers = []
    # Catch scan output to check for reported errors
    for name in names:
        scan_logger = logging.getLogger(name)
        scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
        scan_logger.addHandler(scan_log_handler)
        loggers.append(scan_log_handler.messages)

    return loggers


class TestMetaScans(unittest.TestCase):
    ''' Test meta scan scripts at a high level by just running them.

    Mainly useful to catch API related issues, not logical bugs.
    If errors are mentioned in the log or exceptions occur a test fails.
    Since data from chip is not simulated data from chip cannot make sense.
    '''

    @classmethod
    def setUpClass(cls):
        super(TestMetaScans, cls).setUpClass()

        analysis_logger = logging.getLogger('Analysis')
        cls.analysis_log_handler = utils.MockLoggingHandler(level='DEBUG')
        analysis_logger.addHandler(cls.analysis_log_handler)
        cls.analysis_log_messages = cls.analysis_log_handler.messages

        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=2)
        # Speed up testing time drastically, by not calling mask shifting
        cls.bhm.patch_function('bdaq53.chips.rd53a.RD53AMaskObject.update')
        cls.bhm.start()
        shutil.copyfile(bench_config, bench_config + '~')  # store original

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)

        # Use the second chip of a dual module to make setup more complex
        cls.bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(cls.bench_config['modules']['module_0']['chip_0'])
        cls.bench_config['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        cls.bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        cls.bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

        with open(bench_config, 'w') as outfile:
            outfile.write(yaml.dump(cls.bench_config, default_flow_style=False))

    @classmethod
    def tearDownClass(cls):
        cls.bhm.stop()
        shutil.move(bench_config + '~', bench_config)  # restore original

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls.analysis_log_handler.reset()
        shutil.rmtree('output_data', ignore_errors=True)  # always delete output from previous scan

    def check_scan_success(self, scan_log_messages):
        ''' Check the log output if scan was successfull '''
        if scan_log_messages['error'] or self.analysis_log_messages['error']:
            return False
        if not any('All done!' in msg for msg in scan_log_messages['success']):
            return False
        return True

    def test_meta_threshold_tuning(self):
        script = os.path.join(bdaq53_path, 'scans', 'meta_tune_threshold.py')
        loggers = _scan_loggers(names=['GDACTuning', 'TDACTuning', 'NoiseOccScan', 'StuckPixelScan', 'NoiseOccScan', 'ThresholdScan'])
        _run_meta_scan(script)

        for scan_log_messages in loggers:
            print(scan_log_messages['info'])
            self.assertTrue(self.check_scan_success(scan_log_messages))

    def test_meta_tot_and_threshold_tuning(self):
        script = os.path.join(bdaq53_path, 'scans', 'meta_tune_tot_and_threshold.py')
        loggers = _scan_loggers(names=['GDACTuning', 'TDACTuning', 'TotTuning', 'NoiseOccScan', 'StuckPixelScan', 'NoiseOccScan', 'ThresholdScan'])
        _run_meta_scan(script)

        for scan_log_messages in loggers:
            print(scan_log_messages['info'])
            self.assertTrue(self.check_scan_success(scan_log_messages))

    def test_meta_merged_bumps(self):
        script = os.path.join(bdaq53_path, 'scans', 'meta_scan_merged_bumps.py')
        loggers = _scan_loggers(names=['GDACTuning', 'TDACTuning', 'MergedBumpsScan'])
        _run_meta_scan(script)

        for scan_log_messages in loggers:
            self.assertTrue(self.check_scan_success(scan_log_messages))

    def test_meta_disconnected_bumps_threshold(self):
        script = os.path.join(bdaq53_path, 'scans', 'meta_scan_disconnected_bumps_threshold.py')
        loggers = _scan_loggers(names=['GDACTuning', 'TDACTuning', 'BumpConnThrShScan'])

        with self.assertRaises(Exception):  # no periphery defined
            _run_meta_scan(script)

        for i, scan_log_messages in enumerate(loggers):
            if i < 2:
                self.assertTrue(self.check_scan_success(scan_log_messages))
            else:  # BumpConnThrShScan logger
                self.assertTrue('Scan failed!' in scan_log_messages['error'])


if __name__ == '__main__':
    unittest.main()
