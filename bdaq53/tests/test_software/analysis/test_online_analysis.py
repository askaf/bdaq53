#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import os
import threading
import time
import unittest

import tables as tb
import numpy as np
import matplotlib
matplotlib.use('Agg')  # noqa: E402 Allow headless plotting

import bdaq53  # noqa: E731 E402
from bdaq53.analysis import online as oa  # noqa: E402
from bdaq53.tests import utils  # noqa: E40

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


def get_raw_data(raw_data_file):
    ''' Yield data of one readout

        Delay return if replay is too fast
    '''
    with tb.open_file(raw_data_file, mode="r") as in_file_h5:
        meta_data = in_file_h5.root.meta_data[:]
        raw_data = in_file_h5.root.raw_data
        n_readouts = meta_data.shape[0]

        for i in range(n_readouts):
            # Raw data indeces of readout
            i_start = meta_data['index_start'][i]
            i_stop = meta_data['index_stop'][i]

            yield raw_data[i_start:i_stop]


class TestOnlineAnalysis(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestOnlineAnalysis, cls).setUpClass()

        ana_logger = logging.getLogger('OnlineAnalysis')
        cls._ana_log_handler = utils.MockLoggingHandler(level='DEBUG')
        ana_logger.addHandler(cls._ana_log_handler)
        cls.ana_log_messages = cls._ana_log_handler.messages

        cls.occ_hist_oa = oa.OccupancyHistogramming(chip_type='rd53a')
        cls.tot_hist_oa = oa.TotHistogramming(chip_type='rd53a')

    @classmethod
    def tearDownClass(cls):
        cls.occ_hist_oa.close()
        cls.tot_hist_oa.close()
        del cls.occ_hist_oa
        del cls.tot_hist_oa

    def test_occupancy_histogramming(self):
        ''' Test online occupancy histogramming '''

        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        raw_data_file_result = os.path.join(data_folder, 'analog_scan_interpreted_result.h5')
        self.occ_hist_oa.reset()
        for words in get_raw_data(raw_data_file):
            self.occ_hist_oa.add(words)

        occ_hist = self.occ_hist_oa.get()

        with tb.open_file(raw_data_file_result) as in_file:
            occ_hist_exptected = in_file.root.HistOcc[:, :, 0]
            self.assertTrue(np.array_equal(occ_hist_exptected, occ_hist))

    def test_occupancy_histogramming_errors(self):
        # Check error message when requesting histogram before analysis finished

        def add_data():
            for words in get_raw_data(raw_data_file):
                for _ in range(100):
                    self.occ_hist_oa.add(words)

        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        self.occ_hist_oa.reset()
        thread = threading.Thread(target=add_data)
        thread.start()
        time.sleep(0.5)  # wait for first data to be added to queue
        self.occ_hist_oa.get(wait=False)
        thread.join()
        # Check that warning was given
        self.assertTrue('Getting histogram while analyzing data' in self.ana_log_messages['warning'])

    def test_tot_histogramming(self):
        ''' Test online tot histogramming '''

        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        raw_data_file_result = os.path.join(data_folder, 'analog_scan_interpreted_result.h5')
        self.tot_hist_oa.reset()
        for words in get_raw_data(raw_data_file):
            self.tot_hist_oa.add(words)
        tot_hist = self.tot_hist_oa.get()
        with tb.open_file(raw_data_file_result) as in_file:
            tot_hist_exptected = in_file.root.HistTot[:, :, 0, :]
            self.assertTrue(np.array_equal(tot_hist_exptected, tot_hist))


if __name__ == '__main__':
    logging.basicConfig(filemode='w', level=logging.DEBUG)
    unittest.main()
