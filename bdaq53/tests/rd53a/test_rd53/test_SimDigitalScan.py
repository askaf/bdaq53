#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import unittest
import logging
import shutil
import tables as tb
import numpy as np

from bdaq53.tests import utils
from bdaq53.scans.scan_digital import DigitalScan

dir_path = os.path.dirname(os.path.realpath(__file__))


configuration = {
    'n_injections': 2,

    'start_column': 160,
    'stop_column': 162,
    'start_row': 100,
    'stop_row': 102,
}


class TestDigitalScan(unittest.TestCase):
    def test_scan_digital(self):
        logging.info('Starting digital scan test')

        with DigitalScan(bdaq_conf=utils.setup_cocotb(20), bench_config=os.path.join(dir_path, 'testbench_sim.yaml'), scan_config=configuration) as scan:
            scan.start()

        ''' Assert raw data '''
        with tb.open_file(scan.output_filename + '_interpreted.h5', 'r+') as in_file_h5:
            hist_occ = in_file_h5.root.HistOcc[:]

        self.assertEqual(np.sum(hist_occ), 4 * configuration['n_injections'], 'Incorrect number of hits has been recorded!')
        self.assertEqual(hist_occ[160:162, 100:102].tolist(), [[[configuration['n_injections']]] * 2] * 2)

    def tearDown(self):
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
