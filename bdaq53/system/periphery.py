#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import time
import yaml
import tables as tb
from multiprocessing import Process, Queue, Value, Manager
from multiprocessing.queues import Empty
from tqdm import tqdm

from bdaq53.system import logger

from basil.dut import Dut


class PeripheryDeviceBase(object):
    def __init__(self, dut, name):
        self.dut = dut
        self.name = name
        try:
            self.device_type = self.dut.get_name()
        except TypeError:
            self.device_type = ''
        self._output = Value('i', False)
        self.q = Queue()
        self.ret = Queue()
        self.p = Process(target=self.main, args=(self.q, self.ret), name=name)
        self.p.start()

    def main(self, q, ret):
        stop = False
        while not stop:
            q_data = q.get()
            stop = q_data.get('stop', False)
            function = q_data.get('function')
            expect_ret = q_data.get('expect_ret', False)
            if function is not None:
                kwargs = q_data.get('kwargs')
                if kwargs is not None:
                    if expect_ret:
                        ret.put(getattr(self, '_' + function)(**eval(kwargs)))
                    else:
                        getattr(self, '_' + function)(**eval(kwargs))
                else:
                    if expect_ret:
                        ret.put(getattr(self, '_' + function)())
                    else:
                        getattr(self, '_' + function)()

    def close(self):
        try:
            self.q.put({'stop': True})
            self.q.close()
            self.q.join_thread()  # Needed otherwise IOError: [Errno 232] The pipe is being closed
            self.ret.close()
            self.ret.join_thread()  # Needed otherwise IOError: [Errno 232] The pipe is being closed
            self.p.join()
        except AssertionError:  # Queue was already closed
            pass



class PeripheryThermohygrometer(PeripheryDeviceBase):
    def get_temperature(self):
        self.q.put({'function': 'get_temperature', 'expect_ret': True})
        return self.ret.get()

    def _get_temperature(self):
        return float(self.dut.get_temperature())

    def get_humidity(self):
        self.q.put({'function': 'get_humidity', 'expect_ret': True})
        return self.ret.get()

    def _get_humidity(self):
        return float(self.dut.get_humidity())

    def get_dew_point(self):
        self.q.put({'function': 'get_dew_point', 'expect_ret': True})
        return self.ret.get()

    def _get_dew_point(self):
        return float(self.dut.get_dew_point())


class PeripheryMultimeter(PeripheryDeviceBase):
    def get_voltage(self):
        self.q.put({'function': 'get_voltage', 'expect_ret': True})
        return self.ret.get()

    def _get_voltage(self):
        return float(self.dut.get_voltage())

    def get_current(self):
        self.q.put({'function': 'get_current', 'expect_ret': True})
        return self.ret.get()

    def _get_current(self):
        return float(self.dut.get_current())


class PeripherySourcemeter(PeripheryDeviceBase):
    def on(self):
        self.q.put({'function': 'on'})

    def _on(self):
        self.dut.on()

    def off(self):
        self.q.put({'function': 'off'})

    def _off(self):
        self.dut.off()

    def get_on(self):
        self.q.put({'function': 'get_on', 'expect_ret': True})
        return self.ret.get()

    def _get_on(self):
        return bool(int(self.dut.get_on()))

    def get_voltage(self):
        self.q.put({'function': 'get_voltage', 'expect_ret': True})
        return self.ret.get()

    def _get_voltage(self, coffs=0, timeout=0.1):
        self.dut.off()
        self.dut.source_current()
        self.dut.set_current(coffs)
        self.dut.on()
        time.sleep(timeout)
        ret = self.dut.get_voltage()
        if ',' in ret:
            voltage = float(ret.split(',')[0])
        else:
            voltage = float(ret)
        self.dut.off()
        return voltage

    def get_current(self):
        self.q.put({'function': 'get_current', 'expect_ret': True})
        return self.ret.get()

    def _get_current(self, voffs=0, timeout=0.1):
        self.dut.off()
        self.dut.source_volt()
        self.dut.set_voltage(voffs)
        self.dut.on()
        time.sleep(timeout)
        ret = self.dut.get_current()
        if ',' in ret:
            current = float(ret.split(',')[0])
        else:
            current = float(ret)
        self.dut.off()
        return current

    def set_voltage(self, value):
        self.q.put({'function': 'set_voltage', 'kwargs': "{{'value':{0}}}".format(value)})

    def _set_voltage(self, value):
        self.dut.source_volt()
        if abs(value) < 0.2:
            self.dut.set_voltage_range(0.2)
        elif abs(value) < 2:
            self.dut.set_voltage_range(2)
        elif abs(value) < 20:
            self.dut.set_voltage_range(20)
        elif abs(value) < 200:
            self.dut.set_voltage_range(200)
        else:
            self.dut.set_voltage_range(1000)
        self.dut.set_voltage(value)
        self.dut.on()

    def set_current_limit(self, value):
        self.q.put({'function': 'set_current_limit', 'kwargs': "{{'value':{0}}}".format(value)})

    def _set_current_limit(self, value):
        self.dut.set_current_limit(value)

    def set_current(self, value):
        self.q.put({'function': 'set_current', 'kwargs': "{{'value':{0}}}".format(value)})

    def _set_current(self, value):
        self.dut.source_current()
        self.dut.set_current(value)


class PeripheryBDAQ(PeripheryDeviceBase):
    def on(self):
        self.output = True
        self.q.put({'function': 'on'})

    def _on(self):
        self.dut.set_enable(on=True)

    def off(self):
        self.output = False
        self.q.put({'function': 'off'})

    def _off(self):
        self.dut.set_enable(on=False)

    @property
    def output(self):
        return bool(self._output.value)

    @output.setter
    def output(self, value):
        self._output.value = value


class PeripheryLV(PeripheryDeviceBase):
    def on(self):
        self.output = True
        self.q.put({'function': 'on'})

    def _on(self):
        self.dut.set_enable(on=True)

    def off(self):
        self.output = False
        self.q.put({'function': 'off'})

    def _off(self):
        self.dut.set_enable(on=False)

    @property
    def output(self):
        return bool(self._output.value)

    @output.setter
    def output(self, value):
        self._output.value = value

    def get_current(self):
        self.q.put({'function': 'get_current', 'expect_ret': True})
        return self.ret.get()

    def _get_current(self):
        return float(self.dut.get_current())

    def get_voltage(self):
        self.q.put({'function': 'get_voltage', 'expect_ret': True})
        return self.ret.get()

    def _get_voltage(self):
        return float(self.dut.get_voltage())

    def set_voltage(self, value):
        self.q.put({'function': 'set_voltage', 'kwargs': "{{'value':{0}}}".format(value)})

    def _set_voltage(self, value):
        self.dut.set_voltage(value)

    def set_current_limit(self, value):
        self.q.put({'function': 'set_current_limit', 'kwargs': "{{'value':{0}}}".format(value)})

    def _set_current_limit(self, value):
        self.dut.set_current_limit(value)


class PeripheryHV(PeripheryDeviceBase):
    def on(self):
        self.q.put({'function': 'on'})

    def _on(self):
        self.dut.on()

    def off(self):
        self.q.put({'function': 'off'})

    def _off(self):
        self.dut.off()

    @property
    def output(self):
        self.q.put({'function': 'get_on', 'expect_ret': True})
        return self.ret.get()

    def _get_on(self):
        return bool(int(self.dut.get_on()))

    def get_current(self):
        self.q.put({'function': 'get_current', 'expect_ret': True})
        return self.ret.get()

    def _get_current(self):
        if bool(int(self.dut.get_on())):
            val = float(self.dut.get_current().split(',')[1])
        else:
            val = 0.
        return val

    def get_voltage(self):
        self.q.put({'function': 'get_voltage', 'expect_ret': True})
        return self.ret.get()

    def _get_voltage(self):
        if bool(int(self.dut.get_on())):
            val = float(self.dut.get_voltage().split(',')[0])
        else:
            val = 0.
        return val

    def set_voltage(self, value):
        self.q.put({'function': 'set_voltage', 'kwargs': "{{'value':{0}}}".format(value)})

    def _set_voltage(self, value):
        self.dut.source_volt()
        if abs(value) < 0.2:
            self.dut.set_voltage_range(0.2)
        elif abs(value) < 2:
            self.dut.set_voltage_range(2)
        elif abs(value) < 20:
            self.dut.set_voltage_range(20)
        elif abs(value) < 200:
            self.dut.set_voltage_range(200)
        else:
            self.dut.set_voltage_range(1000)
        self.dut.set_voltage(value)

    def set_current_limit(self, value):
        self.q.put({'function': 'set_current_limit', 'kwargs': "{{'value':{0}}}".format(value)})

    def _set_current_limit(self, value):
        self.dut.set_current_limit(value)


class BDAQ53Periphery(object):
    _aux_device_types = ['BDAQ', 'Multimeter', 'Sourcemeter']

    def __init__(self, bench_config=None, dut_conf=None):
        self.dut = None
        self.is_monitoring = False
        self.closed = True
        self.aux_devices = {}
        self.module_devices = {}
        self.log = logger.setup_derived_logger('Periphery')

        proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if bench_config is None or isinstance(bench_config, str):
            if bench_config is None:
                bench_config = os.path.join(proj_dir, 'testbench.yaml')
            with open(bench_config) as f:
                bench_config = yaml.full_load(f)
        if not isinstance(bench_config, dict):
            raise ValueError('bench_config is of unknown type!')

        if not bench_config['periphery'].get('enable_periphery', False):
            self.log.notice('Periphery is disabled.')
            self.enabled = False
            return
        else:
            self.configuration = bench_config['periphery']
            self.configuration['modules'] = bench_config['modules']
            self.enabled = True

        if dut_conf is None:
            self.dut_conf = os.path.join(proj_dir, 'periphery.yaml')
        else:
            self.dut_conf = dut_conf
        self.dut = Dut(self.dut_conf)

        self.module_list = list(bench_config['modules'].keys())

    def __del__(self):
        if not self.closed:
            self.close()

    def init(self):
        '''
            Initialize hardware and start process for each device.
        '''
        if not self.enabled:
            return

        self.closed = False

        try:
            self.log.notice('Initializing periphery...')
            self.dut.init()
            self.enabled = True

            for mod in self.module_list:
                self.module_devices[mod] = {}
                for typ in ['LV', 'HV']:
                    try:
                        if mod in self.module_devices.keys():
                            self.module_devices[mod].update({typ: eval("Periphery{0}(self.dut['{1}'], name='{1}')".format(typ, self.configuration['modules'][mod]['powersupply'][typ.lower() + '_name']))})
                        else:
                            self.module_devices[mod] = {typ: eval("Periphery{0}(self.dut['{1}'], name='{1}')".format(typ, self.configuration['modules'][mod]['powersupply'][typ.lower() + '_name']))}
                    except KeyError:
                        self.log.debug('No {0} device defined for module {1}.'.format(typ, mod))

            for dev in self.dut:
                if dev.name in self._aux_device_types:
                    self.aux_devices[dev.name] = eval("Periphery{0}(dev, name='{0}')".format(dev.name))

        except Exception as e:
            self.dut = None
            self.enabled = False
            self.log.notice('Cannot initialize periphery: %s' % e)
            raise e

        for mod in self.module_devices.keys():
            self.log.notice('Initialized devices for module {0}: {1}'.format(mod, list(self.module_devices[mod].keys())))
        self.log.notice('Initialized auxilliary devices: {}'.format(list(self.aux_devices.keys())))

    def close(self):
        self.stop_monitoring()
        for dev in self.aux_devices.values():
            dev.close()
        for mod in self.module_devices.values():
            for dev in mod.values():
                dev.close()
        if self.dut is not None:
            self.dut.close()
        self.closed = True

    # Control methods for AUX devices

    def power_on_BDAQ(self):
        if not self.enabled:
            return

        def ping_bdaq():
            ping = os.system('ping -c 1 -W 1 192.168.10.12 > /dev/null')
            if ping == 0:
                return True
            else:
                return False

        if 'BDAQ' not in self.aux_devices.keys():
            self.log.notice('No BDQ powersupply defined!')
            return

        if ping_bdaq():
            self.aux_devices['BDAQ'].output = True
            self.log.notice('BDAQ board already set up!')
        else:
            self.aux_devices['BDAQ'].on()
            self.log.notice('Powered on BDAQ board!')
            for _ in range(10):
                if ping_bdaq():
                    break
            else:
                self.log.notice('Could not power on BDAQ board!')

    def power_off_BDAQ(self):
        if not self.enabled:
            return

        if 'BDAQ' not in self.aux_devices.keys():
            self.log.notice('No BDAQ powersupply defined!')
            return

        self.aux_devices['BDAQ'].off()
        self.log.notice('Powered off BDAQ board!')

    # Control methods for modules

    def power_on_LV(self, module, verbose=True, **kwargs):
        '''
            Power on LV of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            if verbose:
                self.log.notice('No LV powersupply defined for module {}'.format(module))
            return

        config = self.configuration['modules'].get(module, {}).get('powersupply', {})
        config.update(**kwargs)

        if 'lv_voltage' in config.keys():
            self.module_devices[module]['LV'].set_voltage(config['lv_voltage'])
        else:
            if verbose:
                self.log.notice('No LV value defined for module {}'.format(module))
            return
        if 'lv_current_limit' in config.keys():
            self.module_devices[module]['LV'].set_current_limit(config['lv_current_limit'])
        self.module_devices[module]['LV'].on()
        time.sleep(0.5)
        if verbose:
            self.log.notice('Turned on LV of {0}: {1:1.3f}A @ {2:1.3f}V'.format(module, self.module_devices[module]['LV'].get_current(), config['lv_voltage']))

    def power_off_LV(self, module):
        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        if 'LV' in self.module_devices[module]:
            self.module_devices[module]['LV'].off()
            self.log.notice('Turned off LV of {0}.'.format(module))
        else:
            self.log.notice('No LV powersupply defined for module {}'.format(module))

    def power_on_module(self, module, **kwargs):
        '''
            Power on LV and HV of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        self.power_on_LV(module, **kwargs)
        self.power_on_HV(module, **kwargs)

    def power_off_module(self, module):
        '''
            Power off LV and HV of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        self.power_off_HV(module)
        self.power_off_LV(module)

    def get_module_power(self, module, log=True):
        '''
            Get LV and HV voltage and current of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        ret = {}
        log_string = 'Power consumption of {0}:'.format(module)
        if 'LV' in self.module_devices[module]:
            lv_voltage = self.module_devices[module]['LV'].get_voltage()
            lv_current = self.module_devices[module]['LV'].get_current()
            ret.update({'LV': {'voltage': lv_voltage, 'current': lv_current}})
            log_string += ' LV: {0:1.3f}A @ {1:1.3f}V'.format(lv_current, lv_voltage)
        else:
            self.log.notice('No LV powersupply defined for module {}'.format(module))

        if 'HV' in self.module_devices[module]:
            hv_voltage = self.module_devices[module]['HV'].get_voltage()
            hv_current = self.module_devices[module]['HV'].get_current()
            ret.update({'HV': {'voltage': hv_voltage, 'current': hv_current}})
            log_string += ' HV: {0:1.3e}A @ {1:1.0f}V'.format(hv_current, hv_voltage)
        else:
            self.log.notice('No HV powersupply defined for module {}'.format(module))

        if log:
            self.log.notice(log_string)
        return ret

    def power_on_HV(self, module, verbose=True, **kwargs):
        '''
            Power on HV of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            if verbose:
                self.log.notice('No devices defined for module {}'.format(module))
            return

        config = self.configuration['modules'].get(module, {}).get('powersupply', {})
        config.update(kwargs)

        if 'HV' in self.module_devices[module]:
            if 'hv_voltage' in config.keys() and 'hv_current_limit' in config.keys():
                self.module_devices[module]['HV'].set_current_limit(config['hv_current_limit'])
                self._ramp_hv_to(self.module_devices[module]['HV'], config['hv_voltage'], verbose=verbose)
            time.sleep(0.1)
            if verbose:
                self.log.notice('Turned on HV of {0}: {1:1.3e}A @ {2:1.0f}V'.format(module, self.module_devices[module]['HV'].get_current(), self.module_devices[module]['HV'].get_voltage()))
        else:
            if verbose:
                self.log.notice('No HV powersupply defined for module {}'.format(module))

    def power_off_HV(self, module):
        '''
            Power off HV of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        if 'HV' in self.module_devices[module]:
            self._ramp_hv_to(self.module_devices[module]['HV'], 0)
            self.module_devices[module]['HV'].off()
            self.log.notice('Turned off HV of {0}.'.format(module))
        else:
            self.log.notice('No HV powersupply defined for module {}'.format(module))

    def _ramp_hv_to(self, dev, target, verbose=True):
        if not dev.output:
            dev.set_voltage(0)
            dev.on()

        start = int(dev.get_voltage())
        target = int(target)

        if abs(start - target) > 10:
            stepsize = 2
        elif abs(start - target) > 100:
            stepsize = 10
        else:
            dev.set_voltage(target)
            return

        voltage_step = stepsize if target > start else -1 * stepsize
        add = 1 if target > start else -1

        if verbose:
            self.log.info('Ramping bias voltage to {} V...'.format(target))
            pbar = tqdm(total=len(range(start, target + add, voltage_step)), unit=' V')
        for v in range(start, target + add, voltage_step):
            dev.set_voltage(v)
            if verbose:
                pbar.update(1)
            time.sleep(0.5)
        if verbose:
            pbar.close()
        dev.set_voltage(target)

    ''' Monitoring methods '''

    def _monitoring_loop(self, q, interval):
        stop = False
        runtime = 0.
        while not stop:
            try:
                timer = time.time()
                try:
                    stop = q.get(False).get('stop', False)
                    if stop:
                        break
                except Empty:
                    pass

                with tb.open_file(self.monitoring_file, 'a') as f:  # noqa: F841
                    for grp, devs in self.monitored_devices.items():
                        for name, dev_entry in devs.items():
                            table = eval('f.root.' + grp + '.' + name)
                            row = table.row
                            row['timestamp'] = time.time()
                            row['timestamp_text'] = time.strftime("%Y-%m-%d %H:%M:%S")
                            dev = dev_entry['device']  # noqa: F841
                            for method in dev_entry['methods']:
                                if method.startswith('get_'):
                                    row[method.replace('get_', '')] = eval('dev.' + method + '()')
                            for prop in dev_entry['properties']:
                                if prop == 'output':
                                    row[prop] = eval('dev.' + prop)
                            row.append()
                            table.flush()
                            self.monitoring_data[grp][name] = table[:]

                try:
                    stop = q.get(False).get('stop', False)
                    if stop:
                        break
                except Empty:
                    pass

                runtime = time.time() - timer
                if runtime < interval:
                    time.sleep(interval - runtime)
                else:
                    self.log.warning('Monitoring loop runtime is longer than set interval! ({0:1.0f}s > {1}s)'.format(runtime, interval))
            except Exception as e:
                self.log.error('Error in monitoring process: {}'.format(e))

    def start_monitoring(self, outfile='periphery_monitoring'):
        if not self.enabled:
            return

        self.monitored_devices = {}
        all_devices = self.module_devices.copy()
        all_devices['aux'] = self.aux_devices.copy()
        self.manager = Manager()
        self.monitoring_data = self.manager.dict()

        base_description = {'timestamp': tb.Float64Col(pos=0), 'timestamp_text': tb.StringCol(64, pos=1)}

        if outfile.endswith('.h5'):
            self.monitoring_file = outfile
        else:
            self.monitoring_file = outfile + '.h5'
        with tb.open_file(self.monitoring_file, 'w') as f:
            for grp, devs in all_devices.items():
                self.monitored_devices[grp] = {}
                self.monitoring_data[grp] = self.manager.dict()
                f.create_group(f.root, name=grp, title=grp)
                for name, dev in devs.items():
                    description = dict(base_description)
                    self.monitored_devices[grp][name] = {'device': dev, 'methods': [], 'properties': []}
                    for method in [meth for meth in dir(dev) if callable(getattr(dev, meth))]:
                        if method.startswith('get_'):
                            description[method.replace('get_', '')] = tb.Float64Col()
                            self.monitored_devices[grp][name]['methods'].append(method)
                    for prop in [p for p in dir(dev) if property(getattr(dev, p))]:
                        if prop == 'output':
                            description[prop] = tb.BoolCol(pos=2)
                            self.monitored_devices[grp][name]['properties'].append(prop)

                    f.create_table(eval('f.root.' + grp), name=name, title='{0} {1} Data'.format(grp, name), description=description)

        self.log.notice('Starting to monitor periphery devices to file {}'.format(self.monitoring_file))
        self.is_monitoring = True
        self.monitoring_q = Queue()
        self.monitoring_p = Process(target=self._monitoring_loop, args=(self.monitoring_q, self.configuration.get('monitoring_interval', 10)), name='Monitoring')
        self.monitoring_p.start()

    def stop_monitoring(self):
        if self.is_monitoring:
            self.is_monitoring = False
            self.monitoring_q.put({'stop': True})
            self.monitoring_q.close()
            self.monitoring_q.join_thread()
            self.monitoring_p.join()
            self.log.notice('Stopped periphery monitoring process!')
