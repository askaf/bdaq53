
# ---------------------------------------------------------------
# Copyright (c) SILAB ,  Institute of Physics, University of Bonn
# ---------------------------------------------------------------
#
#   This script creates Vivado projects and bitfiles for the supported hardware platforms.
#
#   Start vivado in tcl mode by executing:
#       vivado -mode tcl -source run.tcl
#
#   NOTE: This will build firmware versions for every supported hardware. See the section "Create projects and bitfiles" below.
#   Alternatively, a list of 8 arguments can be used to build only the specified firmware.
#   The arguments have to be in the correct order. Just copy&paste from the "Create projects and bitfiles" section and remove all but one space in between the args. Empty arguments must be set to "".
#       vivado -mode tcl -source run.tcl -tclargs xc7k160tffg676-2 BDAQ53 "" ../src/bdaq53.xdc 64 "" _1LANE _RX640
#

set basil_dir [exec python -c "import basil, os; print(str(os.path.dirname(os.path.dirname(basil.__file__))))"]
set firmware_dir [exec python -c "import os; print(os.path.dirname(os.getcwd()))"]
set include_dirs [list $basil_dir/basil/firmware/modules $basil_dir/basil/firmware/modules/utils $firmware_dir/src/rx_aurora $firmware_dir]

file mkdir output reports


proc read_design_files {} {
    global firmware_dir
    read_verilog $firmware_dir/src/bdaq53.v
    read_verilog $firmware_dir/src/bdaq53_core.v
    read_edif $firmware_dir/SiTCP/SiTCP_XC7K_32K_BBT_V110.ngc
}


proc run_bit { part board connector xdc_file size option lanes speed} {
    set branch [if [catch {exec python -c "from bdaq53.system import scan_base; print(scan_base.get_software_version())"}] {puts "none"} else {exec python -c "from bdaq53.system import scan_base; print(scan_base.get_software_version().split('@')[0])"}]
    set version [exec python -c "import pkg_resources; print(pkg_resources.get_distribution('bdaq53').version)"]
    lindex [split $version '.'] 
    set version_major [lindex [split $version '.'] 0]
    set version_minor [lindex [split $version '.'] 1]
    set version_patch [lindex [split $version '.'] 2]

    create_project -force -part $part $board$option$connector$lanes$speed designs

    read_design_files
    read_xdc $xdc_file
    read_xdc ../src/SiTCP.xdc
    global include_dirs

    synth_design -top bdaq53 -include_dirs $include_dirs -verilog_define "$board=1" -verilog_define "$connector=1" -verilog_define "SYNTHESIS=1" -verilog_define "$option=1" -verilog_define "$lanes=1" -verilog_define "$speed=1" -generic VERSION_MAJOR=8'd$version_major -generic VERSION_MINOR=8'd$version_minor -generic VERSION_PATCH=8'd$version_patch
    opt_design
    place_design
    phys_opt_design
    route_design
    report_utilization -file "reports/report_utilization_$board$option$connector$lanes$speed.log"
    report_timing_summary -file "reports/report_timing_$board$option$connector$lanes$speed.log"
    write_bitstream -force -file output/$board$option$connector$lanes$speed
    write_cfgmem -format mcs -size $size -interface SPIx4 -loadbit "up 0x0 output/$board$option$connector$lanes$speed.bit" -force -file output/$board$option$connector$lanes$speed
    close_project

    exec tar -C ./output -cvzf output/$version\_$branch\_$board$option$connector$lanes$speed.tar.gz $board$option$connector$lanes$speed.bit $board$option$connector$lanes$speed.mcs
}


#########

#
# Create projects and bitfiles
#

if {$argc == 0} {
# By default, all firmware versions are generated. You can comment the ones you don't need.

# Bitfiles for the 640 Mb/s Aurora ip core configuration
#       FPGA type           board name  connector   constraints file     flash size  option  lanes    speed
run_bit xc7k160tffg676-2    BDAQ53      ""          ../src/bdaq53.xdc       64       ""      _1LANE   _RX640
run_bit xc7k160tfbg676-1    USBPIX3     ""          ../src/usbpix3.xdc      64       ""      _1LANE   _RX640
run_bit xc7k325tffg900-2    KC705       _SMA        ../src/kc705_gmii.xdc   16       ""      _1LANE   _RX640
run_bit xc7k325tffg900-2    KC705       _FMC_LPC    ../src/kc705_gmii.xdc   16       ""      _1LANE   _RX640

# Bitfiles for the 1.28 Gb/s Aurora ip core configuration
#       FPGA type           board name  connector   constraints file     flash size  option  lanes    speed
run_bit xc7k160tffg676-2    BDAQ53      ""          ../src/bdaq53.xdc       64       ""      _1LANE   _RX1280
run_bit xc7k160tfbg676-1    USBPIX3     ""          ../src/usbpix3.xdc      64       ""      _1LANE   _RX1280
run_bit xc7k325tffg900-2    KC705       _SMA        ../src/kc705_gmii.xdc   16       ""      _1LANE   _RX1280
run_bit xc7k325tffg900-2    KC705       _FMC_LPC    ../src/kc705_gmii.xdc   16       ""      _1LANE   _RX1280

# Bitfiles for special purposes
#       FPGA type           board name  connector   constraints file     flash size  option  lanes    speed
#run_bit xc7k160tffg676-2    BDAQ53      ""          ../src/bdaq53.xdc       64       ""      _2LANE   _RX1280
#run_bit xc7k160tffg676-2    BDAQ53      ""          ../src/bdaq53.xdc       64       ""      _3LANE   _RX1280
run_bit xc7k160tffg676-2    BDAQ53      ""          ../src/bdaq53.xdc       64       ""      _4LANE   _RX1280
run_bit xc7k160tfbg676-1    BDAQ53      ""          ../src/bdaq53_KX1.xdc   64       _KX1    _1LANE   _RX640
run_bit xc7k160tfbg676-1    BDAQ53      ""          ../src/bdaq53_KX1.xdc   64       _KX1    _1LANE   _RX1280


# In order to build only one specific firmware version, the tun.tcl can be executed with arguments
} else {
    if {$argc == 8} {
        run_bit {*}$argv
    } else {
        puts "ERROR: Invalid args"
    }
}
exit
